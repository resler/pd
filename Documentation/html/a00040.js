var a00040 =
[
    [ "SineWave", "a00040.html#aa90ee2e99f00a9ef45349cad4e737b99", null ],
    [ "SineWave", "a00040.html#ad738115ac756c90c6fd8b2362094958a", null ],
    [ "~SineWave", "a00040.html#a689beff4877f17953623e58ce2e3aa21", null ],
    [ "getFrequency", "a00040.html#ada8a990349ffcf5fe0860ca4db2c86b4", null ],
    [ "getVolume", "a00040.html#ad43dfb3cbe10f27809003c71fc189752", null ],
    [ "perform", "a00040.html#ad38e4ddf8adc38b957f2cd69a7ba004c", null ],
    [ "setFrequency", "a00040.html#a85781e7004c5373586efef734db2201c", null ],
    [ "setVolume", "a00040.html#abc1902a577b14129f5f9dbfa07edf946", null ]
];
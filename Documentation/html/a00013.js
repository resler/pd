var a00013 =
[
    [ "FileRead", "a00013.html#a67b08e455c002ad23b5bf6d7aa720a76", null ],
    [ "FileRead", "a00013.html#ae89dc4c8eb17ffff34804ef34d946160", null ],
    [ "~FileRead", "a00013.html#a8454596b14ca3c1e8903d7f0963aa1a3", null ],
    [ "channels", "a00013.html#a4c0c35f1817a4984b325967f1e7bcd81", null ],
    [ "close", "a00013.html#a59c75117642a01c3c18b1a21a67db1c1", null ],
    [ "fileRate", "a00013.html#a7d6ba525b2f8ae693d953cb7f4b7c199", null ],
    [ "fileSize", "a00013.html#a6a83d925dd5bd18c4299ea42cc0ea488", null ],
    [ "findNextMatArray", "a00013.html#a5cef3660f7d1687d128a5bf0ec5c3aa9", null ],
    [ "format", "a00013.html#a6852b303c34f01967be2a7a69221ef0b", null ],
    [ "getAifInfo", "a00013.html#a6476f0fe1bf878a1cd24c8eaffaec32c", null ],
    [ "getMatInfo", "a00013.html#aaa5d193fb65cdb5cabd7e70e8dcc100a", null ],
    [ "getRawInfo", "a00013.html#a83b0bfd14fec5da55b7b3908bd20fbce", null ],
    [ "getSndInfo", "a00013.html#aeeaa084d8e65bfcdeba4f1d16ebadc3e", null ],
    [ "getWavInfo", "a00013.html#ad6977f0f1dba8ce27435fd93cc975e77", null ],
    [ "isOpen", "a00013.html#a1a0ae01a8e2b289d76b0ad3cb11017b6", null ],
    [ "open", "a00013.html#a9d8b3b6a5250b175b9041676b360118f", null ],
    [ "read", "a00013.html#af870f4c9f1ce8e760bcabcfc23980ad4", null ],
    [ "byteswap_", "a00013.html#af2d78b2cf2557feaf13e7d4f2a642ec5", null ],
    [ "channels_", "a00013.html#a6e259fa88498216b1878d70821bf23f8", null ],
    [ "dataOffset_", "a00013.html#a88a8518d615e4abeecf443555ceef060", null ],
    [ "dataType_", "a00013.html#ac098e7765286aeb44e6ec0d21e6f092d", null ],
    [ "fd_", "a00013.html#ada5e71bd5da087b4f011abb6adce473d", null ],
    [ "fileRate_", "a00013.html#a469cd8abdeb958780c42d245062fd4c0", null ],
    [ "fileSize_", "a00013.html#ac061779011b90da547ff9e614324c95c", null ],
    [ "wavFile_", "a00013.html#a3770c79ad9d29507ea43a663ada54f55", null ]
];
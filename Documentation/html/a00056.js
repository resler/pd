var a00056 =
[
    [ "bitsPerSample", "a00056.html#a371f194c0f04e6f9769ec5f01023449f", null ],
    [ "bytesPerSample", "a00056.html#a28364ec6bdc49532b6019ca8a12fa21c", null ],
    [ "bytesPerSecond", "a00056.html#a8f1f262b8bc01a6596c9ce57891a9c5e", null ],
    [ "cbSize", "a00056.html#a9a1a7167b972698fa0257e593699e108", null ],
    [ "channelMask", "a00056.html#ac2a95de2a98b196b208e5ed757585eea", null ],
    [ "chunkSize", "a00056.html#a3c6b43c7ffea604f3cb521218b83e8fb", null ],
    [ "fact", "a00056.html#a3a9f032aa7932d14741515031662ca6a", null ],
    [ "factSize", "a00056.html#a639a06f3012385f18b519aa7c130c83b", null ],
    [ "fileSize", "a00056.html#a214806762db558bc9ba1341840a1a7b2", null ],
    [ "fmt", "a00056.html#aac64fc58238cd651b5f26269cf57213d", null ],
    [ "formatCode", "a00056.html#a89714ef27a1711f644a7b735c08afcfc", null ],
    [ "frames", "a00056.html#a74ba9e5f5831cb9114ddaa524531410d", null ],
    [ "nChannels", "a00056.html#a07d2875a68f0b8d4a0a9fd48b4c82e55", null ],
    [ "riff", "a00056.html#a2dda8e7133ef82eb0941e8a6c9f36128", null ],
    [ "sampleRate", "a00056.html#a739c529d0c83f1f0230bea4039a35d55", null ],
    [ "subformat", "a00056.html#a3a0320e5fa6e6708f4f88b781c00820a", null ],
    [ "validBits", "a00056.html#afc14cd42db4ddc0d30700146f5ab9cba", null ],
    [ "wave", "a00056.html#a460fa260c76fae220824678e75c436e7", null ]
];
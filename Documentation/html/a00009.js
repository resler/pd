var a00009 =
[
    [ "_cos", "a00001.html", "a00001" ],
    [ "tabfudge", "a00048.html", "a00048" ],
    [ "t_cos", "a00009.html#ac7ddaee7160fc443ba385b8f936e879a", null ],
    [ "Cosine", "a00009.html#ad177395996afcce2c21794c913f84923", null ],
    [ "~Cosine", "a00009.html#a9c0b21e2a0f7da95aa1a08749bc3654c", null ],
    [ "cos_maketable", "a00009.html#a6a8697d70c0fec2778af46e7cebda052", null ],
    [ "cos_new", "a00009.html#a6070c5b3d6912b13c6ad195e041fbb30", null ],
    [ "perform", "a00009.html#a4f4bd4f6993c04fe68ad08abfa1645e1", null ],
    [ "pdName", "a00009.html#acf7f049996d66f743626db18bd57c3cb", null ],
    [ "x", "a00009.html#afe1059cfb1f42ff2adf7522fa3590f0e", null ]
];
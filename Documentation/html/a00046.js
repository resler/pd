var a00046 =
[
    [ "Type", "a00046.html#af308e8782783f5af75ed2583ab651e58", [
      [ "STATUS", "a00046.html#af308e8782783f5af75ed2583ab651e58ab709ca6d9a8c392687e37a33c1e23726", null ],
      [ "WARNING", "a00046.html#af308e8782783f5af75ed2583ab651e58ac5c79dcc809569074828419f37261777", null ],
      [ "DEBUG_PRINT", "a00046.html#af308e8782783f5af75ed2583ab651e58aeb05a38e80eefb461db8feadccfe617f", null ],
      [ "MEMORY_ALLOCATION", "a00046.html#af308e8782783f5af75ed2583ab651e58a4e97a19d7d7033789aa3fa597b49b29b", null ],
      [ "MEMORY_ACCESS", "a00046.html#af308e8782783f5af75ed2583ab651e58afc7d7b4f94cdf159ab0ae28fc0998445", null ],
      [ "FUNCTION_ARGUMENT", "a00046.html#af308e8782783f5af75ed2583ab651e58aadb9382ded920ada3b5536b0a29cb495", null ],
      [ "FILE_NOT_FOUND", "a00046.html#af308e8782783f5af75ed2583ab651e58a4bf51b82f5dfaf5894db21ea9143d714", null ],
      [ "FILE_UNKNOWN_FORMAT", "a00046.html#af308e8782783f5af75ed2583ab651e58ae304dc18a1493711167da5c6d507759a", null ],
      [ "FILE_ERROR", "a00046.html#af308e8782783f5af75ed2583ab651e58a118e8f416b1a0350d92f1a0054fa4fdc", null ],
      [ "PROCESS_THREAD", "a00046.html#af308e8782783f5af75ed2583ab651e58ab0d536eb16937ffaa4ded93cd14e73dc", null ],
      [ "PROCESS_SOCKET", "a00046.html#af308e8782783f5af75ed2583ab651e58a216c334db22e8f405434ec42b6523187", null ],
      [ "PROCESS_SOCKET_IPADDR", "a00046.html#af308e8782783f5af75ed2583ab651e58a530f7dde8e4a499553a14fa18ef6afe7", null ],
      [ "AUDIO_SYSTEM", "a00046.html#af308e8782783f5af75ed2583ab651e58a44cae55f7381f766b24f3581c9a38e6a", null ],
      [ "MIDI_SYSTEM", "a00046.html#af308e8782783f5af75ed2583ab651e58a15516abbe0d3833a1d1d7ae73e871773", null ],
      [ "UNSPECIFIED", "a00046.html#af308e8782783f5af75ed2583ab651e58a9820b1456b50e67b50d5a37936a69a08", null ]
    ] ],
    [ "StkError", "a00046.html#ab6cbc80df11b385b20d244327fb74abb", null ],
    [ "~StkError", "a00046.html#ab0639b3a9354a0f84a9338d1c9b87464", null ],
    [ "getMessage", "a00046.html#ac691cbffe442d9e0760978146df5d9fb", null ],
    [ "getMessageCString", "a00046.html#ae802643199c86a797f7adebe109d84bf", null ],
    [ "getType", "a00046.html#aae641cf5ed2b756adbf02fc668fac53e", null ],
    [ "printMessage", "a00046.html#a337437a729f03d48fc56277037ac6fdc", null ],
    [ "message_", "a00046.html#a03becd4f31b1c32ed27f0b1e9dc84ce9", null ],
    [ "type_", "a00046.html#adec776c9099b403cf3de3577c8871d7d", null ]
];
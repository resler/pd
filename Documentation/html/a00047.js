var a00047 =
[
    [ "StkFrames", "a00047.html#aa9ad14889de633ba4269f1330be8bedf", null ],
    [ "StkFrames", "a00047.html#ac48aabe6cf029c06d2aa8a5fedf3621e", null ],
    [ "~StkFrames", "a00047.html#a3fa9a0a175de2b18e75d9b68a6387895", null ],
    [ "StkFrames", "a00047.html#a30fa9db1642284dff19084cad0cf47e1", null ],
    [ "channels", "a00047.html#aec7ef9c46675a24111aa6e2fda3ba870", null ],
    [ "dataRate", "a00047.html#a4dbbcbdc8db39a803e0861976f7f3522", null ],
    [ "empty", "a00047.html#ac6652950830fe907f9158ee891560253", null ],
    [ "frames", "a00047.html#a05b1ab6fa750a8221a7d65c30e0cdab9", null ],
    [ "interpolate", "a00047.html#a4f9402d9be28da418f630b4261b9b386", null ],
    [ "operator()", "a00047.html#a813632277bf0c432bcf81136ccdbbec1", null ],
    [ "operator()", "a00047.html#a93e7e4c30affc29d0cdc98e7514188de", null ],
    [ "operator*=", "a00047.html#a049cbfd10908f754a7c48c1dd0da21e0", null ],
    [ "operator+=", "a00047.html#a0f6a81d36a8905d782b283b9a05893e6", null ],
    [ "operator=", "a00047.html#ab6c76da2fafb251b5a94036bb11db5b1", null ],
    [ "operator[]", "a00047.html#a8ed9cc0a4309f786cfe36da9e901fde9", null ],
    [ "operator[]", "a00047.html#a2d1c8a2b32725ca72e0f3256f9cd8cb6", null ],
    [ "resize", "a00047.html#a386e1b86cf48f7a8117313f9e41fc0fe", null ],
    [ "resize", "a00047.html#a57ecf6563fdd2d3732aa9de2692af32e", null ],
    [ "setDataRate", "a00047.html#ad528b1c8ea2866570e7d9c2bac0e8b40", null ],
    [ "size", "a00047.html#a5c41aae98f77487c004085912d1f8f79", null ]
];
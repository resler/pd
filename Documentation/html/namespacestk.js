var namespacestk =
[
    [ "AifHeader", "structstk_1_1_aif_header.html", "structstk_1_1_aif_header" ],
    [ "AifSsnd", "structstk_1_1_aif_ssnd.html", "structstk_1_1_aif_ssnd" ],
    [ "FileRead", "classstk_1_1_file_read.html", "classstk_1_1_file_read" ],
    [ "FileWrite", "classstk_1_1_file_write.html", "classstk_1_1_file_write" ],
    [ "FileWvIn", "classstk_1_1_file_wv_in.html", "classstk_1_1_file_wv_in" ],
    [ "MatHeader", "structstk_1_1_mat_header.html", "structstk_1_1_mat_header" ],
    [ "SndHeader", "structstk_1_1_snd_header.html", "structstk_1_1_snd_header" ],
    [ "Stk", "classstk_1_1_stk.html", "classstk_1_1_stk" ],
    [ "StkError", "classstk_1_1_stk_error.html", "classstk_1_1_stk_error" ],
    [ "StkFrames", "classstk_1_1_stk_frames.html", "classstk_1_1_stk_frames" ],
    [ "WaveHeader", "structstk_1_1_wave_header.html", "structstk_1_1_wave_header" ],
    [ "WvIn", "classstk_1_1_wv_in.html", "classstk_1_1_wv_in" ]
];
var a00027 =
[
    [ "defaultHighInputLatency", "a00027.html#a4214826038fcaf374beb9816024e6c9f", null ],
    [ "defaultHighOutputLatency", "a00027.html#a2a43dbc2b158806a937065bda037070e", null ],
    [ "defaultLowInputLatency", "a00027.html#aad6629064b8c7cf043d237ea0a5cc534", null ],
    [ "defaultLowOutputLatency", "a00027.html#a89e60515505eea8d668ede3a26a19ac6", null ],
    [ "defaultSampleRate", "a00027.html#a68f435353bfe1a4c9b632203a9afcacb", null ],
    [ "hostApi", "a00027.html#afe741e4d185069577f7e74b78fdef5a4", null ],
    [ "maxInputChannels", "a00027.html#ad0baeeb2b6ceaf7fc9bb76109a7555e9", null ],
    [ "maxOutputChannels", "a00027.html#a48f908b21e8a11fdf0a88132b18fe7b0", null ],
    [ "name", "a00027.html#a1bc26d68f1d89472ae67eaa49a542aea", null ],
    [ "structVersion", "a00027.html#a2d726add4cd2af78a94c052b3a7df568", null ]
];
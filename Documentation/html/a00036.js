var a00036 =
[
    [ "Phasor", "a00036.html#aef043b4a0dbb10d01760ee45ee446f37", null ],
    [ "Phasor", "a00036.html#a34fe01723138c12280040fbbae6f7f68", null ],
    [ "~Phasor", "a00036.html#adffdf9dfef658dad64d7683a58b26448", null ],
    [ "Phasor", "a00036.html#a1040bd535d374907b22eda2aaf057ea9", null ],
    [ "getConv", "a00036.html#a265859cc63ed9195c83a06e1618077f4", null ],
    [ "getFrequency", "a00036.html#adc528ae9ca77a5813eef45f9daa93b9c", null ],
    [ "getPhase", "a00036.html#a7654f894d0ebead4b1c5896a1e62cd58", null ],
    [ "getVolume", "a00036.html#a5e85e6f193fae97dd4843bfc4a333f17", null ],
    [ "perform", "a00036.html#a74d2ff51bc79ea56019decff2e4d0793", null ],
    [ "setConv", "a00036.html#aeab4219d6e9a662f42d58589ecd32fc5", null ],
    [ "setFrequency", "a00036.html#a866e67fb197d5494f5175c1c5e63aed7", null ],
    [ "setPhase", "a00036.html#af465e7b59bfa161508273ceae77c4d73", null ],
    [ "setVolume", "a00036.html#adc5e736f9217148237a6d583164eac0f", null ],
    [ "pdName", "a00036.html#a240d0c963e815d10b5760518d098e10a", null ],
    [ "tf", "a00036.html#a1d3e0dd0be1efbd6f5732e4dae6148a7", null ]
];
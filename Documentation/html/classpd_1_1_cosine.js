var classpd_1_1_cosine =
[
    [ "_cos", "structpd_1_1_cosine_1_1__cos.html", "structpd_1_1_cosine_1_1__cos" ],
    [ "tabfudge", "unionpd_1_1_cosine_1_1tabfudge.html", "unionpd_1_1_cosine_1_1tabfudge" ],
    [ "t_cos", "classpd_1_1_cosine.html#ac7ddaee7160fc443ba385b8f936e879a", null ],
    [ "Cosine", "classpd_1_1_cosine.html#ad2c5d411527822ce0d32d626490768bf", null ],
    [ "~Cosine", "classpd_1_1_cosine.html#a34831b93025b43e6e8f2e4e14293db58", null ],
    [ "cos_maketable", "classpd_1_1_cosine.html#a12925008506530dc5c3d6e5ad15e4bc0", null ],
    [ "cos_new", "classpd_1_1_cosine.html#aaa455f7df04aefc7728c0ef1d177402c", null ],
    [ "perform", "classpd_1_1_cosine.html#a14295ac28d8f9b2dfd706299ce3d2675", null ],
    [ "pdName", "classpd_1_1_cosine.html#acf7f049996d66f743626db18bd57c3cb", null ],
    [ "x", "classpd_1_1_cosine.html#afe1059cfb1f42ff2adf7522fa3590f0e", null ]
];
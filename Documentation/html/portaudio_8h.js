var portaudio_8h =
[
    [ "PaHostApiInfo", "struct_pa_host_api_info.html", "struct_pa_host_api_info" ],
    [ "PaHostErrorInfo", "struct_pa_host_error_info.html", "struct_pa_host_error_info" ],
    [ "PaDeviceInfo", "struct_pa_device_info.html", "struct_pa_device_info" ],
    [ "PaStreamParameters", "struct_pa_stream_parameters.html", "struct_pa_stream_parameters" ],
    [ "PaStreamCallbackTimeInfo", "struct_pa_stream_callback_time_info.html", "struct_pa_stream_callback_time_info" ],
    [ "PaStreamInfo", "struct_pa_stream_info.html", "struct_pa_stream_info" ],
    [ "paClipOff", "portaudio_8h.html#a837b8a81be3f7e36c96003e0d8dcec12", null ],
    [ "paCustomFormat", "portaudio_8h.html#aba595f6b645c8134a8ac3004957b9ff7", null ],
    [ "paDitherOff", "portaudio_8h.html#add037e93aec78fa8d67f7a59ed500707", null ],
    [ "paFloat32", "portaudio_8h.html#a2f16d29916725b8791eae60ab9e0b081", null ],
    [ "paFormatIsSupported", "portaudio_8h.html#a400df642339bf4112333060af6a43c0f", null ],
    [ "paFramesPerBufferUnspecified", "portaudio_8h.html#ac1e20b97c1858a59bc62823573c59f0b", null ],
    [ "paInputOverflow", "portaudio_8h.html#aea144f0edaf02d0ac8b113f41fb95c47", null ],
    [ "paInputUnderflow", "portaudio_8h.html#a57d1879f52406bdedfdda8f79f458e15", null ],
    [ "paInt16", "portaudio_8h.html#ab284f8e0a161d78ede863aec45461dbd", null ],
    [ "paInt24", "portaudio_8h.html#a7f891a0bd9e94a94a8f446c176b749e7", null ],
    [ "paInt32", "portaudio_8h.html#a6fea69f3d81b628288325c06310b2fcf", null ],
    [ "paInt8", "portaudio_8h.html#a058ecfb48ac9f0efad7a7b0d446efe3e", null ],
    [ "paNeverDropInput", "portaudio_8h.html#ad89be5ed5279fc4c4a8495c775e1d5d5", null ],
    [ "paNoDevice", "portaudio_8h.html#a8c26dd5e6f28bb4cda7a1f5460282297", null ],
    [ "paNoFlag", "portaudio_8h.html#ad33384abe3754a39f4773f2561773595", null ],
    [ "paNonInterleaved", "portaudio_8h.html#a1d79f0b88812cef5364b276dbbc898b5", null ],
    [ "paOutputOverflow", "portaudio_8h.html#ac22bb260d47b348bd3aab8813fd45462", null ],
    [ "paOutputUnderflow", "portaudio_8h.html#aa17c91c859b12ebbf53a1440def53c89", null ],
    [ "paPlatformSpecificFlags", "portaudio_8h.html#a8522957e1bf814b9782d3a0e314c8b51", null ],
    [ "paPrimeOutputBuffersUsingStreamCallback", "portaudio_8h.html#ad7b862a223edd680d7985c33c847e31d", null ],
    [ "paPrimingOutput", "portaudio_8h.html#ad6616980c2259866a39eb2b76fa4697d", null ],
    [ "paUInt8", "portaudio_8h.html#ad826044e4ffe7b16bf990803ab77df37", null ],
    [ "paUseHostApiSpecificDeviceSpecification", "portaudio_8h.html#a14795fe3377c3b037025a61e550d44b5", null ],
    [ "PaDeviceIndex", "portaudio_8h.html#ad79317e65bde63d76c4b8e711ac5a361", null ],
    [ "PaDeviceInfo", "portaudio_8h.html#a84166d5f748e192341b71cf1b05d28b3", null ],
    [ "PaError", "portaudio_8h.html#a4949e4a8ef9f9dbe8cbee414ce69841d", null ],
    [ "PaErrorCode", "portaudio_8h.html#af3e4186dbeeb329bceafc0efc2124d98", null ],
    [ "PaHostApiIndex", "portaudio_8h.html#aeef6da084c57c70aa94be97411e19930", null ],
    [ "PaHostApiInfo", "portaudio_8h.html#a59bc8800f4c058add2e9a4de6f263352", null ],
    [ "PaHostApiTypeId", "portaudio_8h.html#ae247ec252e84112170079ece319fc42c", null ],
    [ "PaHostErrorInfo", "portaudio_8h.html#aae5f7523aa88f6e1ab05230103bd3560", null ],
    [ "PaSampleFormat", "portaudio_8h.html#a4582d93c2c2e60e12be3d74c5fe00b96", null ],
    [ "PaStream", "portaudio_8h.html#a19874734f89958fccf86785490d53b4c", null ],
    [ "PaStreamCallback", "portaudio_8h.html#a8a60fb2a5ec9cbade3f54a9c978e2710", null ],
    [ "PaStreamCallbackFlags", "portaudio_8h.html#a55a005924bcfa0424594f4f65cd4ae82", null ],
    [ "PaStreamCallbackResult", "portaudio_8h.html#a82cf09a8d14c7b614a8b73774f4952f1", null ],
    [ "PaStreamCallbackTimeInfo", "portaudio_8h.html#ac81b1065de3722c62eae395281451a73", null ],
    [ "PaStreamFinishedCallback", "portaudio_8h.html#ab2530ee0cb756c67726f9074d3482ef2", null ],
    [ "PaStreamFlags", "portaudio_8h.html#a37c7ac3ace7d2dd1430f40ecdee4ebb6", null ],
    [ "PaStreamInfo", "portaudio_8h.html#a5efc2a97ba96a3e06849c1a857127acc", null ],
    [ "PaStreamParameters", "portaudio_8h.html#aaedb633e6f41d592bf999c18bdb8bb12", null ],
    [ "PaTime", "portaudio_8h.html#af17a7e6d0471a23071acf8dbd7bbe4bd", null ],
    [ "PaErrorCode", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7", [
      [ "paNoError", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7aeb09d15a48b6c1034728a9c518cfe4ba", null ],
      [ "paNotInitialized", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7a12769658a78482b4265673f7c08fbb57", null ],
      [ "paUnanticipatedHostError", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7a47726071f5dccc656d5e3ff20bbfc5a0", null ],
      [ "paInvalidChannelCount", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7a46c51714378540c67c46895e726fb787", null ],
      [ "paInvalidSampleRate", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7a30a98097c3b6d9789077b72c26ae7c24", null ],
      [ "paInvalidDevice", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7a3442d8018bfa4329847d87df68b564a3", null ],
      [ "paInvalidFlag", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7aa5e7d0d9ea49ef845a93c89a3d138294", null ],
      [ "paSampleFormatNotSupported", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7a69660cd762aec2672fd3393e0971e99c", null ],
      [ "paBadIODeviceCombination", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7a795db60c291d92362b87edbc0bdb56cc", null ],
      [ "paInsufficientMemory", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7ad58986a9b5064d5a29c7ed0837bfc621", null ],
      [ "paBufferTooBig", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7a5a07d92bec4707ab34a01893bf24940e", null ],
      [ "paBufferTooSmall", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7a0711883755e19ee598cefa1db34d98cb", null ],
      [ "paNullCallback", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7a3fbb82a5d01ed678642426f1f5058e2c", null ],
      [ "paBadStreamPtr", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7aef50bc6456d8bcbd94d1690dfb8d447a", null ],
      [ "paTimedOut", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7a26ddab3d4dace3978ec6d5b3ccc10e87", null ],
      [ "paInternalError", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7a4851f9f753cdad3e7a89380d6589816c", null ],
      [ "paDeviceUnavailable", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7a467dac712e45745d8b0d8cd429fd1a01", null ],
      [ "paIncompatibleHostApiSpecificStreamInfo", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7a44f6cce375a3d38af28652179a400403", null ],
      [ "paStreamIsStopped", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7a43201ac8bea6578d4a13024315c24117", null ],
      [ "paStreamIsNotStopped", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7a9e05b6974d01fcce82c5d94b575ff1b2", null ],
      [ "paInputOverflowed", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7a25d517e59ea2e846119d972d5f1c53bb", null ],
      [ "paOutputUnderflowed", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7a77657d34c5e91a2962e7df7730441eee", null ],
      [ "paHostApiNotFound", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7a4b470a48b5fd9cf04591b1fbdd2ad26a", null ],
      [ "paInvalidHostApi", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7a99cc1bb53662f07a06876ec1818356a7", null ],
      [ "paCanNotReadFromACallbackStream", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7ae7bc144c8e0548287c6a3af67974df5a", null ],
      [ "paCanNotWriteToACallbackStream", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7af389ed20229971db693046093281b255", null ],
      [ "paCanNotReadFromAnOutputOnlyStream", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7a4a708b0414ebca1a78f9084671bd0a0c", null ],
      [ "paCanNotWriteToAnInputOnlyStream", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7af718bcda33019db4a3b9ec6bdefc6e9d", null ],
      [ "paIncompatibleStreamHostApi", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7a82f2e0e1c05189caf15b5f6c016edac6", null ],
      [ "paBadBufferPtr", "portaudio_8h.html#a2e45bf8b5145f131a91c128af2bdaec7a8679fc0a41ce0517d89a53b9c182a4b8", null ]
    ] ],
    [ "PaHostApiTypeId", "portaudio_8h.html#a8eaebe3d39c5ea45598da8f86dc2e5ae", [
      [ "paInDevelopment", "portaudio_8h.html#a8eaebe3d39c5ea45598da8f86dc2e5aea6d01a3fac7c228e8481efd5fd269f26d", null ],
      [ "paDirectSound", "portaudio_8h.html#a8eaebe3d39c5ea45598da8f86dc2e5aea5a888ef503d4dee954bb3582a65eb9a0", null ],
      [ "paMME", "portaudio_8h.html#a8eaebe3d39c5ea45598da8f86dc2e5aea92db20780c2d5a900ea6a9e988b4da0c", null ],
      [ "paASIO", "portaudio_8h.html#a8eaebe3d39c5ea45598da8f86dc2e5aea540d6dd219e6a6856347b66c4637adbe", null ],
      [ "paSoundManager", "portaudio_8h.html#a8eaebe3d39c5ea45598da8f86dc2e5aea069313b969ea3d41a275720879aa5ba8", null ],
      [ "paCoreAudio", "portaudio_8h.html#a8eaebe3d39c5ea45598da8f86dc2e5aeaa7295ccb885c47b70c829f8236161d1a", null ],
      [ "paOSS", "portaudio_8h.html#a8eaebe3d39c5ea45598da8f86dc2e5aea9afdcbf978e7b8b48db08a7c5208ebe4", null ],
      [ "paALSA", "portaudio_8h.html#a8eaebe3d39c5ea45598da8f86dc2e5aea5fb23d8b2cf3e2e714298de5ab1b83e1", null ],
      [ "paAL", "portaudio_8h.html#a8eaebe3d39c5ea45598da8f86dc2e5aea40c3e003e36409b4a1aae360625c69a9", null ],
      [ "paBeOS", "portaudio_8h.html#a8eaebe3d39c5ea45598da8f86dc2e5aeab6ef5d4eff3f3e80b90148a4b871b501", null ],
      [ "paWDMKS", "portaudio_8h.html#a8eaebe3d39c5ea45598da8f86dc2e5aeacb4ba221afe42867d96ab345ee13d78d", null ],
      [ "paJACK", "portaudio_8h.html#a8eaebe3d39c5ea45598da8f86dc2e5aeaf51e19fba6cd8946f06af332e61ad190", null ],
      [ "paWASAPI", "portaudio_8h.html#a8eaebe3d39c5ea45598da8f86dc2e5aeaa27b155bf2883aef821dfb6eba89e456", null ],
      [ "paAudioScienceHPI", "portaudio_8h.html#a8eaebe3d39c5ea45598da8f86dc2e5aea4651a7c2fb6e75843c130d82be7c7c4f", null ]
    ] ],
    [ "PaStreamCallbackResult", "portaudio_8h.html#ae9bfb9c4e1895326f30f80d415c66c32", [
      [ "paContinue", "portaudio_8h.html#ae9bfb9c4e1895326f30f80d415c66c32acba49cbf0e3bf605bb3deecef3b4fce3", null ],
      [ "paComplete", "portaudio_8h.html#ae9bfb9c4e1895326f30f80d415c66c32aa01800db52ead393d8b0016f63d76650", null ],
      [ "paAbort", "portaudio_8h.html#ae9bfb9c4e1895326f30f80d415c66c32acaf141eb5d71420ffb4474da1dbd8de2", null ]
    ] ],
    [ "Pa_AbortStream", "portaudio_8h.html#a138e57abde4e833c457b64895f638a25", null ],
    [ "Pa_CloseStream", "portaudio_8h.html#a92f56f88cbd14da0e8e03077e835d104", null ],
    [ "Pa_GetDefaultHostApi", "portaudio_8h.html#ae55c77f9b7e3f8eb301a6f1c0e2347ac", null ],
    [ "Pa_GetDefaultInputDevice", "portaudio_8h.html#abf9f2f82da95553d5adb929af670f74b", null ],
    [ "Pa_GetDefaultOutputDevice", "portaudio_8h.html#adc955dfab007624000695c48d4f876dc", null ],
    [ "Pa_GetDeviceCount", "portaudio_8h.html#acfe4d3c5ec1a343f459981bfa2057f8d", null ],
    [ "Pa_GetDeviceInfo", "portaudio_8h.html#ac7d8e091ffc1d1d4a035704660e117eb", null ],
    [ "Pa_GetErrorText", "portaudio_8h.html#ae606855a611cf29c7d2d7421df5e3b5d", null ],
    [ "Pa_GetHostApiCount", "portaudio_8h.html#a19dbdb7c8702e3f4bfc0cdb99dac3dd9", null ],
    [ "Pa_GetHostApiInfo", "portaudio_8h.html#a7c650aede88ea553066bab9bbe97ea90", null ],
    [ "Pa_GetLastHostErrorInfo", "portaudio_8h.html#aad573f208b60577f21d2777a7c5054e0", null ],
    [ "Pa_GetSampleSize", "portaudio_8h.html#a541ed0b734df2631bc4c229acf92abc1", null ],
    [ "Pa_GetStreamCpuLoad", "portaudio_8h.html#a83b8c624464dd7bb6a01b06ab596c115", null ],
    [ "Pa_GetStreamInfo", "portaudio_8h.html#a3d9c4cbda4e9f381b76f287c3de8a758", null ],
    [ "Pa_GetStreamReadAvailable", "portaudio_8h.html#ad04c33f045fa58d7b705b56b1fd3e816", null ],
    [ "Pa_GetStreamTime", "portaudio_8h.html#a2b3fb60e6949f37f7f134105ff425749", null ],
    [ "Pa_GetStreamWriteAvailable", "portaudio_8h.html#a25595acf48733ec32045aa189c3caa61", null ],
    [ "Pa_GetVersion", "portaudio_8h.html#a66da08bcf908e0849c62a6b47f50d7b4", null ],
    [ "Pa_GetVersionText", "portaudio_8h.html#a28f3fd9e6d9f933cc695abea71c4b445", null ],
    [ "Pa_HostApiDeviceIndexToDeviceIndex", "portaudio_8h.html#a54f306b5e5258323c95a27c5722258cd", null ],
    [ "Pa_HostApiTypeIdToHostApiIndex", "portaudio_8h.html#a081c3975126d20b4226facfb7ba0620f", null ],
    [ "Pa_Initialize", "portaudio_8h.html#abed859482d156622d9332dff9b2d89da", null ],
    [ "Pa_IsFormatSupported", "portaudio_8h.html#abdb313743d6efef26cecdae787a2bd3d", null ],
    [ "Pa_IsStreamActive", "portaudio_8h.html#a1f8709c4971932643681a6f374c4bb5a", null ],
    [ "Pa_IsStreamStopped", "portaudio_8h.html#a52d778c985ae9d566de7e13529cc771f", null ],
    [ "Pa_OpenDefaultStream", "portaudio_8h.html#a0a12735ac191200f696a43b87667b714", null ],
    [ "Pa_OpenStream", "portaudio_8h.html#a443ad16338191af364e3be988014cbbe", null ],
    [ "Pa_ReadStream", "portaudio_8h.html#a0b62d4b74b5d3d88368e9e4c0b8b2dc7", null ],
    [ "Pa_SetStreamFinishedCallback", "portaudio_8h.html#aa11e7b06b2cde8621551f5d527965838", null ],
    [ "Pa_Sleep", "portaudio_8h.html#a1b3c20044c9401c42add29475636e83d", null ],
    [ "Pa_StartStream", "portaudio_8h.html#a7432aadd26c40452da12fa99fc1a047b", null ],
    [ "Pa_StopStream", "portaudio_8h.html#af18dd60220251286c337631a855e38a0", null ],
    [ "Pa_Terminate", "portaudio_8h.html#a0db317604e916e8bd6098e60e6237221", null ],
    [ "Pa_WriteStream", "portaudio_8h.html#a075a6efb503a728213bdae24347ed27d", null ]
];
var structstk_1_1_aif_header =
[
    [ "aiff", "structstk_1_1_aif_header.html#ae6c5d81c8ffd1e8210eab7efe6cd72d1", null ],
    [ "comm", "structstk_1_1_aif_header.html#a0301215e8e7d35c33b6202b0ae57ec33", null ],
    [ "commSize", "structstk_1_1_aif_header.html#ab80932dd5ca1d43a961baa039a8e9464", null ],
    [ "form", "structstk_1_1_aif_header.html#a2f8f1e0ccffc7240e594e950ebf2ee61", null ],
    [ "formSize", "structstk_1_1_aif_header.html#af5d40a9a8e104d6377e52dc21b1c518c", null ],
    [ "nChannels", "structstk_1_1_aif_header.html#a68ac170aec411af534ea89832df6cb31", null ],
    [ "sampleFrames", "structstk_1_1_aif_header.html#a9c7221267f684cd0d33a3ba4f0fcee55", null ],
    [ "sampleSize", "structstk_1_1_aif_header.html#abe229ffc232da45ccec96e3104adf348", null ],
    [ "srate", "structstk_1_1_aif_header.html#a84c8854159b07cc519547c9cc65b12db", null ]
];
var structstk_1_1_snd_header =
[
    [ "comment", "structstk_1_1_snd_header.html#a00635d433714b51afbfaaf83aab81749", null ],
    [ "dataBytes", "structstk_1_1_snd_header.html#a0d7ccdc49b68b96efdda380e9c39f799", null ],
    [ "format", "structstk_1_1_snd_header.html#ae5dda7896c514b0520fbdc83eb541cc5", null ],
    [ "headerBytes", "structstk_1_1_snd_header.html#a25809a442e4f91ece069351d19ae6f06", null ],
    [ "nChannels", "structstk_1_1_snd_header.html#a3185d981e1a17c11491dbbca77fb01b7", null ],
    [ "pref", "structstk_1_1_snd_header.html#ae17c6d1d8e38358356357f9e78a88959", null ],
    [ "sampleRate", "structstk_1_1_snd_header.html#a732cbb42d55751ccad377b32770fcee4", null ]
];
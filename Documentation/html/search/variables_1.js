var searchData=
[
  ['defaulthighinputlatency',['defaultHighInputLatency',['../struct_pa_device_info.html#a4214826038fcaf374beb9816024e6c9f',1,'PaDeviceInfo']]],
  ['defaultinputdevice',['defaultInputDevice',['../struct_pa_host_api_info.html#a85a62057c3479d3efaa477d8562cf21d',1,'PaHostApiInfo']]],
  ['defaultlowinputlatency',['defaultLowInputLatency',['../struct_pa_device_info.html#aad6629064b8c7cf043d237ea0a5cc534',1,'PaDeviceInfo']]],
  ['defaultoutputdevice',['defaultOutputDevice',['../struct_pa_host_api_info.html#a62632690b5f7906d3c9bfdab91e7f4b2',1,'PaHostApiInfo']]],
  ['device',['device',['../struct_pa_stream_parameters.html#aebaf648b4d11dd1252a747b76b8da084',1,'PaStreamParameters']]],
  ['devicecount',['deviceCount',['../struct_pa_host_api_info.html#a44e3adfaba0117a6780e2493468c96b1',1,'PaHostApiInfo']]]
];

var searchData=
[
  ['sampleformat',['sampleFormat',['../struct_pa_stream_parameters.html#ad8d2d3063757b812f9e5f8709f41052b',1,'PaStreamParameters']]],
  ['samplerate',['sampleRate',['../struct_pa_stream_info.html#a9200fdee790d9155bc35d03be51ee2dd',1,'PaStreamInfo']]],
  ['stk_5ffloat32',['STK_FLOAT32',['../classstk_1_1_stk.html#a63c3942bca00afc51c38e6c2c99f72db',1,'stk::Stk']]],
  ['stk_5ffloat64',['STK_FLOAT64',['../classstk_1_1_stk.html#a7c6c1932fcab4d5adacd9ae4c3d36054',1,'stk::Stk']]],
  ['stk_5fsint16',['STK_SINT16',['../classstk_1_1_stk.html#a74923e12da75c2b13fac753ae381fbe3',1,'stk::Stk']]],
  ['stk_5fsint24',['STK_SINT24',['../classstk_1_1_stk.html#a02d5a1c8e8d98868a158053727ef4315',1,'stk::Stk']]],
  ['stk_5fsint32',['STK_SINT32',['../classstk_1_1_stk.html#acf8717472230415ff11bf499fc079bc5',1,'stk::Stk']]],
  ['stk_5fsint8',['STK_SINT8',['../classstk_1_1_stk.html#a5534403dfce86091451c6f23f48e39b3',1,'stk::Stk']]],
  ['structversion',['structVersion',['../struct_pa_host_api_info.html#a1f3a8c465488e7af8024237256d80c14',1,'PaHostApiInfo::structVersion()'],['../struct_pa_stream_info.html#a5e053a3f0f6232a5d1ec0dee0f9a943b',1,'PaStreamInfo::structVersion()']]],
  ['suggestedlatency',['suggestedLatency',['../struct_pa_stream_parameters.html#aa1e80ac0551162fd091db8936ccbe9a0',1,'PaStreamParameters']]]
];

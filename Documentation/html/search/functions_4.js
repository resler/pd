var searchData=
[
  ['filerate',['fileRate',['../classstk_1_1_file_read.html#a7d6ba525b2f8ae693d953cb7f4b7c199',1,'stk::FileRead']]],
  ['fileread',['FileRead',['../classstk_1_1_file_read.html#a67b08e455c002ad23b5bf6d7aa720a76',1,'stk::FileRead::FileRead(void)'],['../classstk_1_1_file_read.html#ae89dc4c8eb17ffff34804ef34d946160',1,'stk::FileRead::FileRead(std::string fileName, bool typeRaw=false, unsigned int nChannels=1, StkFormat format=STK_SINT16, StkFloat rate=22050.0)']]],
  ['filesize',['fileSize',['../classstk_1_1_file_read.html#a6a83d925dd5bd18c4299ea42cc0ea488',1,'stk::FileRead']]],
  ['filewrite',['FileWrite',['../classstk_1_1_file_write.html#aeecde2eff9578045deb9e85f72674780',1,'stk::FileWrite::FileWrite(void)'],['../classstk_1_1_file_write.html#a5e00a6b389ca0d0d14dd44eaaf524329',1,'stk::FileWrite::FileWrite(std::string fileName, unsigned int nChannels=1, FILE_TYPE type=FILE_WAV, Stk::StkFormat format=STK_SINT16)']]],
  ['filewvin',['FileWvIn',['../classstk_1_1_file_wv_in.html#ad5a2290033fd2a367aada83cd78a152d',1,'stk::FileWvIn::FileWvIn(unsigned long chunkThreshold=1000000, unsigned long chunkSize=1024)'],['../classstk_1_1_file_wv_in.html#af52be61388e804e8d0a6848ed9391242',1,'stk::FileWvIn::FileWvIn(std::string fileName, bool raw=false, bool doNormalize=true, unsigned long chunkThreshold=1000000, unsigned long chunkSize=1024)']]],
  ['format',['format',['../classstk_1_1_file_read.html#a6852b303c34f01967be2a7a69221ef0b',1,'stk::FileRead']]],
  ['frames',['frames',['../classstk_1_1_stk_frames.html#a05b1ab6fa750a8221a7d65c30e0cdab9',1,'stk::StkFrames']]],
  ['freebytes',['freebytes',['../class_pd_master.html#aebfa9aa079adae8ed55e7e48fa24a18c',1,'PdMaster']]],
  ['ftom',['ftom',['../class_pd_master.html#ad986f0867e03907d487efa4a481a5156',1,'PdMaster']]]
];

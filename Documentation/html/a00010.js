var a00010 =
[
    [ "CosineWave", "a00010.html#ae68b24fb8190115cd9a8c4bd490dc0db", null ],
    [ "CosineWave", "a00010.html#a35be64a7ec6f643e4f450076f675ab23", null ],
    [ "~CosineWave", "a00010.html#addb2216f3c407a30db3e501d55961206", null ],
    [ "getFrequency", "a00010.html#a54aa013903c0905be5eec03b4252fcde", null ],
    [ "getVolume", "a00010.html#ada4d00ebd2288308e92008004bfa27f0", null ],
    [ "perform", "a00010.html#aaa00925d263e82af92d8288f49d7f39d", null ],
    [ "setFrequency", "a00010.html#a1d7625481db2b943306e54a373cb10a6", null ],
    [ "setVolume", "a00010.html#a2a3d2e843daa99bcd88550b0ccc497dc", null ]
];
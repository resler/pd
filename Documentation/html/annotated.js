var annotated =
[
    [ "pd", "namespacepd.html", "namespacepd" ],
    [ "rwe", "namespacerwe.html", "namespacerwe" ],
    [ "stk", "namespacestk.html", "namespacestk" ],
    [ "Buffer", "struct_buffer.html", "struct_buffer" ],
    [ "Cosine", "class_cosine.html", null ],
    [ "FFT", "class_f_f_t.html", "class_f_f_t" ],
    [ "FileRead", "class_file_read.html", null ],
    [ "FileWrite", "class_file_write.html", null ],
    [ "FileWvIn", "class_file_wv_in.html", null ],
    [ "PaDeviceInfo", "struct_pa_device_info.html", "struct_pa_device_info" ],
    [ "PaHostApiInfo", "struct_pa_host_api_info.html", "struct_pa_host_api_info" ],
    [ "PaHostErrorInfo", "struct_pa_host_error_info.html", "struct_pa_host_error_info" ],
    [ "paRender", "classpa_render.html", "classpa_render" ],
    [ "PaStreamCallbackTimeInfo", "struct_pa_stream_callback_time_info.html", "struct_pa_stream_callback_time_info" ],
    [ "PaStreamInfo", "struct_pa_stream_info.html", "struct_pa_stream_info" ],
    [ "PaStreamParameters", "struct_pa_stream_parameters.html", "struct_pa_stream_parameters" ],
    [ "PdAlgorithm", "class_pd_algorithm.html", "class_pd_algorithm" ],
    [ "PdMaster", "class_pd_master.html", "class_pd_master" ],
    [ "Stk", "class_stk.html", null ]
];
//
//  LowPass_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/24/23.
//

#include "LowPass_Unity_Bindings.hpp"

pd::LowPass* LowPass_allocate0() {
    pd::LowPass* lop = new pd::LowPass();
    return lop;
}

void LowPass_free0(pd::LowPass* ptr) {
    pd::LowPass* lop = ptr;
    if(lop !=NULL)
    {
        delete lop;
        lop = nullptr;
    }
}

double LowPass_perform0(pd::LowPass* ptr, double input) {
    pd::LowPass* lop = ptr;
    return lop->perform(input);
}

void LowPass_setCutoff0(pd::LowPass* ptr, double cutoff) {
    pd::LowPass* lop = ptr;
    lop->setCutoff(cutoff);
}

double LowPass_getCutoff0(pd::LowPass* ptr) {
    pd::LowPass* lop = ptr;
    return lop->getCutoff();
}

void LowPass_clear0(pd::LowPass* ptr) {
    pd::LowPass* lop = ptr;
    lop->clear();
}

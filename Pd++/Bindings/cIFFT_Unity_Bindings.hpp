//
//  cIFFT_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#ifndef cIFFT_Unity_Bindings_hpp
#define cIFFT_Unity_Bindings_hpp

#include "cIFFT.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif
	struct complexFFTOutput {
		double real;
		double imaginary;
};
UNITY_API pd::complexIFFT* cIFFT_allocate0();
UNITY_API void cIFFT_free0(pd::complexIFFT*);
UNITY_API complexFFTOutput cIFFT_perform0(pd::complexIFFT*, double *input);

#ifdef __cplusplus
}
#endif

#endif /* cIFFT_Unity_Bindings_hpp */

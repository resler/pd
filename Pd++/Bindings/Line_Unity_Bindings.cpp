//
//  Line_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/24/23.
//

#include "Line_Unity_Bindings.hpp"

pd::Line* Line_allocate0() {
    pd::Line* line = new pd::Line();
    return line;
}

void Line_free0(pd::Line* ptr) {
    pd::Line* line = ptr;
    if(line !=NULL)
    {
        delete line;
        line = nullptr;
    }
}

double Line_perform0(pd::Line* ptr, double target, double time) {
    pd::Line* line = ptr;
    return line->perform(target, time);
}

void Line_set0(pd::Line* ptr, double target, double time) {
    pd::Line* line = ptr;
    line->set(target, time);
}

void Line_stop0(pd::Line* ptr) {
    pd::Line* line = ptr;
    line->stop();
}

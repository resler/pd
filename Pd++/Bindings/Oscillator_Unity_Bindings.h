//
//  Oscillator_Unity_Bindings.h
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/23/23.
//

#include "Oscillator.h"

#ifndef Oscillator_Unity_Bindings_h
#define Oscillator_Unity_Bindings_h


#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif
  
UNITY_API pd::Oscillator* Oscillator_allocate0 ();
UNITY_API void Oscillator_free0 (pd::Oscillator*);
UNITY_API double Oscillator_perform0 (pd::Oscillator*, double);
UNITY_API void Oscillator_setPhase0 (pd::Oscillator*, double);


#ifdef __cplusplus
}
#endif

#endif /* Oscillator_Unity_Bindings_h */

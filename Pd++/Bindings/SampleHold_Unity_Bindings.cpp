//
//  SampleHold_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/24/23.
//

#include "SampleHold_Unity_Bindings.hpp"

pd::SampleHold* SampleHold_allocate0() {
    pd::SampleHold* samp = new pd::SampleHold();
    return samp;
}

void SampleHold_free0(pd::SampleHold* ptr) {
    pd::SampleHold* samp = ptr;
    if(samp !=NULL)
    {
        delete samp;
        samp = nullptr;
    }
}

double SampleHold_perform0(pd::SampleHold* ptr, double input, double control) {
    pd::SampleHold* samp = ptr;
    return samp->perform(input, control);
}

void SampleHold_reset0(pd::SampleHold* ptr, double value) {
    pd::SampleHold* samp = ptr;
    samp->reset(value);
}

void SampleHold_set0(pd::SampleHold* ptr, double value) {
    pd::SampleHold* samp = ptr;
    samp->set(value);
}

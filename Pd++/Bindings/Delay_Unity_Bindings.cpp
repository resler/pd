//
//  Delay_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/24/23.
//

#include "Delay_Unity_Bindings.hpp"

pd::Delay* Delay_allocate0() {
    pd::Delay* del = new pd::Delay();
    return del;
}

void Delay_free0(pd::Delay* ptr) {
    pd::Delay* del = ptr;
    if(del !=NULL)
    {
        delete del;
        del = nullptr;
    }
}

double Delay_perform0(pd::Delay* ptr, double input) {
    pd::Delay* del = ptr;
    return del->perform(input);
}

void Delay_setDelayTime0(pd::Delay* ptr, double time) {
    pd::Delay* del = ptr;
    del->setDelayTime(time);
}

void Delay_reset0(pd::Delay* ptr) {
    pd::Delay* del = ptr;
    del->reset();
}

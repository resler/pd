//
//  SoundFiler_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/26/23.
//

#include "SoundFiler_Unity_Bindings.hpp"

pd::SoundFiler* SoundFiler_allocate0() {
    pd::SoundFiler* soundfiler = new pd::SoundFiler();
    return soundfiler;
}

void SoundFiler_free0(pd::SoundFiler* ptr) {
    pd::SoundFiler* soundfiler = ptr;
    if(soundfiler !=NULL)
    {
        delete soundfiler;
        soundfiler = nullptr;
    }
}

double SoundFiler_read0(pd::SoundFiler* ptr, char* file) {
    pd::SoundFiler* soundfiler = ptr;
    std::string f = file;
    return soundfiler->read(f);
}

/*See the notes below on what type and format to use.*/
 //types
 //0 = FILE_RAW; /*!< STK RAW file type. */
 //1 = FILE_WAV; /*!< WAV file type. */
 //2 = FILE_SND; /*!< SND (AU) file type. */
 //3 = FILE_AIF; /*!< AIFF file type. */
 //4 = FILE_MAT; /*!< Matlab MAT-file type. */
 
 //formats
 //0 = STK_SINT8;   /*!< -128 to +127 */
 //1 = STK_SINT16;  /*!< -32768 to +32767 */
 //2 =STK_SINT24;  /*!< Lower 3 bytes of 32-bit signed integer. */
 //3 = STK_SINT32;  /*!< -2147483648 to +2147483647. */
 //4 = STK_FLOAT32; /*!< Normalized between plus/minus 1.0. */
 //5 = STK_FLOAT64; /*!< Normalized between plus/minus 1.0. */

void SoundFiler_write0(pd::SoundFiler* ptr, char* fileName,
           unsigned int nChannels,
           long type,
           long format,
           double* array,
           long count) {
     pd::SoundFiler* soundfiler = ptr;
     std::string f = fileName;
    
    stk::FileWrite::FILE_TYPE t = 1;
    stk::Stk::StkFormat fo = 1;
    std::vector<double> audioFileData;
    
    for (int i = 0; i < count; i++)
            audioFileData.push_back(array[i]);
    
    switch (type) {
        case 0:
            t = stk::FileWrite::FILE_RAW;
            break;
        case 1:
            t = stk::FileWrite::FILE_WAV;
            break;
        case 2:
            t = stk::FileWrite::FILE_SND;
            break;
        case 3:
            t = stk::FileWrite::FILE_AIF;
            break;
        case 4:
            t = stk::FileWrite::FILE_MAT;
            break;
        default:
            t = stk::FileWrite::FILE_WAV;
            break;
    }
    
    switch (format) {
        case 0:
            fo =	stk::Stk::STK_SINT8;
            break;
        case 1:
            fo = stk::Stk::STK_SINT16;
            break;
        case 2:
            fo = stk::Stk::STK_SINT24;
            break;
        case 3:
            fo = stk::Stk::STK_SINT32;
            break;
        case 4:
            fo = stk::Stk::STK_FLOAT32;
            break;
        case 5:
            fo = stk::Stk::STK_FLOAT64;
            break;
        default:
            fo = stk::Stk::STK_SINT16;
            break;
    }
    
    soundfiler->write(fileName, nChannels, t, fo, audioFileData);
     
 }

void SoundFiler_getArray0(pd::SoundFiler* ptr, int size, double* output) {
    pd::SoundFiler* soundfiler = ptr;
    std::vector<double> arr = soundfiler->getArray();
    
    //copy the array
    for(int i = 0; i < size; i++)
        output[i] = arr[i];
    
 
}

//
//  rFFT_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/24/23.
//

#include "rFFT_Unity_Bindings.hpp"

pd::realFFT* rFFT_allocate0() {
    pd::realFFT* rfft = new pd::realFFT();
    return rfft;
}

void rFFT_free0(pd::realFFT* ptr) {
    pd::realFFT* rfft = ptr;
    if(rfft !=NULL)
    {
        delete rfft;
        rfft = nullptr;
    }
}

int rFFT_perform0(pd::realFFT* ptr, double input, double* output) {
    pd::realFFT* rfft = ptr;
    double* temp = rfft->perform(input);
    
    for (int i = 0; i < rfft->getFFTWindow(); i++)
    {
        output[i] = temp[i];
    }

    if (output == nullptr)
    {
        return 0;
    }
    else
    {
        return 1;
    }
    
}

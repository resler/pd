//
//  Threshold_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/24/23.
//

#include "Threshold_Unity_Bindings.hpp"

pd::Threshold* Threshold_allocate0() {
	pd::Threshold* thresh = new pd::Threshold();
	return thresh;
}

void Threshold_free0(pd::Threshold* ptr) {
	pd::Threshold* thresh = ptr;
	if (thresh != NULL)
	{
		delete thresh;
		thresh = nullptr;
	}
}

int Threshold_perform0(pd::Threshold* ptr, double input) {
	pd::Threshold* thresh = ptr;
	return thresh->perform(input);
}

void Threshold_setValues0(pd::Threshold* ptr, double ht, double hd, double lt, double ld) {
	pd::Threshold* thresh = ptr;
	thresh->setValues(ht, hd, lt, ld);
}

void Threshold_setState0(pd::Threshold* ptr, int s) {
	pd::Threshold* thresh = ptr;
	thresh->setState(s);
}
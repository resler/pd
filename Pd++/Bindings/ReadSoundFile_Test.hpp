//
//  ReadSoundFile_Test.hpp
//  pdplusplusUnity
//
//  Created by Klewen on 11/14/23.
//

#ifndef ReadSoundFile_Test_hpp
#define ReadSoundFile_Test_hpp

#include <string>
#include <vector>
#include <math.h>
#include "FileWvIn.h"
#include "Stk.h"
#include "PdMaster.h"

/**
  This is a separate class from ReadSoundFile.h.  Though it works the same, for some reason when calling
 the file handle from the C layer it always returns false (e.g. isOpen() ), which means that we can't confirm the file was
 actually opened by the OS.  Instead, in this version of the class we just assume it was opened when open() is
 called, if there is a bad path that will still throw an error, but it is possible, though probably rare, that the OS may not
 have actually opened the file and you may read into a buffer that is empty which may crash.
 This class was only created for the Unity Bindings.  The original ReadSoundFile class works as is in C++ and Java
 so only use this in Unity or C# and only if you really need to read audio from disk and not RAM.
 **/

namespace pd {

class ReadSoundFile_Test : public PdMaster, public stk::Stk {


public:

    ReadSoundFile_Test();
    ~ReadSoundFile_Test();
    void open(std::string file);
    void open(std::string file, double onset);
    double* start();
    void stop();
    void print();
    void setBufferSize(int bytes);
    int getBufferSize();
    int getChannels() {return channels;};
    bool isComplete();

    const std::string pdName = "readsf~";

private:
    stk::FileWvIn input;
    stk::StkFrames array;
    bool stopPlackback = false;
    bool bufferIsSet = false;
    long sampleCounter = 0;
    int channels = 2;
    double onset = 0;
    unsigned int bufferSize = 1024;
    double *output = nullptr;
    bool fileOpen = false;
};


}


#endif /* ReadSoundFile_Test_hpp */

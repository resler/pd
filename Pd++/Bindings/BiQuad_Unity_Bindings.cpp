//
//  BiQuad_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#include "BiQuad_Unity_Bindings.hpp"

pd::BiQuad* BiQuad_allocate0() {
    pd::BiQuad* biquad = new pd::BiQuad();
    return biquad;
}

double BiQuad_perform0(pd::BiQuad* ptr, double input) {
    pd::BiQuad* biquad = ptr;
    return biquad->perform(input);
}

void BiQuad_free0 (pd::BiQuad* ptr) {
    pd::BiQuad* biquad = ptr;
    if(biquad !=NULL)
    {
        delete biquad;
        biquad = nullptr;
    }
}

void BiQuad_setCoefficients0(pd::BiQuad* ptr, double fb1, double fb2, double ff1, double ff2, double ff3) {
    pd::BiQuad* biquad = ptr;
    biquad->setCoefficients(fb1, fb2, ff1, ff2, ff3);
}

void BiQuad_set0(pd::BiQuad* ptr, double a, double b) {
    pd::BiQuad* biquad = ptr;
    biquad->set(a, b);
}

//
//  VariableDelay_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/24/23.
//

#include "VariableDelay_Unity_Bindings.hpp"

pd::VariableDelay* VariableDelay_allocate0() {
	pd::VariableDelay* vd = new pd::VariableDelay();
	return vd;
 }

void VariableDelay_free0(pd::VariableDelay* ptr) {
	pd::VariableDelay* vd = ptr;
	if (vd != NULL)
	{
		delete vd;
		vd = nullptr;
	}
}

void VariableDelay_delayWrite0(pd::VariableDelay* ptr, double input) {
	pd::VariableDelay* vd = ptr;
	vd->delayWrite(input);
}

double VariableDelay_delayRead0(pd::VariableDelay* ptr, double delayTime) {
	pd::VariableDelay* vd = ptr;
	return vd->delayRead(delayTime);
}

double VariableDelay_perform0(pd::VariableDelay* ptr, double delayTime) {
	pd::VariableDelay* vd = ptr;
	return vd->perform(delayTime);
}
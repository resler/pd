//
//  BobFilter_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/24/23.
//

#include "BobFilter_Unity_Bindings.hpp"

 
pd::BobFilter* BobFilter_allocate0()
{
    pd::BobFilter* bob = new pd::BobFilter;
    return bob;
     
}

void BobFilter_free0(pd::BobFilter* ptr)
{
    pd::BobFilter* bob = ptr;
    if(bob !=NULL)
    {
        delete bob;
        bob = nullptr;
    }
}
 
double BobFilter_perform0(pd::BobFilter* ptr, double input) {
    pd::BobFilter* bob = ptr;
    return bob->perform(input);
}
 
void BobFilter_setCutoffFrequency0(pd::BobFilter* ptr, double cf) {
    pd::BobFilter* bob = ptr;
    bob->setCutoffFrequency(cf);
}

 void BobFilter_setResonance0(pd::BobFilter* ptr, double r) {
     pd::BobFilter* bob = ptr;
     bob->setResonance(r);
 }

 void BobFilter_setSaturation0(pd::BobFilter* ptr, double s) {
     pd::BobFilter* bob = ptr;
     bob->setSaturation(s);
 }

 void BobFilter_setOversampling0(pd::BobFilter* ptr, double o) {
     pd::BobFilter* bob = ptr;
     bob->setOversampling(o);
 }

 double BobFilter_getCutoffFrequency0(pd::BobFilter* ptr) {
     pd::BobFilter* bob = ptr;
     return bob->getCutoffFrequency();
 }
     
 double BobFilter_getResonance0(pd::BobFilter* ptr){
     pd::BobFilter* bob = ptr;
     return bob->getResonance();
 }
     
 void BobFilter_error0(pd::BobFilter* ptr){
     pd::BobFilter* bob = ptr;
     bob->error();
 }

 void BobFilter_clear0(pd::BobFilter* ptr){
     pd::BobFilter* bob = ptr;
     bob->clear();
 }

 void BobFilter_print0(pd::BobFilter* ptr){
     pd::BobFilter* bob = ptr;
     bob->print();
 }

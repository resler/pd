//
//  WriteSoundFile_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/26/23.
//

#include "WriteSoundFile_Unity_Bindings.hpp"

pd::WriteSoundFile* WriteSoundFile_allocate0() {
    pd::WriteSoundFile* writesf = new pd::WriteSoundFile();
    return writesf;
}

void WriteSoundFile_free0(pd::WriteSoundFile* ptr) {
    pd::WriteSoundFile* writesf = ptr;
    if(writesf !=NULL)
    {
        delete writesf;
        writesf = nullptr;
    }
}

/*See the notes below on what type and format to use.*/
 //types
 //0 = FILE_RAW; /*!< STK RAW file type. */
 //1 = FILE_WAV; /*!< WAV file type. */
 //2 = FILE_SND; /*!< SND (AU) file type. */
 //3 = FILE_AIF; /*!< AIFF file type. */
 //4 = FILE_MAT; /*!< Matlab MAT-file type. */
 
 //formats
 //0 = STK_SINT8;   /*!< -128 to +127 */
 //1 = STK_SINT16;  /*!< -32768 to +32767 */
 //2 =STK_SINT24;  /*!< Lower 3 bytes of 32-bit signed integer. */
 //3 = STK_SINT32;  /*!< -2147483648 to +2147483647. */
 //4 = STK_FLOAT32; /*!< Normalized between plus/minus 1.0. */
 //5 = STK_FLOAT64; /*!< Normalized between plus/minus 1.0. */
 void WriteSoundFile_open0(pd::WriteSoundFile* ptr, char* f, unsigned int nChannels,
                           long type, long format) {
     pd::WriteSoundFile* writesf = ptr;
     
     std::string file = f;
    
    stk::FileWrite::FILE_TYPE t = 1;
    stk::Stk::StkFormat form = 1;
    
    
    switch (type) {
        case 0:
            t = stk::FileWrite::FILE_RAW;
            break;
        case 1:
            t = stk::FileWrite::FILE_WAV;
            break;
        case 2:
            t = stk::FileWrite::FILE_SND;
            break;
        case 3:
            t = stk::FileWrite::FILE_AIF;
            break;
        case 4:
            t = stk::FileWrite::FILE_MAT;
            break;
        default:
            t = stk::FileWrite::FILE_WAV;
            break;
    }
    
    switch (format) {
        case 0:
            form = stk::Stk::STK_SINT8;
            break;
        case 1:
            form = stk::Stk::STK_SINT16;
            break;
        case 2:
            form = stk::Stk::STK_SINT24;
            break;
        case 3:
            form = stk::Stk::STK_SINT32;
            break;
        case 4:
            form = stk::Stk::STK_FLOAT32;
            break;
        case 5:
            form = stk::Stk::STK_FLOAT64;
            break;
        default:
            form = stk::Stk::STK_SINT16;
            break;
    }
     writesf->open(file, nChannels, t, form);
 }

void WriteSoundFile_start0(pd::WriteSoundFile* ptr, double* i) {
    pd::WriteSoundFile* writesf = ptr;
    writesf->start(i);
}

void WriteSoundFile_stop0(pd::WriteSoundFile* ptr) {
    pd::WriteSoundFile* writesf = ptr;
    writesf->stop();
}

void WriteSoundFile_print0(pd::WriteSoundFile* ptr) {
    pd::WriteSoundFile* writesf = ptr;
    writesf->print();
}

//
//  BobFilter_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#ifndef BobFilter_Unity_Bindings_hpp
#define BobFilter_Unity_Bindings_hpp

#include "BobFilter.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API pd::BobFilter* BobFilter_allocate0();
UNITY_API void BobFilter_free0(pd::BobFilter*);
UNITY_API double BobFilter_perform0(pd::BobFilter*, double input);
UNITY_API void BobFilter_setCutoffFrequency0(pd::BobFilter*, double cf);
UNITY_API void BobFilter_setResonance0(pd::BobFilter*, double r);
UNITY_API void BobFilter_setSaturation0(pd::BobFilter*, double s);
UNITY_API void BobFilter_setOversampling0(pd::BobFilter*, double o);
UNITY_API double BobFilter_getCutoffFrequency0(pd::BobFilter*);
UNITY_API double BobFilter_getResonance0(pd::BobFilter*);
UNITY_API void BobFilter_error0(pd::BobFilter*);
UNITY_API void BobFilter_clear0(pd::BobFilter*);
UNITY_API void BobFilter_print0(pd::BobFilter*);

#ifdef __cplusplus
}
#endif

#endif /* BobFilter_Unity_Bindings_hpp */

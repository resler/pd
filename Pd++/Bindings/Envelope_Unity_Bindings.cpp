//
//  Envelope_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/24/23.
//

#include "Envelope_Unity_Bindings.hpp"

pd::Envelope* Envelope_allocate0() {
    pd::Envelope* env = new pd::Envelope();
    return env;
}

pd::Envelope* Envelope_allocate1(int ws, int p) {
    pd::Envelope* env = new pd::Envelope(ws, p);
    return env;
}

void Envelope_free0(pd::Envelope* ptr) {
    pd::Envelope* env = ptr;
    if(env !=NULL)
    {
        delete env;
        env = nullptr;
    }
}

double Envelope_perform0(pd::Envelope* ptr, double input) {
    pd::Envelope* env = ptr;
    return env->perform(input);
}


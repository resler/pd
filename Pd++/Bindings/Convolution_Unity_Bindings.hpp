//
//  Convolution_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/26/23.
//

#ifndef Convolution_Unity_Bindings_hpp
#define Convolution_Unity_Bindings_hpp

#include "Convolution.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API rwe::Convolution* Convolution_allocate0();
UNITY_API void Convolution_free0(rwe::Convolution*);
UNITY_API double Convolution_perform0(rwe::Convolution*, double filter, double control);
UNITY_API void Convolution_setSquelch0(rwe::Convolution*, int sq);
UNITY_API int Convolution_getSquelch0(rwe::Convolution*);

#ifdef __cplusplus
}
#endif


#endif /* Convolution_Unity_Bindings_hpp */

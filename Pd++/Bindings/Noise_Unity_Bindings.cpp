//
//  Noise_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/23/23.
//

#include "Noise_Unity_Bindings.hpp"

pd::Noise* Noise_allocate0 () {
    pd::Noise *noise = new pd::Noise();
    return noise;
}

void Noise_free0 (pd::Noise* ptr) {
    pd::Noise *noise = ptr;
    if(noise !=NULL)
    {
        delete noise;
        noise = nullptr;
    }
}

double Noise_perform0 (pd::Noise* ptr) {
    pd::Noise *noise = ptr;
    return noise->perform();
}

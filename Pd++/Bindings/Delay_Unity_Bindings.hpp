//
//  Delay_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#ifndef Delay_Unity_Bindings_hpp
#define Delay_Unity_Bindings_hpp

#include "Delay.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API pd::Delay* Delay_allocate0();
UNITY_API void Delay_free0(pd::Delay*);
UNITY_API double Delay_perform0(pd::Delay*, double input);
UNITY_API void Delay_setDelayTime0(pd::Delay*, double time);
UNITY_API void Delay_reset0(pd::Delay*);

#ifdef __cplusplus
}
#endif

#endif /* Delay_Unity_Bindings_hpp */

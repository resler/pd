//
//  VariableDelay_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#ifndef VariableDelay_Unity_Bindings_hpp
#define VariableDelay_Unity_Bindings_hpp

#include "VariableDelay.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

    UNITY_API pd::VariableDelay* VariableDelay_allocate0();
    UNITY_API void VariableDelay_free0(pd::VariableDelay*);
    UNITY_API void VariableDelay_delayWrite0(pd::VariableDelay*, double input);
    UNITY_API  double VariableDelay_delayRead0(pd::VariableDelay*, double delayTime);
    UNITY_API double VariableDelay_perform0(pd::VariableDelay*, double delayTime);

#ifdef __cplusplus
}
#endif

#endif /* VariableDelay_Unity_Bindings_hpp */

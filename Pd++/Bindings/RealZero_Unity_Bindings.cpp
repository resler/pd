//
//  RealZero_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/24/23.
//

#include "RealZero_Unity_Bindings.hpp"

pd::RealZero* RealZero_allocate0() {
    pd::RealZero* rzero = new pd::RealZero();
    return rzero;
}

void RealZero_free0(pd::RealZero* ptr) {
    pd::RealZero* rzero = ptr;
    if(rzero !=NULL)
    {
        delete rzero;
        rzero = nullptr;
    }
}

double RealZero_perform0(pd::RealZero* ptr, double in, double coef) {
    pd::RealZero* rzero = ptr;
    return rzero->perform(in, coef);
}

void RealZero_set0(pd::RealZero* ptr, double real, double imaginary) {
    pd::RealZero* rzero = ptr;
    rzero->set(real, imaginary);
}

void RealZero_clear0(pd::RealZero* ptr) {
    pd::RealZero* rzero = ptr;
    rzero->clear();
}

//
//  SampleHold_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#ifndef SampleHold_Unity_Bindings_hpp
#define SampleHold_Unity_Bindings_hpp

#include "SampleHold.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API pd::SampleHold* SampleHold_allocate0();
UNITY_API void SampleHold_free0(pd::SampleHold*);
UNITY_API double SampleHold_perform0(pd::SampleHold*, double input, double control);
UNITY_API void SampleHold_reset0(pd::SampleHold*, double value);
UNITY_API void SampleHold_set0(pd::SampleHold*, double value);

#ifdef __cplusplus
}
#endif

#endif /* SampleHold_Unity_Bindings_hpp */

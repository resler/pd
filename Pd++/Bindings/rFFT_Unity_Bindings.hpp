//
//  rFFT_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#ifndef rFFT_Unity_Bindings_hpp
#define rFFT_Unity_Bindings_hpp

#include "rFFT.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API pd::realFFT* rFFT_allocate0();
UNITY_API void rFFT_free0(pd::realFFT*);
UNITY_API int rFFT_perform0(pd::realFFT*, double input, double* output);

#ifdef __cplusplus
}
#endif

#endif /* rFFT_Unity_Bindings_hpp */

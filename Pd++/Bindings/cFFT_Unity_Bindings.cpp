//
//  cFFT_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/24/23.
//

#include "cFFT_Unity_Bindings.hpp"

pd::complexFFT* cFFT_allocate0() {
    pd::complexFFT* fft = new pd::complexFFT();
    return fft;
}

void cFFT_free0(pd::complexFFT* ptr) {
    pd::complexFFT* fft = ptr;
    if(fft !=NULL)
    {
        delete fft;
        fft = nullptr;
    }
}

//complex FFT takes a window for both the real and the imaginary so our buffer is 2 * fftWindow
int cFFT_perform0(pd::complexFFT* ptr, double real, double imaginary, double* output) {
    pd::complexFFT* fft = ptr;
    double* temp = fft->perform(real, imaginary);
    for (int i = 0; i < fft->getFFTWindow()*2; i++)
    {
        output[i] = temp[i];
    }
    int status = 0;
    if (output == nullptr)
    {
        status = 0;
    }
    else
    {
        status = 1;
    }
    return status;
}

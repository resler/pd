//
//  VoltageControlFilter_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/24/23.
//

#include "VoltageControlFilter_Unity_Bindings.hpp"

pd::VoltageControlFilter* VoltageControlFilter_allocate0() {
	pd::VoltageControlFilter* vcf = new pd::VoltageControlFilter();
    vcfOut = new double[2];
	return vcf;
}

void VoltageControlFilter_free0(pd::VoltageControlFilter* ptr) {
	pd::VoltageControlFilter* vcf = ptr;
	if (vcf != nullptr)
	{
		delete vcf;
		vcf = nullptr;
	}
    if(vcfOut != nullptr)
    {
        delete vcfOut;
        vcfOut = nullptr;
    }
}

//returns both the real and imaginary components
pd::vcfOutput VoltageControlFilter_perform0(pd::VoltageControlFilter* ptr, double input, double centerFrequency) {
	pd::VoltageControlFilter* vcf = ptr;
    pd::vcfOutput vcfOutput = vcf->perform(input, centerFrequency);
    vcfOut[0] = vcfOutput.real;
    vcfOut[1] = vcfOutput.imaginary;
    return vcfOutput;
}

//returns only the real value, e.g the bandpass output
double VoltageControlFilter_perform1(pd::VoltageControlFilter* ptr, double input, double centerFrequency) {
    pd::VoltageControlFilter* vcf = ptr;
    pd::vcfOutput vcfOutput = vcf->perform(input, centerFrequency);
    double output = vcfOutput.real;
    return output;
}

//returns only the imaginary value, e.g the lowpass output
double VoltageControlFilter_perform2(pd::VoltageControlFilter* ptr, double input, double centerFrequency) {
    pd::VoltageControlFilter* vcf = ptr;
    pd::vcfOutput vcfOutput = vcf->perform(input, centerFrequency);
    double output = vcfOutput.imaginary;
    return output;
}

void VoltageControlFilter_setQ0(pd::VoltageControlFilter* ptr, double f) {
	pd::VoltageControlFilter* vcf = ptr;
	vcf->setQ(f);
}

double VoltageControlFilter_getDouble(pd::VoltageControlFilter*) {
    return 3.141592653589793238462643383279;
}

//
//  rIFFT_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#ifndef rIFFT_Unity_Bindings_hpp
#define rIFFT_Unity_Bindings_hpp

#include "rIFFT.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API pd::realIFFT* rIFFT_allocate0();
UNITY_API void rIFFT_free0(pd::realIFFT*);
UNITY_API double rIFFT_perform0(pd::realIFFT*, double *input);

#ifdef __cplusplus
}
#endif

#endif /* rIFFT_Unity_Bindings_hpp */

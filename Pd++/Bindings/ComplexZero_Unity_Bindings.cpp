//
//  ComplexZero_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/24/23.
//

#include "ComplexZero_Unity_Bindings.hpp"

pd::ComplexZero* ComplexZero_allocate0() {
    pd::ComplexZero* czero = new pd::ComplexZero();
    return czero;
}

void ComplexZero_free0(pd::ComplexZero* ptr) {
    pd::ComplexZero* czero = ptr;
    if(czero !=NULL)
    {
        delete czero;
        czero = nullptr;
    }
}

complexOutput ComplexZero_perform0(pd::ComplexZero* ptr, double in, double imag, double rcoef, double icoef) {
    pd::ComplexZero* czero = ptr;
    return czero->perform(in, imag, rcoef, icoef);
}

void ComplexZero_set0(pd::ComplexZero* ptr, double Complex, double imaginary) {
    pd::ComplexZero* czero = ptr;
    czero->set(Complex, imaginary);
}

void ComplexZero_clear0(pd::ComplexZero* ptr) {
    pd::ComplexZero* czero = ptr;
    czero->clear();
}

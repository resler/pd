//
//  BandPass_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/23/23.
//

#ifndef BandPass_Unity_Bindings_hpp
#define BandPass_Unity_Bindings_hpp

#include <stdio.h>
#include "BandPass.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API pd::BandPass* BandPass_allocate0();
UNITY_API void BandPass_free0(pd::BandPass*);
UNITY_API double BandPass_perform0(pd::BandPass*, double );
UNITY_API void BandPass_setCenterFrequency0(pd::BandPass*, double);
UNITY_API void BandPass_setQ0(pd::BandPass*, double );
UNITY_API void BandPass_clear0(pd::BandPass*);

#ifdef __cplusplus
}
#endif

#endif /* BandPass_Unity_Bindings_hpp */

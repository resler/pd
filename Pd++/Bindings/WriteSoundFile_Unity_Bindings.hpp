//
//  WriteSoundFile_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/26/23.
//

#ifndef WriteSoundFile_Unity_Bindings_hpp
#define WriteSoundFile_Unity_Bindings_hpp

#include "WriteSoundFile.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API pd::WriteSoundFile* WriteSoundFile_allocate0();
UNITY_API void WriteSoundFile_free0(pd::WriteSoundFile*);
UNITY_API void WriteSoundFile_open0(pd::WriteSoundFile*, char* f, unsigned int nChannels,
            long type, long format);
UNITY_API void WriteSoundFile_start0(pd::WriteSoundFile*, double* i);
UNITY_API void WriteSoundFile_stop0(pd::WriteSoundFile*);
UNITY_API void WriteSoundFile_print0(pd::WriteSoundFile*);

#ifdef __cplusplus
}
#endif


#endif /* WriteSoundFile_Unity_Bindings_hpp */

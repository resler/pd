//
//  RealZeroReverseReverse_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#ifndef RealZeroReverse_Unity_Bindings_hpp
#define RealZeroReverse_Unity_Bindings_hpp

#include "RawFilters.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API pd::RealZeroReverse* RealZeroReverse_allocate0();
UNITY_API void RealZeroReverse_free0(pd::RealZeroReverse*);
UNITY_API double RealZeroReverse_perform0(pd::RealZeroReverse*, double, double);
UNITY_API void RealZeroReverse_set0(pd::RealZeroReverse*, double real, double imaginary);
UNITY_API void RealZeroReverse_clear0(pd::RealZeroReverse*);

#ifdef __cplusplus
}
#endif

#endif /* RealZeroReverse_Unity_Bindings_hpp */

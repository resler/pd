//
//  Sigmund_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#ifndef Sigmund_Unity_Bindings_hpp
#define Sigmund_Unity_Bindings_hpp

#include "Sigmund.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif


#ifdef __cplusplus
extern "C" {
#endif

struct sigPack {
    double pitch;
    double notes;
    double envelope;
//    double **peaks; // we will copies these in our perform0() function.
//    double **tracks;
    int peakSize;
    int trackSize;
    
};

UNITY_API sigPack Sigmund_perform0(pd::Sigmund*, bool peaks, double input, double* output);
UNITY_API sigPack Sigmund_list0(pd::Sigmund*, bool peaks, double *array, int numOfPoints, int index, long sr, int debug, double* output);//read from an array
UNITY_API pd::Sigmund* Sigmund_allocate0();
UNITY_API pd::Sigmund* Sigmund_allocate_pitch0(char *pitch, char *env);
UNITY_API pd::Sigmund* Sigmund_allocate_tracks0(char *tracks, int num);
UNITY_API void Sigmund_free0(pd::Sigmund*);

UNITY_API void Sigmund_setMode0(pd::Sigmund*, int mode);
UNITY_API void Sigmund_setNumOfPoints0(pd::Sigmund*, double n);
UNITY_API void Sigmund_setHop0(pd::Sigmund*, double h);
UNITY_API void Sigmund_setNumOfPeaks0(pd::Sigmund*, double p);
UNITY_API void Sigmund_setMaxFrequency0(pd::Sigmund*, double mf);
UNITY_API void Sigmund_setVibrato0(pd::Sigmund*, double v);
UNITY_API void Sigmund_setStableTime0(pd::Sigmund*, double st);
UNITY_API void Sigmund_setMinPower0(pd::Sigmund*, double mp);
UNITY_API void Sigmund_setGrowth0(pd::Sigmund*, double g);
UNITY_API void Sigmund_print0(pd::Sigmund*);//print all parameters

UNITY_API void Sigmund_clear0(pd::Sigmund*);
    
UNITY_API int Sigmund_getMode0(pd::Sigmund*);
UNITY_API double Sigmund_getNumOfPoints0(pd::Sigmund*);
UNITY_API double Sigmund_getHop0(pd::Sigmund*);
UNITY_API double Sigmund_getNumOfPeaks0(pd::Sigmund*);
UNITY_API double Sigmund_getMaxFrequency0(pd::Sigmund*);
UNITY_API double Sigmund_getVibrato0(pd::Sigmund*);
UNITY_API double Sigmund_getStableTime0(pd::Sigmund*);
UNITY_API double Sigmund_getMinPower0(pd::Sigmund*);
UNITY_API double Sigmund_getGrowth0(pd::Sigmund*);
UNITY_API int Sigmund_getPeakColumnNumber0(pd::Sigmund*);
UNITY_API int Sigmund_getTrackColumnNumber0(pd::Sigmund*);
UNITY_API int Sigmund_getPeakBool0(pd::Sigmund*);
UNITY_API int Sigmund_getTrackBool0(pd::Sigmund*);


#ifdef __cplusplus
}
#endif

#endif /* Sigmund_Unity_Bindings_hpp */

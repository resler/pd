//
//  Line_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#ifndef Line_Unity_Bindings_hpp
#define Line_Unity_Bindings_hpp

#include "Line.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API pd::Line* Line_allocate0();
UNITY_API void Line_free0(pd::Line* );
UNITY_API double Line_perform0(pd::Line*, double target, double time);
UNITY_API void Line_set0(pd::Line*, double target, double time);
UNITY_API void Line_stop0(pd::Line*);

#ifdef __cplusplus
}
#endif

#endif /* Line_Unity_Bindings_hpp */

//
//  rIFFT_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/24/23.
//

#include "rIFFT_Unity_Bindings.hpp"

pd::realIFFT* rIFFT_allocate0() {
    pd::realIFFT* rifft = new pd::realIFFT();
    return rifft;
}

void rIFFT_free0(pd::realIFFT* ptr) {
    pd::realIFFT* rifft = ptr;
    if(rifft !=NULL)
    {
        delete rifft;
        rifft = nullptr;
    }
}

double rIFFT_perform0(pd::realIFFT* ptr, double *input) {
    pd::realIFFT* rifft = ptr;
    
    return rifft->perform(input);
}

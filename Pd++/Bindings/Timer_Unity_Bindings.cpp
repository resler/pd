//
//  Timer_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/26/23.
//

#include "Timer_Unity_Bindings.hpp"

rwe::Timer* Timer_allocate0() {
    rwe::Timer* timer = new rwe::Timer();
    return timer;
}

void Timer_free0(rwe::Timer* ptr) {
    rwe::Timer* timer = ptr;
    if(timer !=NULL)
    {
        delete timer;
        timer = nullptr;
    }
}

double Timer_perform0(rwe::Timer* ptr) {
    rwe::Timer* timer = ptr;
    return timer->perform();
}

void Timer_start0(rwe::Timer* ptr) {
    rwe::Timer* timer = ptr;
    timer->start();
}

void Timer_stop0(rwe::Timer* ptr) {
    rwe::Timer* timer = ptr;
    timer->stop();
}

void Timer_reset0(rwe::Timer* ptr) {
    rwe::Timer* timer = ptr;
    timer->reset();
}


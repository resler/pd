//
//  Metro_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/26/23.
//

#include "Metro_Unity_Bindings.hpp"

rwe::Metro* Metro_allocate0() {
    rwe::Metro* metro = new rwe::Metro();
    return metro;
}

void Metro_free0(rwe::Metro* ptr) {
    rwe::Metro* metro = ptr;
    if(metro !=NULL)
    {
        delete metro;
        metro = nullptr;
    }
}

bool Metro_perform0(rwe::Metro* ptr, double time) {
    rwe::Metro* metro = ptr;
    return metro->perform(time);
}

void Metro_setBPM0(rwe::Metro* ptr, bool t) {
    rwe::Metro* metro = ptr;
    metro->setBPM(t);
}

bool Metro_getBPM0(rwe::Metro* ptr) {
    rwe::Metro* metro = ptr;
    return metro->getBPM();
}

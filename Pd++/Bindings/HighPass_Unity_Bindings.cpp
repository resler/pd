//
//  HighPass_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/24/23.
//

#include "HighPass_Unity_Bindings.hpp"

pd::HighPass* HighPass_allocate0() {
    pd::HighPass* hip = new pd::HighPass();
    return hip;
}

void HighPass_free0(pd::HighPass* ptr) {
    pd::HighPass* hip = ptr;
    if(hip !=NULL)
    {
        delete hip;
        hip = nullptr;
    }
}

double HighPass_perform0(pd::HighPass* ptr, double input) {
    pd::HighPass* hip = ptr;
    return hip->perform(input);
}

void HighPass_setCutOff0(pd::HighPass* ptr, double f) {
    pd::HighPass* hip = ptr;
    hip->setCutOff(f);
}

void HighPass_clear0(pd::HighPass* ptr, double q) {
    pd::HighPass* hip = ptr;
    hip->clear(q);
}

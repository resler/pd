//
//  ComplexZeroReverse_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/24/23.
//

#include "ComplexZeroReverse_Unity_Bindings.hpp"

pd::ComplexZeroReverse* ComplexZeroReverse_allocate0() {
    pd::ComplexZeroReverse* czero = new pd::ComplexZeroReverse();
    return czero;
}

void ComplexZeroReverse_free0(pd::ComplexZeroReverse* ptr) {
    pd::ComplexZeroReverse* czero = ptr;
    if(czero !=NULL)
    {
        delete czero;
        czero = nullptr;
    }
}

complexOutput ComplexZeroReverse_perform0(pd::ComplexZeroReverse* ptr, double in, double imag, double rcoef, double icoef) {
    pd::ComplexZeroReverse* czero = ptr;
    return czero->perform(in, imag, rcoef, icoef);
}

void ComplexZeroReverse_set0(pd::ComplexZeroReverse* ptr, double Complex, double imaginary) {
    pd::ComplexZeroReverse* czero = ptr;
    czero->set(Complex, imaginary);
}

void ComplexZeroReverse_clear0(pd::ComplexZeroReverse* ptr) {
    pd::ComplexZeroReverse* czero = ptr;
    czero->clear();
}

//
//  Noise_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/23/23.
//

#ifndef Noise_Unity_Bindings_hpp
#define Noise_Unity_Bindings_hpp

#include <stdio.h>
#include "Noise.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API 
#endif

#ifdef __cplusplus
extern "C" {
#endif
  
	UNITY_API pd::Noise* Noise_allocate0 ();
	UNITY_API void Noise_free0 (pd::Noise*);
	UNITY_API double Noise_perform0 (pd::Noise*);

#ifdef __cplusplus
}
#endif

#endif /* Noise_Unity_Bindings_hpp */

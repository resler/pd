//
//  Cosine_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/25/23.
//

#ifndef Cosine_Unity_Bindings_hpp
#define Cosine_Unity_Bindings_hpp

#include "Cosine.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API pd::Cosine* Cosine_allocate0();
UNITY_API void Cosine_free0(pd::Cosine*);
UNITY_API double Cosine_perform0(pd::Cosine*, double );

#ifdef __cplusplus
}
#endif


#endif /* Cosine_Unity_Bindings_hpp */

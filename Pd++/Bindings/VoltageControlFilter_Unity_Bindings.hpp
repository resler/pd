//
//  VoltageControlFilter_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#ifndef VoltageControlFilter_Unity_Bindings_hpp
#define VoltageControlFilter_Unity_Bindings_hpp

#include "VoltageControlFilter.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif
    double *vcfOut;
	UNITY_API pd::VoltageControlFilter* VoltageControlFilter_allocate0();
	UNITY_API void VoltageControlFilter_free0(pd::VoltageControlFilter*);
	UNITY_API pd::vcfOutput VoltageControlFilter_perform0(pd::VoltageControlFilter*, double input, double centerFrequency);
    UNITY_API double VoltageControlFilter_perform1(pd::VoltageControlFilter*, double input, double centerFrequency);
    UNITY_API double VoltageControlFilter_perform2(pd::VoltageControlFilter*, double input, double centerFrequency);
    UNITY_API void VoltageControlFilter_setQ0(pd::VoltageControlFilter*, double f);
    UNITY_API double VoltageControlFilter_getDouble(pd::VoltageControlFilter*);

#ifdef __cplusplus
}
#endif

#endif /* VoltageControlFilter_Unity_Bindings_hpp */

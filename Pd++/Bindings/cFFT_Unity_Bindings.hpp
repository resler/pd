//
//  cFFT_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#ifndef cFFT_Unity_Bindings_hpp
#define cFFT_Unity_Bindings_hpp

#include "cFFT.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API pd::complexFFT* cFFT_allocate0();
UNITY_API void cFFT_free0(pd::complexFFT*);
UNITY_API int cFFT_perform0(pd::complexFFT*, double real, double imaginary, double* output);

#ifdef __cplusplus
}
#endif

#endif /* cFFT_Unity_Bindings_hpp */

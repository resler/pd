//
//  Cosine_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/25/23.
//

#include "Cosine_Unity_Bindings.hpp"

pd::Cosine* Cosine_allocate0() {
    pd::Cosine* cos = new pd::Cosine();
    return cos;
}

void Cosine_free0(pd::Cosine* ptr) {
    pd::Cosine* cos = ptr;
    if(cos !=NULL)
    {
        delete cos;
        cos = nullptr;
    }
}

double Cosine_perform0(pd::Cosine* ptr, double in) {
    pd::Cosine* cos = ptr;
    return cos->perform(in);
}

//
//  Oscillator_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/23/23.
//

#include "Oscillator_Unity_Bindings.h"

pd::Oscillator* Oscillator_allocate0 () {
    pd::Oscillator *osc = new pd::Oscillator();
    return osc;
}

void Oscillator_free0 (pd::Oscillator* ptr) {
    pd::Oscillator *osc = ptr;
    if(osc !=NULL)
    {
        delete osc;
        osc = nullptr;
    }
}

double Oscillator_perform0 (pd::Oscillator* ptr, double f) {
    pd::Oscillator *osc = ptr;
    return osc->perform(f);
}

void Oscillator_setPhase0 (pd::Oscillator* ptr, double ph) {
    pd::Oscillator *osc = ptr;
    osc->setPhase(ph);
}



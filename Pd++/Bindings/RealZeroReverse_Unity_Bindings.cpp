//
//  RealZeroReverseReverse_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/24/23.
//

#include "RealZeroReverse_Unity_Bindings.hpp"

pd::RealZeroReverse* RealZeroReverse_allocate0() {
    pd::RealZeroReverse* rzerorev = new pd::RealZeroReverse();
    return rzerorev;
}

void RealZeroReverse_free0(pd::RealZeroReverse* ptr) {
    pd::RealZeroReverse* rzerorev = ptr;
    if(rzerorev !=NULL)
    {
        delete rzerorev;
        rzerorev = nullptr;
    }
}

double RealZeroReverse_perform0(pd::RealZeroReverse* ptr, double in, double coef) {
    pd::RealZeroReverse* rzerorev = ptr;
    return rzerorev->perform(in, coef);
}

void RealZeroReverse_set0(pd::RealZeroReverse* ptr, double real, double imaginary) {
    pd::RealZeroReverse* rzerorev = ptr;
    rzerorev->set(real, imaginary);
}

void RealZeroReverse_clear0(pd::RealZeroReverse* ptr) {
    pd::RealZeroReverse* rzerorev = ptr;
    rzerorev->clear();
}

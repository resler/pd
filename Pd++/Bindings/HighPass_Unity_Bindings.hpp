//
//  HighPass_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#ifndef HighPass_Unity_Bindings_hpp
#define HighPass_Unity_Bindings_hpp

#include "HighPass.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif


UNITY_API pd::HighPass* HighPass_allocate0();
UNITY_API void HighPass_free0(pd::HighPass*);
UNITY_API double HighPass_perform0(pd::HighPass*, double input);
UNITY_API void HighPass_setCutOff0(pd::HighPass*, double f);
UNITY_API void HighPass_clear0(pd::HighPass*, double q);

#ifdef __cplusplus
}
#endif

#endif /* HighPass_Unity_Bindings_hpp */

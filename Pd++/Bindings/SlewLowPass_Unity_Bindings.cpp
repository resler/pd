//
//  SlewLowPass_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/24/23.
//

#include "SlewLowPass_Unity_Bindings.hpp"

pd::SlewLowPass* SlewLowPass_allocate0() {
    pd::SlewLowPass* slop = new pd::SlewLowPass();
    return slop;
}

void SlewLowPass_free0(pd::SlewLowPass* ptr) {
    pd::SlewLowPass* slop = ptr;
    if(slop !=NULL)
    {
        delete slop;
        slop = nullptr;
    }
}

double SlewLowPass_perform0(pd::SlewLowPass* ptr, double input, double freq, double posLimitIn, double posFreqIn, double negLimitIn, double negFreqIn ) {
    pd::SlewLowPass* slop = ptr;
    return slop->perform(input, freq, posLimitIn, posFreqIn, negLimitIn, negFreqIn);
}

void SlewLowPass_set0(pd::SlewLowPass* ptr, double last) {
    pd::SlewLowPass* slop = ptr;
    slop->set(last);
}

//
//  Metro_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/26/23.
//

#ifndef Metro_Unity_Bindings_hpp
#define Metro_Unity_Bindings_hpp

#include "Metro.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API rwe::Metro* Metro_allocate0();
UNITY_API void Metro_free0(rwe::Metro*);
UNITY_API bool Metro_perform0(rwe::Metro*, double time);
UNITY_API void Metro_setBPM0(rwe::Metro*, bool );
UNITY_API bool Metro_getBPM0(rwe::Metro*);

#ifdef __cplusplus
}
#endif

#endif /* Metro_Unity_Bindings_hpp */

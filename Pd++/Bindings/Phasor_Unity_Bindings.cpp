//
//  Phasor_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/24/23.
//

#include "Phasor_Unity_Bindings.hpp"

pd::Phasor* Phasor_allocate0() {
    pd::Phasor* phasor = new pd::Phasor();
    return phasor;
}

void Phasor_free0(pd::Phasor* ptr) {
    pd::Phasor* phasor = ptr;
    if(phasor !=NULL)
    {
        delete phasor;
        phasor = nullptr;
    }
}

double Phasor_perform0(pd::Phasor* ptr, double input) {
    pd::Phasor* phasor = ptr;
    return phasor->perform(input);
}

void Phasor_setPhase0(pd::Phasor* ptr, double p) {
    pd::Phasor* phasor = ptr;
    phasor->setPhase(p);
}

double Phasor_getPhase0(pd::Phasor* ptr) {
    pd::Phasor* phasor = ptr;
    return phasor->getPhase();
}

void Phasor_setFrequency0(pd::Phasor* ptr,  double f) {
    pd::Phasor* phasor = ptr;
    phasor->setFrequency(f);
}

double Phasor_getFrequency0(pd::Phasor* ptr) {
    pd::Phasor* phasor = ptr;
    return phasor->getFrequency();
}

void Phasor_setVolume0(pd::Phasor* ptr, double v) {
    pd::Phasor* phasor = ptr;
    phasor->setVolume(v);
}

double Phasor_getVolume0(pd::Phasor* ptr) {
    pd::Phasor* phasor = ptr;
    return phasor->getVolume();
}

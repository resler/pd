//
//  ReadSoundFile_Test.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 11/14/23.
//

#include "ReadSoundFile_Test.hpp"


namespace pd {

    ReadSoundFile_Test::ReadSoundFile_Test() {
        
    }

    ReadSoundFile_Test::~ReadSoundFile_Test() {
     if(output != nullptr)
        delete [] output;
        
    }

    void ReadSoundFile_Test::open(std::string file) {
        
        try
        {
            
            bufferIsSet = true;
            input.openFile(file);
            fileOpen = true; //see note in .h file about this
            // Find out how many channels we have.
            channels = input.channelsOut();
            output = new double[channels];
        }
        catch (stk::StkError&)
        {
            std::cout << "Not a valid file path. " << std::endl;
        }

        
    }

    /*
        This function will take an  additional onset to read into a file, in ms.
    */
    void ReadSoundFile_Test::open(std::string file, double o) {
        try
        {
            bufferIsSet = true;
            input.openFile(file);
            fileOpen = true; //see note in .h file about this
            // Find out how many channels we have.
            channels = input.channelsOut();
            onset = o;
            output = new double[channels];
        }
        catch (stk::StkError& e)
        {
            std::cout << "Not a valid file path. " << std::endl;
        }
    }

    double* ReadSoundFile_Test::start() {
        
    
        
        if (input.isOpen() || fileOpen)
        {
            std::cout << "We at least got here." << std::endl;
            if (onset > 0)
            {
                if ((onset / 1000) * input.getFileRate() >= input.getSize())
                {
                    std::cout << "Error: Offset greater than length of file!  Setting to 0.\n";
                        onset = 0;
                }
                input.addTime(input.getFileRate() * (onset/1000));//convert ms to samples
                onset = 0;
            }
            
            // Set input read rate based on the default sample rate.
            input.setRate(input.getFileRate() / ReadSoundFile_Test::sampleRate());

            input.ignoreSampleRateChange();

            // Resize the StkFrames object to our bufferSize.
            array.resize(this->getBufferSize(), channels);
            
            while (!input.isFinished() || !stopPlackback)
            {
                if (sampleCounter == 0)
                {
                    input.tick(array);
                }
                

                switch (channels) {
                case 1:
                    output[0] = array[sampleCounter++];
                    break;
                case 2:
                    output[0] = array[sampleCounter++];
                    output[1] = array[sampleCounter++];
                    break;
                case 3:
                    output[0] = array[sampleCounter++];
                    output[1] = array[sampleCounter++];
                    output[2] = array[sampleCounter++];
                    break;
                case 4:
                    output[0] = array[sampleCounter++];
                    output[1] = array[sampleCounter++];
                    output[2] = array[sampleCounter++];
                    output[3] = array[sampleCounter++];
                    break;
                case 5:
                    output[0] = array[sampleCounter++];
                    output[1] = array[sampleCounter++];
                    output[2] = array[sampleCounter++];
                    output[3] = array[sampleCounter++];
                    output[4] = array[sampleCounter++];
                    break;
                case 6:
                    output[0] = array[sampleCounter++];
                    output[1] = array[sampleCounter++];
                    output[2] = array[sampleCounter++];
                    output[3] = array[sampleCounter++];
                    output[4] = array[sampleCounter++];
                    output[5] = array[sampleCounter++];
                    break;
                case 7:
                    output[0] = array[sampleCounter++];
                    output[1] = array[sampleCounter++];
                    output[2] = array[sampleCounter++];
                    output[3] = array[sampleCounter++];
                    output[4] = array[sampleCounter++];
                    output[5] = array[sampleCounter++];
                    output[6] = array[sampleCounter++];
                    break;
                case 8:
                    output[0] = array[sampleCounter++];
                    output[1] = array[sampleCounter++];
                    output[2] = array[sampleCounter++];
                    output[3] = array[sampleCounter++];
                    output[4] = array[sampleCounter++];
                    output[5] = array[sampleCounter++];
                    output[6] = array[sampleCounter++];
                    output[7] = array[sampleCounter++];
                    break;
                }

                if (sampleCounter == array.size())
                {
                    sampleCounter = 0;
                }

                return output;
            }

            stop();

        }
        else
        {
            std::cout << "No file was opened.  Use open() before calling start()" << std::endl;
            return nullptr;
        }
        
        return nullptr; //everything failed, try again.
    }

    void ReadSoundFile_Test::stop() {
        stopPlackback = true;
        sampleCounter = 0;
        input.closeFile();
        fileOpen = false;
        delete [] output;
        output = nullptr;
    }

    //Sets the buffer size optionally to another size in bytes, default is 1024 or 10 bytes
    void ReadSoundFile_Test::setBufferSize(int bytes) {
        
        if (bufferIsSet)
        {
            std::cout << "File already open, cannot change buffer size.  Default to: " << this->getBufferSize() << std::endl;
        }
        else
        {
            bufferSize = pow(2.0, bytes);
        }
    }

    int ReadSoundFile_Test::getBufferSize() {
        return bufferSize;
    }

    void ReadSoundFile_Test::print() {
        std::cout << "Sample Rate: " << input.getFileRate() << std::endl;
        std::cout << "Channels: " << input.channelsOut() << std::endl;
        std::cout << "File Size in Samples: " << input.getSize() << std::endl;
        std::cout << "Endian: " << input.getEndian() << std::endl;
        std::cout << "Buffer Size: " << this->getBufferSize() << std::endl;
    }

    bool ReadSoundFile_Test::isComplete() {
        return stopPlackback;
    }


}

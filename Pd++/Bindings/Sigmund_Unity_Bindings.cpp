//
//  Sigmund_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/24/23.
//

#include "Sigmund_Unity_Bindings.hpp"

pd::Sigmund* Sigmund_allocate0() {
    pd::Sigmund* sigmund = new pd::Sigmund();
    return sigmund;
}

//this creates pitch and envelope following
pd::Sigmund* Sigmund_allocate_pitch0(char *pitch, char *env) {
    
    std::string p(pitch);
    std::string e(env);
    pd::Sigmund* sigmund = new pd::Sigmund(p, e);
    
    return sigmund;
}

//this creates tracks, see Sigmund.cpp for more information, or pd [sigmund~] help file.
pd::Sigmund* Sigmund_allocate_tracks0(char *tracks, int num) {
    
    std::string t(tracks);
    pd::Sigmund* sigmund = new pd::Sigmund(t, num);
    return sigmund;
}

void Sigmund_free0(pd::Sigmund* ptr) {
    pd::Sigmund* sigmund = ptr;
    if(sigmund !=NULL)
    {
        delete sigmund;
        sigmund = nullptr;
    }
}

//bool peaks == 0 means pitch/note, bool peaks == 1 means peaks/tracks
sigPack Sigmund_perform0(pd::Sigmund* ptr, bool peaks, double input, double* output) {
    pd::Sigmund* sigmund = ptr;
    sigPack sig;
    sigmundPackage _sig = sigmund->perform(input);
    
    if(peaks)
    {
        //otherwise we are tracks
        sig.envelope = _sig.envelope;
        sig.notes = _sig.notes;
        sig.pitch = _sig.pitch;
        sig.trackSize = _sig.trackSize;//these are 2*numOfTracks
        sig.peakSize = _sig.peakSize;//2*numOfPeaks,
        double** temp = _sig.peaks;
        
        if(sigmund->getPeakBool())
        {
            for(int i = 0; i < sig.peakSize/2; i++)
            {
                for(int j = 0; j < sigmund->getPeakColumnNumber(); j++)
                {
                    output[i*sigmund->getPeakColumnNumber() + j] = temp[i][j];
                }
            }
             
        }
        
        if(sigmund->getTrackBool())
        {
            double** temp = _sig.tracks;
            for(int i = 0; i < sig.trackSize/2; i++)
            {
                for(int j = 0; j < sigmund->getTrackColumnNumber(); j++)
                {
                    output[i*sigmund->getTrackColumnNumber() + j] = temp[i][j];
                }
            }
        }
        
    }
    else
    {
        //we are pitch/notes
        sig.envelope = _sig.envelope;
        sig.notes = _sig.notes;
        sig.pitch = _sig.pitch;
        sig.trackSize = _sig.trackSize;
        sig.peakSize = _sig.peakSize;
        //output[0][0] = 0;
    }
    
    return sig;
}

void Sigmund_setMode0(pd::Sigmund* ptr, int mode) {
    pd::Sigmund* sigmund = ptr;
    sigmund->setMode(mode);
}

void Sigmund_setNumOfPoints0(pd::Sigmund* ptr, double n) {
    pd::Sigmund* sigmund = ptr;
    sigmund->setNumOfPoints(n);
}

void Sigmund_setHop0(pd::Sigmund* ptr, double h) {
    pd::Sigmund* sigmund = ptr;
    sigmund->setHop(h);
}

void Sigmund_setNumOfPeaks0(pd::Sigmund* ptr, double p) {
    pd::Sigmund* sigmund = ptr;
    sigmund->setNumOfPeaks(p);
}

void Sigmund_setMaxFrequency0(pd::Sigmund* ptr, double mf) {
    pd::Sigmund* sigmund = ptr;
    sigmund->setMaxFrequency(mf);
}

void Sigmund_setVibrato0(pd::Sigmund* ptr, double v) {
    pd::Sigmund* sigmund = ptr;
    sigmund->setVibrato(v);
}

void Sigmund_setStableTime0(pd::Sigmund* ptr, double st) {
    pd::Sigmund* sigmund = ptr;
    sigmund->setStableTime(st);
}

void Sigmund_setMinPower0(pd::Sigmund* ptr, double mp) {
    pd::Sigmund* sigmund = ptr;
    sigmund->setMinPower(mp);
}

void Sigmund_setGrowth0(pd::Sigmund* ptr, double g) {
    pd::Sigmund* sigmund = ptr;
    sigmund->setGrowth(g);
}

void Sigmund_print0(pd::Sigmund* ptr) {
    pd::Sigmund* sigmund = ptr;
    sigmund->print();
}

sigPack Sigmund_list0(pd::Sigmund* ptr, bool peaks, double *array, int numOfPoints, int index, long sr, int debug, double* output) {
    pd::Sigmund* sigmund = ptr;
    sigPack sig;
    sigmundPackage _sig = sigmund->list(array, numOfPoints, index, sr, debug);;
    sig.envelope = _sig.envelope;
    sig.notes = _sig.notes;
    if(peaks)
    {
        //otherwise we are tracks
        sig.envelope = _sig.envelope;
        sig.notes = _sig.notes;
        sig.pitch = _sig.pitch;
        sig.trackSize = _sig.trackSize;//these are 2*numOfTracks
        sig.peakSize = _sig.peakSize;//2*numOfPeaks,
        double** temp = _sig.peaks;
        
        if(sigmund->getPeakBool())
        {
            for(int i = 0; i < sig.peakSize/2; i++)
            {
                for(int j = 0; j < sigmund->getPeakColumnNumber(); j++)
                {
                    output[i*sigmund->getPeakColumnNumber() + j] = temp[i][j];
                }
            }
             
        }
        
        if(sigmund->getTrackBool())
        {
            double** temp = _sig.tracks;
            for(int i = 0; i < sig.trackSize/2; i++)
            {
                for(int j = 0; j < sigmund->getTrackColumnNumber(); j++)
                {
                    output[i*sigmund->getTrackColumnNumber() + j] = temp[i][j];
                }
            }
        }
        
    }
    else
    {
        //we are pitch/notes
        sig.envelope = _sig.envelope;
        sig.notes = _sig.notes;
        sig.pitch = _sig.pitch;
        sig.trackSize = _sig.trackSize;
        sig.peakSize = _sig.peakSize;
        //output[0][0] = 0;
    }
    return sig;
}

void Sigmund_clear0(pd::Sigmund* ptr) {
    pd::Sigmund* sigmund = ptr;
    sigmund->clear();
}
    
int Sigmund_getMode0(pd::Sigmund* ptr) {
    pd::Sigmund* sigmund = ptr;
   return  sigmund->getMode();
}

double Sigmund_getNumOfPoints0(pd::Sigmund* ptr) {
    pd::Sigmund* sigmund = ptr;
    return sigmund->getNumOfPoints();
}

double Sigmund_getHop0(pd::Sigmund* ptr) {
    pd::Sigmund* sigmund = ptr;
    return sigmund->getHop();
}

double Sigmund_getNumOfPeaks0(pd::Sigmund* ptr) {
    pd::Sigmund* sigmund = ptr;
    return sigmund->getNumOfPeaks();
}

double Sigmund_getMaxFrequency0(pd::Sigmund* ptr) {
    pd::Sigmund* sigmund = ptr;
    return sigmund->getMaxFrequency();
}

double Sigmund_getVibrato0(pd::Sigmund* ptr) {
    pd::Sigmund* sigmund = ptr;
    return sigmund->getVibrato();
}

double Sigmund_getStableTime0(pd::Sigmund* ptr) {
    pd::Sigmund* sigmund = ptr;
    return sigmund->getStableTime();
}

double Sigmund_getMinPower0(pd::Sigmund* ptr) {
    pd::Sigmund* sigmund = ptr;
    return sigmund->getMinPower();
}

double Sigmund_getGrowth0(pd::Sigmund* ptr) {
    pd::Sigmund* sigmund = ptr;
    return sigmund->getGrowth();
}

int Sigmund_getPeakColumnNumber0(pd::Sigmund* ptr) {
    pd::Sigmund* sigmund = ptr;
    return sigmund->getPeakColumnNumber();
}

int Sigmund_getTrackColumnNumber0(pd::Sigmund* ptr) {
    pd::Sigmund* sigmund = ptr;
    return sigmund->getTrackColumnNumber();
}

int Sigmund_getPeakBool0(pd::Sigmund* ptr) {
    pd::Sigmund* sigmund = ptr;
    return sigmund->getPeakBool();
}

int Sigmund_getTrackBool0(pd::Sigmund* ptr) {
    pd::Sigmund* sigmund = ptr;
    return sigmund->getTrackBool();
}


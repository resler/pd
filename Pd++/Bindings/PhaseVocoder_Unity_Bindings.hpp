//
//  PhaseVocoder_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Klewen on 6/7/2024.
//

#ifndef PhaseVocoder_Unity_Bindings_hpp
#define PhaseVocoder_Unity_Bindings_hpp

#include "PhaseVocoder.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

	UNITY_API rwe::PhaseVocoder* PhaseVocoder_allocate0();
	UNITY_API void PhaseVocoder_free0(rwe::PhaseVocoder*);
	UNITY_API double PhaseVocoder_perform0(rwe::PhaseVocoder*);
	UNITY_API void PhaseVocoder_inSample(rwe::PhaseVocoder*, char* file);
	UNITY_API void PhaseVocoder_setSpeed0(rwe::PhaseVocoder*, double s);
	UNITY_API void PhaseVocoder_setTranspo0(rwe::PhaseVocoder*, double t);
	UNITY_API void PhaseVocoder_setLock0(rwe::PhaseVocoder*, int l);
	UNITY_API void PhaseVocoder_setRewindd0(rwe::PhaseVocoder*);

#ifdef __cplusplus
}
#endif


#endif /* PhaseVocoder_Unity_Bindings_hpp */


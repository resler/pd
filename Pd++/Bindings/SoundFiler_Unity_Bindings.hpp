//
//  SoundFiler_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/26/23.
//

#ifndef SoundFiler_Unity_Bindings_hpp
#define SoundFiler_Unity_Bindings_hpp

#include "SoundFiler.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API pd::SoundFiler* SoundFiler_allocate0();
UNITY_API void SoundFiler_free0(pd::SoundFiler*);
UNITY_API double SoundFiler_read0(pd::SoundFiler*, char* file);
UNITY_API void SoundFiler_write0(pd::SoundFiler*, char* fileName,
           unsigned int nChannels,
           long type,
           long format,
           double* array,
           long count);

UNITY_API void SoundFiler_getArray0(pd::SoundFiler*, int size, double* output);


#ifdef __cplusplus
}
#endif


#endif /* SoundFiler_Unity_Bindings_hpp */

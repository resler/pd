//
//  BiQuad_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#ifndef BiQuad_Unity_Bindings_hpp
#define BiQuad_Unity_Bindings_hpp

#include "BiQuad.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API pd::BiQuad* BiQuad_allocate0();
UNITY_API void BiQuad_free0 (pd::BiQuad*);
UNITY_API double BiQuad_perform0(pd::BiQuad*, double );
UNITY_API void BiQuad_setCoefficients0(pd::BiQuad*, double fb1, double fb2, double ff1, double ff2, double ff3);
UNITY_API void BiQuad_set0(pd::BiQuad*, double a, double b);

#ifdef __cplusplus
}
#endif

#endif /* BiQuad_Unity_Bindings_hpp */

//
//  TabRead4_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/24/23.
//

#include "TabRead4_Unity_Bindings.hpp"

pd::TabRead4* TabRead4_allocate0() {
    pd::TabRead4* tabread = new pd::TabRead4();
    return tabread;
}
void TabRead4_free0(pd::TabRead4* ptr) {
    pd::TabRead4* tabread = ptr;
    if(tabread !=NULL)
    {
        delete tabread;
        tabread = nullptr;
    }
}

double TabRead4_perform0(pd::TabRead4* ptr, double index) {
    pd::TabRead4* tabread = ptr;
    return tabread->perform(index);
}

void TabRead4_setTable0(pd::TabRead4* ptr, double* table, long size) {
    pd::TabRead4* tabread = ptr;

    std::vector<double> t;
    for (int i = 0; i < size; i++)
    {
        t.push_back(table[i]);
    }
    tabread->setTable(t);
}

size_t TabRead4_getTableSize0(pd::TabRead4* ptr) {
    pd::TabRead4* tabread = ptr;
    return tabread->getTableSize();
}

void TabRead4_setOnset0(pd::TabRead4* ptr, double onset) {
    pd::TabRead4* tabread = ptr;
    tabread->setOnset(onset);
}

double TabRead4_getOnset0(pd::TabRead4* ptr) {
    pd::TabRead4* tabread = ptr;
    return tabread->getOnset();
}

//
//  Threshold_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#ifndef Threshold_Unity_Bindings_hpp
#define Threshold_Unity_Bindings_hpp

#include "Threshold.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

    UNITY_API pd::Threshold* Threshold_allocate0();
    UNITY_API void Threshold_free0(pd::Threshold*);
    UNITY_API int Threshold_perform0(pd::Threshold*, double input); //returns 1 if you went above the threshold, or 0 if you went below the low threshold. -1 means neither, or that nothing has happened yet.
    UNITY_API void Threshold_setValues0(pd::Threshold*, double ht, double hd, double lt, double ld);
    UNITY_API void Threshold_setState0(pd::Threshold*, int s);

#ifdef __cplusplus
}
#endif
#endif /* Threshold_Unity_Bindings_hpp */

//
//  ComplexPole_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#include "ComplexPole_Unity_Bindings.hpp"

pd::ComplexPole* ComplexPole_allocate0() {
    pd::ComplexPole* cpole = new pd::ComplexPole();
    return cpole;
}

void ComplexPole_free0(pd::ComplexPole* ptr) {
    pd::ComplexPole* cpole = ptr;
    if(cpole !=NULL)
    {
        delete cpole;
        cpole = nullptr;
    }
}

complexOutput ComplexPole_perform0(pd::ComplexPole* ptr, double in, double imag, double rcoef, double icoef) {
    pd::ComplexPole* cpole = ptr;
    return cpole->perform(in, imag, rcoef, icoef);
}

void ComplexPole_set0(pd::ComplexPole* ptr, double Complex, double imaginary) {
    pd::ComplexPole* cpole = ptr;
    cpole->set(Complex, imaginary);
}

void ComplexPole_clear0(pd::ComplexPole* ptr) {
    pd::ComplexPole* cpole = ptr;
    cpole->clear();
}

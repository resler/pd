//
//  Phasor_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#ifndef Phasor_Unity_Bindings_hpp
#define Phasor_Unity_Bindings_hpp

#include "Phasor.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API pd::Phasor* Phasor_allocate0();
UNITY_API void Phasor_free0(pd::Phasor*);
UNITY_API double Phasor_perform0(pd::Phasor*, double input);
UNITY_API void Phasor_setPhase0(pd::Phasor*, double);
UNITY_API double Phasor_getPhase0(pd::Phasor*);
UNITY_API void Phasor_setFrequency0(pd::Phasor*, double);
UNITY_API double Phasor_getFrequency0(pd::Phasor*);
UNITY_API void Phasor_setVolume0(pd::Phasor*, double);
UNITY_API double Phasor_getVolume0(pd::Phasor*);

#ifdef __cplusplus
}
#endif

#endif /* Phasor_Unity_Bindings_hpp */

//
//  BandPass_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/23/23.
//

#include "BandPass_Unity_Bindings.hpp"

pd::BandPass* BandPass_allocate0() {
    pd::BandPass* bp = new pd::BandPass();
    return bp;
}

void BandPass_free0(pd::BandPass* ptr) {
    pd::BandPass* bp = ptr;
    if(bp !=NULL)
    {
        delete bp;
        bp = nullptr;
    }
}

double BandPass_perform0(pd::BandPass* ptr, double cf) {
    pd::BandPass* bp = ptr;
    return bp->perform(cf);
}

void BandPass_setCenterFrequency0(pd::BandPass* ptr, double cf) {
    pd::BandPass* bp = ptr;
    bp->setCenterFrequency(cf);
}

void BandPass_setQ0(pd::BandPass* ptr, double q) {
    pd::BandPass* bp = ptr;
    bp->setQ(q);
}

void BandPass_clear0(pd::BandPass* ptr) {
    pd::BandPass* bp = ptr;
    bp->clear();
}

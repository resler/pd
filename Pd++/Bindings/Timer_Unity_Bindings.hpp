//
//  Timer_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/26/23.
//

#ifndef Timer_Unity_Bindings_hpp
#define Timer_Unity_Bindings_hpp

#include "Timer.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API rwe::Timer* Timer_allocate0();
UNITY_API void Timer_free0(rwe::Timer*);
UNITY_API double Timer_perform0(rwe::Timer*);
UNITY_API void Timer_start0(rwe::Timer*);
UNITY_API void Timer_stop0(rwe::Timer*);
UNITY_API void Timer_reset0(rwe::Timer*);

#ifdef __cplusplus
}
#endif


#endif /* Timer_Unity_Bindings_hpp */

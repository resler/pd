//
//  Convolution_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/26/23.
//

#include "Convolution_Unity_Bindings.hpp"

rwe::Convolution* Convolution_allocate0() {
    rwe::Convolution* convo = new rwe::Convolution();
    return convo;
}

void Convolution_free0(rwe::Convolution* ptr) {
    rwe::Convolution* convo = ptr;
    if(convo !=NULL)
    {
        delete convo;
        convo = nullptr;
    }
}

double Convolution_perform0(rwe::Convolution* ptr, double filter, double control) {
    rwe::Convolution* convo = ptr;
    return convo->perform(filter, control);
}

void Convolution_setSquelch0(rwe::Convolution* ptr, int sq) {
    rwe::Convolution* convo = ptr;
    convo->setSquelch(sq);
}

int Convolution_getSquelch0(rwe::Convolution* ptr) {
    rwe::Convolution* convo = ptr;
    return convo->getSquelch();
}

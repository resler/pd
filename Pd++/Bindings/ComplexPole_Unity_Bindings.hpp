//
//  ComplexPole_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#ifndef ComplexPole_Unity_Bindings_hpp
#define ComplexPole_Unity_Bindings_hpp

#include "RawFilters.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API pd::ComplexPole* ComplexPole_allocate0();
UNITY_API void ComplexPole_free0(pd::ComplexPole*);
UNITY_API complexOutput ComplexPole_perform0(pd::ComplexPole*, double, double, double, double);
UNITY_API void ComplexPole_set0(pd::ComplexPole*, double Complex, double imaginary);
UNITY_API void ComplexPole_clear0(pd::ComplexPole*);

#ifdef __cplusplus
}
#endif

#endif /* ComplexPole_Unity_Bindings_hpp */

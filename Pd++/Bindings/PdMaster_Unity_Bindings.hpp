//
//  PdMaster_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/17/23.
//

#ifndef PdMaster_Unity_Bindings_hpp
#define PdMaster_Unity_Bindings_hpp

#include <stdio.h>
#include "PdMaster.h"
#include "Dummy.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API 
#endif

#ifdef __cplusplus
extern "C" {
#endif

    UNITY_API Dummy* PdMaster_allocate0();
    UNITY_API void PdMaster_free0(Dummy* ptr);
    UNITY_API void PdMaster_setSampleRate0(Dummy* ptr, unsigned long sr);
    UNITY_API unsigned long PdMaster_getSampleRate0(Dummy* ptr);
    UNITY_API void PdMaster_setBlockSize0(Dummy* ptr, int b);
    UNITY_API int PdMaster_getBlockSize0(Dummy* ptr);
    UNITY_API void PdMaster_setEndian0(Dummy* ptr);
    UNITY_API int PdMaster_getEndian0(Dummy* ptr);

    /*Converts samples into ms.*/
    UNITY_API double PdMaster_getTimeInSampleTicks0(Dummy* ptr);
    /*Converts milliseconds into samples/ms*/
    UNITY_API long PdMaster_getTimeInMilliSeconds0(Dummy* ptr, double time);

    /* A denormaling routine using C++'s numerics.*/
    UNITY_API int PdMaster_pdBigOrSmall0(Dummy* ptr, double f);

    /* Pure Data's denormaling routine*/
    UNITY_API int PdMaster_PD_BIGORSMALL0(Dummy* ptr, float f);

    UNITY_API void PdMaster_setFFTWindow0(Dummy* ptr, int);
    UNITY_API int PdMaster_getFFTWindow0(Dummy* ptr);

    /*acoustic conversions live here*/
    UNITY_API double PdMaster_mtof0(Dummy* ptr, double n); // MIDI note number to frequency
    UNITY_API double PdMaster_ftom0(Dummy* ptr, double f); // Frequency to MIDI note number
    UNITY_API double PdMaster_powtodb0(Dummy* ptr, double n);
    UNITY_API double PdMaster_dbtopow0(Dummy* ptr, double db);
    UNITY_API double PdMaster_rmstodb0(Dummy* ptr, double n);  // RMS (e.g 0-1) to dB (0-100)
    UNITY_API double PdMaster_dbtorms0(Dummy* ptr, double db);

#ifdef __cplusplus
}
#endif

#endif /* PdMaster_Unity_Bindings_hpp */
//
//  RealPole_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#ifndef RealPole_Unity_Bindings_hpp
#define RealPole_Unity_Bindings_hpp

#include "RawFilters.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API pd::RealPole* RealPole_allocate0();
UNITY_API void RealPole_free0(pd::RealPole*);
UNITY_API double RealPole_perform0(pd::RealPole*, double, double);
UNITY_API void RealPole_set0(pd::RealPole*, double real, double imaginary);
UNITY_API void RealPole_clear0(pd::RealPole*);

#ifdef __cplusplus
}
#endif

#endif /* RealPole_Unity_Bindings_hpp */

//
//  ComplexZeroReverse_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#ifndef ComplexZeroReverse_Unity_Bindings_hpp
#define ComplexZeroReverse_Unity_Bindings_hpp

#include "RawFilters.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API pd::ComplexZeroReverse* ComplexZeroReverse_allocate0();
UNITY_API void ComplexZeroReverse_free0(pd::ComplexZeroReverse*);
UNITY_API complexOutput ComplexZeroReverse_perform0(pd::ComplexZeroReverse*, double, double, double, double);
UNITY_API void ComplexZeroReverse_set0(pd::ComplexZeroReverse*, double Complex, double imaginary);
UNITY_API void ComplexZeroReverse_clear0(pd::ComplexZeroReverse*);

#ifdef __cplusplus
}
#endif

#endif /* ComplexZeroReverse_Unity_Bindings_hpp */

//
//  Envelope_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#ifndef Envelope_Unity_Bindings_hpp
#define Envelope_Unity_Bindings_hpp

#include "Envelope.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API pd::Envelope* Envelope_allocate0();
UNITY_API pd::Envelope* Envelope_allocate1(int ws, int p);
UNITY_API void Envelope_free0(pd::Envelope*);
UNITY_API double Envelope_perform0(pd::Envelope*, double input);


#ifdef __cplusplus
}
#endif

#endif /* Envelope_Unity_Bindings_hpp */

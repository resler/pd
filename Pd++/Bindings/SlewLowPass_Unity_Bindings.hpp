//
//  SlewLowPass_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#ifndef SlewLowPass_Unity_Bindings_hpp
#define SlewLowPass_Unity_Bindings_hpp

#include "SlewLowPass.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API pd::SlewLowPass* SlewLowPass_allocate0();
UNITY_API void SlewLowPass_free0(pd::SlewLowPass*);
UNITY_API double SlewLowPass_perform0(pd::SlewLowPass*, double input, double freq, double posLimitIn, double posFreqIn, double negLimitIn, double negFreqIn );
UNITY_API void SlewLowPass_set0(pd::SlewLowPass*, double last);

#ifdef __cplusplus
}
#endif

#endif /* SlewLowPass_Unity_Bindings_hpp */

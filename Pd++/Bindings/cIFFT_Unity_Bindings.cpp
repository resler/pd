//
//  cIFFT_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/24/23.
//

#include "cIFFT_Unity_Bindings.hpp"

pd::complexIFFT* cIFFT_allocate0() {
    pd::complexIFFT* ifft = new pd::complexIFFT();
    return ifft;
}

void cIFFT_free0(pd::complexIFFT* ptr) {
    pd::complexIFFT* ifft = ptr;
    if(ifft !=NULL)
    {
        delete ifft;
        ifft = nullptr;
    }
}

complexFFTOutput cIFFT_perform0(pd::complexIFFT* ptr, double *input) {
    pd::complexIFFT* ifft = ptr;
    double *temp = ifft->perform(input);//returns real and imaginary values, size = 2;
    complexFFTOutput f;
    f.real = temp[0];
    f.imaginary = temp[1];
    
    return f;
}

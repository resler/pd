//
//  ReadSoundFile_Test_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/26/23.
//

#include "ReadSoundFile_Unity_Bindings.hpp"

pd::ReadSoundFile_Test* ReadSoundFile_allocate0() {
    pd::ReadSoundFile_Test* readsf = new pd::ReadSoundFile_Test();
    return readsf;
}

void ReadSoundFile_free0(pd::ReadSoundFile_Test* ptr) {
    pd::ReadSoundFile_Test* readsf = ptr;
    if(readsf !=NULL)
    {
        delete readsf;
        readsf = nullptr;
    }
}

void ReadSoundFile_open0(pd::ReadSoundFile_Test* ptr, char* file, double onset) {
    pd::ReadSoundFile_Test* readsf = ptr;
    std::string f = file;
    readsf->open(f, onset);
}

void ReadSoundFile_start0(pd::ReadSoundFile_Test* ptr, double* output) {
    pd::ReadSoundFile_Test* readsf = ptr;
    double* temp = readsf->start();
    for(int i = 0; i < readsf->getBufferSize(); i++)
    {
        output[i] = temp[i];
    }
    
}

void ReadSoundFile_stop0(pd::ReadSoundFile_Test* ptr) {
    pd::ReadSoundFile_Test* readsf = ptr;
    readsf->stop();
}

void ReadSoundFile_print0(pd::ReadSoundFile_Test* ptr) {
    pd::ReadSoundFile_Test* readsf = ptr;
    readsf->print();
}

void ReadSoundFile_setBufferSize0(pd::ReadSoundFile_Test* ptr, int bytes) {
    pd::ReadSoundFile_Test* readsf = ptr;
    readsf->setBufferSize(bytes);
}

int ReadSoundFile_getBufferSize0(pd::ReadSoundFile_Test* ptr) {
    pd::ReadSoundFile_Test* readsf = ptr;
    return readsf->getBufferSize();
}

int ReadSoundFile_getChannels0(pd::ReadSoundFile_Test* ptr) {
    pd::ReadSoundFile_Test* readsf = ptr;
    return readsf->getChannels();//must be called after open
}

bool ReadSoundFile_isComplete0(pd::ReadSoundFile_Test* ptr) {
    pd::ReadSoundFile_Test* readsf = ptr;
    return readsf->isComplete();
}

//
//  TabRead4_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#ifndef TabRead4_Unity_Bindings_hpp
#define TabRead4_Unity_Bindings_hpp

#include "TabRead4.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API pd::TabRead4* TabRead4_allocate0();
UNITY_API void TabRead4_free0(pd::TabRead4*);
UNITY_API double TabRead4_perform0(pd::TabRead4*, double index);
UNITY_API void TabRead4_setTable0(pd::TabRead4*, double* table, long size);
UNITY_API size_t TabRead4_getTableSize0(pd::TabRead4*);
UNITY_API void TabRead4_setOnset0(pd::TabRead4*, double);
UNITY_API double TabRead4_getOnset0(pd::TabRead4*);

#ifdef __cplusplus
}
#endif

#endif /* TabRead4_Unity_Bindings_hpp */

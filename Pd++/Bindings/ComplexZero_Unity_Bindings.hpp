//
//  ComplexZero_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#ifndef ComplexZero_Unity_Bindings_hpp
#define ComplexZero_Unity_Bindings_hpp

#include "RawFilters.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API pd::ComplexZero* ComplexZero_allocate0();
UNITY_API void ComplexZero_free0(pd::ComplexZero*);
UNITY_API complexOutput ComplexZero_perform0(pd::ComplexZero*, double, double, double, double);
UNITY_API void ComplexZero_set0(pd::ComplexZero*, double Complex, double imaginary);
UNITY_API void ComplexZero_clear0(pd::ComplexZero*);

#ifdef __cplusplus
}
#endif

#endif /* ComplexZero_Unity_Bindings_hpp */

//
//  LowPass_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Robert Esler on 10/24/23.
//

#ifndef LowPass_Unity_Bindings_hpp
#define LowPass_Unity_Bindings_hpp

#include "LowPass.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API pd::LowPass* LowPass_allocate0();
UNITY_API void LowPass_free0(pd::LowPass* );
UNITY_API double LowPass_perform0(pd::LowPass*, double input);
UNITY_API void LowPass_setCutoff0(pd::LowPass*, double cutoff);
UNITY_API double LowPass_getCutoff0(pd::LowPass* );
UNITY_API void LowPass_clear0(pd::LowPass* );

#ifdef __cplusplus
}
#endif

#endif /* LowPass_Unity_Bindings_hpp */

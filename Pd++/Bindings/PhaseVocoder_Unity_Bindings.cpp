//
//  PhaseVocoder_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 6/7/2024.
//

#include "PhaseVocoder_Unity_Bindings.hpp"

rwe::PhaseVocoder* PhaseVocoder_allocate0() {
	rwe::PhaseVocoder* pvoc = new rwe::PhaseVocoder();
	return pvoc;
}

void PhaseVocoder_free0(rwe::PhaseVocoder* ptr) {
	rwe::PhaseVocoder* pvoc = ptr;
	if (pvoc != nullptr)
	{
		delete pvoc;
		pvoc = nullptr;
	}
}

double PhaseVocoder_perform0(rwe::PhaseVocoder* ptr) {
	rwe::PhaseVocoder* pvoc =ptr;
	return pvoc->perform();
}

void PhaseVocoder_inSample(rwe::PhaseVocoder* ptr, char* file) {
	rwe::PhaseVocoder* pvoc = ptr;
	std::string f = file;
	pvoc->inSample(f);
}

void PhaseVocoder_setSpeed0(rwe::PhaseVocoder* ptr, double s) {
	rwe::PhaseVocoder* pvoc = ptr;
	pvoc->setSpeed(s);
}

void PhaseVocoder_setTranspo0(rwe::PhaseVocoder* ptr, double t) {
	rwe::PhaseVocoder* pvoc = ptr;
	pvoc->setTranspo(t);
}

void PhaseVocoder_setLock0(rwe::PhaseVocoder* ptr, int l) {
	rwe::PhaseVocoder* pvoc = ptr;
	pvoc->setLock(l);
}

void PhaseVocoder_setRewindd0(rwe::PhaseVocoder* ptr) {
	rwe::PhaseVocoder* pvoc = ptr;
	pvoc->setRewind();
}
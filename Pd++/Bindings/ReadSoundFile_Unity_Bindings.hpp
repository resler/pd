//
//  ReadSoundFile_Test_Unity_Bindings.hpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/26/23.
//

#ifndef ReadSoundFile_Test_Unity_Bindings_hpp
#define ReadSoundFile_Test_Unity_Bindings_hpp

#include "ReadSoundFile_Test.hpp"
#include "FileWvIn.h"

#ifdef UNITY_EXPORTS
#define UNITY_API __declspec(dllexport)
#else
#define UNITY_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

UNITY_API pd::ReadSoundFile_Test* ReadSoundFile_allocate0();
UNITY_API void ReadSoundFile_free0(pd::ReadSoundFile_Test*);
UNITY_API void ReadSoundFile_open0(pd::ReadSoundFile_Test*, char* file, double onset);
UNITY_API void ReadSoundFile_start0(pd::ReadSoundFile_Test*, double* output);
UNITY_API void ReadSoundFile_stop0(pd::ReadSoundFile_Test*);
UNITY_API void ReadSoundFile_print0(pd::ReadSoundFile_Test*);
UNITY_API void ReadSoundFile_setBufferSize0(pd::ReadSoundFile_Test*, int bytes);
UNITY_API int ReadSoundFile_getBufferSize0(pd::ReadSoundFile_Test*);
UNITY_API int ReadSoundFile_getChannels0(pd::ReadSoundFile_Test*);
UNITY_API bool ReadSoundFile_isComplete0(pd::ReadSoundFile_Test*);

#ifdef __cplusplus
}
#endif

#endif /* ReadSoundFile_Test_Unity_Bindings_hpp */

//
//  RealPole_Unity_Bindings.cpp
//  pdplusplusUnity
//
//  Created by Klewen on 10/24/23.
//

#include "RealPole_Unity_Bindings.hpp"

pd::RealPole* RealPole_allocate0() {
    pd::RealPole* rpole = new pd::RealPole();
    return rpole;
}

void RealPole_free0(pd::RealPole* ptr) {
    pd::RealPole* rpole = ptr;
    if(rpole !=NULL)
    {
        delete rpole;
        rpole = nullptr;
    }
}

double RealPole_perform0(pd::RealPole* ptr, double in, double coef) {
    pd::RealPole* rpole = ptr;
    return rpole->perform(in, coef);
}

void RealPole_set0(pd::RealPole* ptr, double real, double imaginary) {
    pd::RealPole* rpole = ptr;
    rpole->set(real, imaginary);
}

void RealPole_clear0(pd::RealPole* ptr) {
    pd::RealPole* rpole = ptr;
    rpole->clear();
}

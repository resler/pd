#include "PdMaster_Unity_Bindings.hpp"

Dummy* PdMaster_allocate0() {
	Dummy* dummy = new Dummy();
	return dummy;
 }

void PdMaster_free0(Dummy* ptr) {
    Dummy* dummy = ptr;
    if (dummy != NULL)
    {
        delete dummy;
        dummy = nullptr;
    }
  }

void PdMaster_setSampleRate0(Dummy* ptr, unsigned long sr) {
    Dummy* dummy = ptr;
    dummy->setSampleRate(sr);
  }

unsigned long PdMaster_getSampleRate0(Dummy* ptr) {
    Dummy* dummy = ptr;
    return dummy->getSampleRate();
 }

void PdMaster_setBlockSize0(Dummy* ptr, int b) {
    Dummy* dummy = ptr;
    dummy->setBlockSize(b);
 }

int PdMaster_getBlockSize0(Dummy* ptr) {
    Dummy* dummy = ptr;
    return dummy->getBlockSize();
 }

void PdMaster_setEndian0(Dummy* ptr) {
    Dummy* dummy = ptr;
    dummy->setEndian();
 }

int PdMaster_getEndian0(Dummy* ptr) {
    Dummy* dummy = ptr;
    return dummy->getEndian();
  }

/*Converts samples into ms.*/
double PdMaster_getTimeInSampleTicks0(Dummy* ptr) {
    Dummy* dummy = ptr;
    return dummy->getTimeInSampleTicks();
 }

/*Converts milliseconds into samples/ms*/
long PdMaster_getTimeInMilliSeconds0(Dummy* ptr, double time) {
    Dummy* dummy = ptr;
    return dummy->getTimeInMilliSeconds(time);
 }

/* A denormaling routine using C++'s numerics.*/
int PdMaster_pdBigOrSmall0(Dummy* ptr, double f) {
    Dummy* dummy = ptr;
    return dummy->pdBigOrSmall(f);
 }

/* Pure Data's denormaling routine*/
int PdMaster_PD_BIGORSMALL0(Dummy* ptr, float f) {
    Dummy* dummy = ptr;
    return dummy->PD_BIGORSMALL(f);
 }

void PdMaster_setFFTWindow0(Dummy* ptr, int w) {
    Dummy* dummy = ptr;
    dummy->setFFTWindow(w);
 }

int PdMaster_getFFTWindow0(Dummy* ptr) {
    Dummy* dummy = ptr;
    return dummy->getFFTWindow();
 }

/*acoustic conversions live here*/
double PdMaster_mtof0(Dummy* ptr, double n) {
    Dummy* dummy = ptr;
    return dummy->mtof(n);
 }

double PdMaster_ftom0(Dummy* ptr, double f) {
    Dummy* dummy = ptr;
    return dummy->ftom(f);
  }

double PdMaster_powtodb0(Dummy* ptr, double n) {
    Dummy* dummy = ptr;
    return dummy->powtodb(n);
}

double PdMaster_dbtopow0(Dummy* ptr, double db) {
    Dummy* dummy = ptr;
    return dummy->dbtopow(db);
 }

double PdMaster_rmstodb0(Dummy* ptr, double n) {
    Dummy* dummy = ptr;
    return dummy->rmstodb(n);
 }

double PdMaster_dbtorms0(Dummy* ptr, double db) {
    Dummy* dummy = ptr;
    return dummy->dbtorms(db);
 }
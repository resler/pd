/* This class is where you put all your audio routines, etc.
  PdAlgorithm.cpp
  Pd++

  This class emulates a Pd Patch.  Your algorithm or patch code would 
  be simulated in the runAlgorithm(double, double) function.  
  Subpatches and abstractions could be member functions.
  Abstractions and externals could be separate classes.
  The runAlgorithm receives audio input and writes the output
  to the Buffer data type.  Buffer is defined in the superclass PdMaster.
  The audio I/O class paRender only calls the runAlgorithm function, so
  all your audio output data must end up in this function via objects
  or member functions.

  NOTE: It's best not to write to the stdout (e.g std::cout, or printf()) in
  your runAlgorithm function, unless you are using mainNoAudio.cpp. It could
  cause audio interruptions.
 
  Created by Robert Esler on 9/17/14.
  Copyright (c) 2014 Robert Esler. All rights reserved.
*/

#include "PdAlgorithm.h"
#include <iostream>
#define MAX_ARRAY_SIZE (1<<24) //2^24

PdAlgorithm::PdAlgorithm() {
   
    /*Initialize stuff here*/
   
    /*
    bob.setOversampling(4);
    bob.setSaturation(3);
    bob.setCutoffFrequency(400);
    bob.setResonance(1);
    */
    
    /*
    // Initializes a table for use with TabRead4
    int s = 256;
    for (int i = 0; i < s+3; i++) // notice the added 3 points for interpolation!
        test.push_back( .3 * sin(2 * 3.14159265358979 * i/s) ); // sine wave
    
    tabRead4.setTable(test);
    */
    
    /*
    // create a new array for file output, notice the max size in #define
     outFile = new double[MAX_ARRAY_SIZE];
    */
    
    /*read an audio file
    arraySize = soundfiler.read("/../../../../../441Hz-stereo.wav"); //this is the read method
    audioFileData = soundfiler.getArray(); // this is your array.
    */
    pvoc.inSample("C:\\Users\\rwe8\\Documents\\Pd4P3\\Pd4P3\\examples\\Analysis\\PhaseVocoder\\data\\voice.wav");
}

PdAlgorithm::~PdAlgorithm() {
    
    /*
    // write audio data here, make sure you created outFile in the constructor.
    for (int i = 0; i < count; i++)
        audioFileData.push_back(outFile[i]);
    
    delete outFile;
    //soundfiler.write("/../../../../../testSoundFiler.wav", 2, stk::FileWrite::FILE_WAV, stk::Stk::STK_SINT16, audioFileData);
    */
    
}

/*! This is where the actual algorithm gets called by paRender
 you can use other non-static functions of your class but 
 paRender only cares about the Buffer that is returned from
 this function.  Put any functions here that need to be updated.
 Be wary of using locking or wait routines, allocating or deallocating memory. */

Buffer PdAlgorithm::runAlgorithm(double input1, double input2) {
    
    Buffer theBuffer;
    
    /*This is the audio input stream*/
    theBuffer.inbuf1 = input1;
    theBuffer.inbuf2 = input2;
    
    double signal = 0;
    double signal2 = 0;
    
    
    /*put your audio objects here, initialize them in the constructor.
      Use the destructor to dealloc and write out buffers, etc.*/
    
    /*
     I left much of my test code commented out below to show some simple
     examples of how to use the classes.
     Just make sure to comment out the test tone below.
     */
    
   
    //test tone
    /*
    signal = osc.perform(200);
    signal2 = signal;
    */
    signal = pvoc.perform();
    signal2 = signal;
    /*
    //Basic test of Sigmund, remember don't print to the iostream with audio.
    //A better way to use this is with an if statement
    sigmundOut = sigmund.perform(signal);
    std::cout << sigmundOut.pitch << " | " << sigmundOut.envelope << std::endl;
    */
    
     /*
     //Test using BobFilter
    double noyz = noise.perform();
    
    double rez = 0;
    double freak = 400;
    if(!bobCtl)
    {
        rez = line.perform(1, 1000);
        freak = line2.perform(1000, 1000);
    }
    if(rez == 1)
        bobCtl = true;
    if(bobCtl)
    {
        rez = line.perform(8, 1000);
        freak = line2.perform(400, 1000);
    }
    if(rez == 8)
        bobCtl = false;

    
    bob.setResonance(rez);
    bob.setCutoffFrequency(freak);
    signal = bob.perform(noyz);
    */

    
    /*
    //Envelope follower, won't work well with audio on.  
    double envOutput = env.perform(signal);
    std::cout << envOutput << std::endl;
    */
    
    /*
     // An example of time based functions.
     freqSweep = line.perform(1000, 5000);
     signal = osc.perform(freqSweep);
    */
    
    /*
    vcf.setQ(10000);
    pd::vcfOutput filter = vcf.perform(noise.perform(), 880);
    lop.setCutoff(1000);
    signal = lop.perform((filter.real + filter.imaginary) * line.perform(60, 50)) *
            (osc.perform(.25)*.3) + .5;
    signal2 = signal;
     */
     
    /*
     //another version of a test tone.
    signal = cos.perform(phasor.perform(300));
    signal2 = signal; // mono output
    */
    
    /*
    // variable delay example showing doppler
    double n = osc2.perform(689.0625);
    double d = (osc.perform(5) + 10) * 3;
    vd.delayWrite(n);
    signal = .2*vd.perform(d);
    signal2 = signal;
    */
  
    /*
     // using TabRead4 as a wavetable oscillator
    signal = tabRead4.perform(tabRead4Counter++);
    if (tabRead4Counter == test.size()-2) tabRead4Counter = 1;
    */
    
    /*
    // read an audio file
    if(count != arraySize)
    {
        signal = audioFileData[count++];
        signal2 = audioFileData[count++];
    }
    */
    
    /*
    // write to an array, see deconstructor for write to file method
    signal = sine[0].perform(500);
    signal2 = sine[1].perform(750)*.5;
    
    //Don't use a vector here, it creates audio dropouts, instead just use an array.
    //This is the array that will be used for file output. This would be an example using stereo.
    outFile[count++] = signal;
    outFile[count++] = signal2;
    */
    
    /*
    // complexFFT test, converts to frequency domain and back to time domain.
     double *fftOutput;
     signal = osc.perform(440);
     fftOutput = ifft.perform(fft.perform(signal, 0));
     
     signal = (fftOutput[0])/this->getFFTWindow();
     signal2 = (fftOutput[1])/this->getFFTWindow();
     
     signal *= .1;
     signal2 *= .1;
    */
    
    /*
    // SSB test using mic input
    freqSweep = ( ( osc.perform(.2) * 350 ) );
    ssb.setOscFreq(freqSweep);
    signal = ssb.perform( input1 );
    */
   
    /*write output to buf.outbufL, buf.outbufR*/
    theBuffer.outbufL =  signal;
    theBuffer.outbufR =  signal2;
    
     return theBuffer;
    
}

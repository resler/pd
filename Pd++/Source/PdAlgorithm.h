/*! \brief This class is where you put all your audio routines, etc.
 
 This class emulates a Pd Patch.  Your algorithm or patch code would
 be simulated in the runAlgorithm(double, double) function.
 Subpatches and abstractions could be member functions.
 Abstractions and externals could be separate classes.
 The runAlgorithm receives audio input and writes the output
 to the Buffer data type.  Buffer is defined in the superclass PdMaster.
 The audio I/O class paRender only calls the runAlgorithm function, so
 all your audio output data must end up in this function via objects
 or member functions.
 
 NOTE: It's best not to write to the stdout (e.g std::cout, or printf()) in
 your runAlgorithm function, unless you are using mainNoAudio.cpp. It could
 cause audio interruptions.
 
 Created by Robert Esler on 9/17/14.
 Copyright (c) 2014 Robert Esler. All rights reserved.
 */

#ifndef __Pd____PdAlgorithm__
#define __Pd____PdAlgorithm__

#include "PdMaster.h"
#include "MasterObjectList.h"
#include <cmath>

/*A simulation of a Pd Patch, this is where your audio code will be placed.*/
class PdAlgorithm : public PdMaster {
    
    /*Pd++ objects created here, plus other private members*/
private:
    
    rwe::SineWave sine[6];
    rwe::SSB ssb;
    rwe::WhiteNoise myWhiteNoise;
    rwe::Metro metro;
    rwe::Timer timer;
    
    pd::SoundFiler soundfiler;
    std::vector<double> audioFileData;
    double *outFile;
    pd::realFFT rFFT;
    pd::realIFFT rIFFT;
    pd::Line line;
    pd::Line line2;
    pd::TabRead4 tabRead4;
    pd::Noise noise;
    pd::Cosine cos;
    pd::Phasor phasor;
    pd::Oscillator osc;
    pd::Delay delay;
    pd::VariableDelay vd;
    pd::VoltageControlFilter vcf;
    pd::LowPass lop;
    pd::Envelope env;
    pd::Threshold threshold;
    pd::BobFilter bob;
    pd::Sigmund sigmund;
    rwe::PhaseVocoder pvoc;

    sigmundPackage sigmundOut;
    bool bobCtl = false;
    double *windOutput;
    double freqSweep;
    double feedBack = 0;
    double arraySize = 0;
    double f = 441;
    int tabRead4Counter = 1;
    int count = 0;
    
public:
    
    std::vector<double> test;
    
    PdAlgorithm();
    ~PdAlgorithm();
    
    /*algorithm goes here
    audio input is passed from paRender*/
    Buffer runAlgorithm(double input1, double input2);
    
};

#endif /* defined(__Pd____PdAlgorithm__) */

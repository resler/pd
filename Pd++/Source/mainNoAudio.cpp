/*!
 mainNoAudio.cpp
 Pd++
 
 This is meant for testing your algorithm and/or Pd++ classes
 without the audio I/O.  If you want audio right now use
 main.cpp.
 
 It's safe to call your PdAlgorithm here, only tests output.
 
 Created by Robert Esler on 9/16/14.
 Copyright (c) 2014 Robert Esler. All rights reserved.
 */

#include <iostream>
#include "PdAlgorithm.h"



int main(int argc, const char * argv[])
{
    PdAlgorithm algor1; //runs the Al Gore Rhythm
    Buffer buffer;
    
    for (int i = 0; i < 88200; i++)
    {
        buffer = algor1.runAlgorithm(0, 0);
        //std::cout << i << ": " << buffer.outbufL << " || " << buffer.outbufR << std::endl;
    }

}
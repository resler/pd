/*!
  main.cpp
  Pd++
 
  This is meant for testing your algorithm and/or Pd++ classes
  with the audio I/O.  If you don't want audio right now use
  mainNoAudio.cpp. 
  Don't call Pd++ classes or PdAlgorithm in main(), use PdAlgorithm.cpp
  Created by Robert Esler on 9/16/14.
  Copyright (c) 2014 Robert Esler. All rights reserved.

 */

#include <iostream>
#include "stdio.h"
#include "paRender++.h"

int main(int argc, const char * argv[])
{
    
    /*port audio data types start with Pa__*/
    PaError err;
    /*Our audio I/O object*/
    paRender audio;
    const char quit = 'q';
    char input;
    
    err = Pa_Initialize();
    if( err != paNoError ) audio.sendError(err);
    
    if (audio.open(Pa_GetDefaultOutputDevice(), Pa_GetDefaultInputDevice()))
    {
        if (audio.start())
        {
            std::cout << "playing audio...Enter 'q' to quit: ";
            std::cin >> input;
            while (input != quit)
            {
                Pa_Sleep(1000 );
                std::cout << ".";
                
            }
            
            audio.stop();
        }
        
        audio.close();
    }
    
   
    printf("Test finished.\n");
    
    
    return err;
    
   
}



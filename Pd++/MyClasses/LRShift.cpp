#include "LRShift.h"

namespace rwe {

	LRShift::LRShift() {
		int ws = this->getFFTWindow();
		tmp = new (std::nothrow) double[ws] {};
		tmpIn = new (std::nothrow) double[ws] {};
	}

	LRShift::LRShift(int ws) {
		tmp = new (std::nothrow) double[ws] {};
		tmpIn = new (std::nothrow) double[ws] {};
	}

	LRShift::~LRShift() {
		delete[] tmp;
		delete[] tmpIn;
	}

	double* LRShift::perform(double* in, int shift) {

		//evaluate shift is not greater than window size (+/-)
		if (shift > this->getFFTWindow())
		{
			shift = this->getFFTWindow();
		}
		if (shift < this->getFFTWindow() * -1)
		{
			shift = this->getFFTWindow() * -1;
		}


		if (shift > 0)
		{
			//zero out the edge bins
			for (int i = this->getFFTWindow() - 1; i > this->getFFTWindow() + shift; i--)
				tmp[i] = 0;
			
				//copy left
			for (int i = 0; i < this->getFFTWindow() - shift; i++)
			{
				tmp[i] = in[i + shift];
			}
		}

		if (shift < 0)
		{

			//zero out the edge bins
			int n = shift * -1;
			for (int i = 0; i < n; i++)
				tmp[i] = 0;
			
				//copy right 
			for (int i = 0; i < this->getFFTWindow() + shift; i++)
			{
				tmp[i - shift] = in[i];
			}
		}

		if (shift == 0)
		{
			tmp = in;
			std::cout << "You didn't shift your block. Use a number greater than or less than 0" << std::endl;
		}

		return tmp;
	}

}
/*
 Sawtooth.cpp
 Pd++
 
 Created by Robert Esler on 9/17/14.
 Copyright (c) 2014 Robert Esler. All rights reserved.
 */

#ifndef __Pd____Sawtooth__
#define __Pd____Sawtooth__

#include <iostream>
#include "PdMaster.h"

namespace rwe {

/*! \brief A sawtooth wave generator.
    
 This is simple iteration of a sawtooth. Similar to phasor but uses
a much simpler routine.
     
*/
    
class Sawtooth : public PdMaster {
    
private:
    double samplesPerCycle;
    double frequency;
    double volume = 1;
    double iterator = 0;
    
public:
    Sawtooth();
    ~Sawtooth();
    double perform(double);
    void setFrequency(double);
    double getFrequency();
    void setVolume(double);
    double getVolume();
    
};

} // rwe namespace

#endif /* defined(__Pd____Sawtooth__) */

/*
 CosineWave.cpp
 Pd++
 
 Created by Esler,Robert Wadhams on 9/24/14.
 Copyright (c) 2014 Robert Esler. All rights reserved.
*/

#ifndef __Pd____CosineWave__
#define __Pd____CosineWave__

#include <iostream>

#include "PdMaster.h"
#include "Phasor.h"



namespace rwe {
    
/*! \brief A cosine wave generator.
     
     This generator uses the Pd++ class phasor to generate its
     ramp.
*/
    
class CosineWave : public PdMaster {
    
private:
    double factor = 0;
    int iterator = 0;
    double volume = 1;
    double multiplier = 0;
    double frequency = 0;
    double samplesPerCycle;
    double phase = 0;
    void setPhase(double );
    pd::Phasor phasor;
    
public:
    CosineWave();
    CosineWave(double, double);
    ~CosineWave();
    double perform(double );
    
    void setFrequency(double);
    double getFrequency() {return frequency;};
    void setVolume(double);
    double getVolume() {return volume;};
    
    
    
};

} //rwe namespace
#endif /* defined(__Pd____CosineWave__) */

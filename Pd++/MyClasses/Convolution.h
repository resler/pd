/*
Copyright Robert Esler 2021

This code is based on Miller Puckette's "Timbre Stamp" example in 
Pd.  It takes two windowd inputs, performs the FFT of both
then convolves their frequency bins, or stamps the control
timbre onto the filtered timbre.

*/

#ifndef __Rwe____Convolution__
#define __Rwe____Convolution__

#include "PdMaster.h"
#include "MasterObjectList.h"

namespace rwe {

	class Convolution : public PdMaster {

	private:
		pd::realFFT *rfft;//for our input, aka filter
		pd::realIFFT *rifft;
		pd::realFFT *rfft2;//for our 2nd input, aka control

		pd::Oscillator osc; //for our Hann window
		int squelch = 30;
		int overlap = 4;
		//window
		double *fft;
		double *fft2;
		double *convolution;
		double *hann;
		double *buffer;
		double *buffer2;

		//window/overlap
		double *in;
		double *cIn;
		
		//window
		double *ifft;
		double *ifftWas;
		
		long sampleCounter = 0;
		double* convolve(double* filter, double* control);
		void createHann();
		double clip(double a, double b, double c);
		float rsqrt(float x);

	public:
		Convolution();
		Convolution(int ws, int overlap);
		~Convolution();

		double perform(double filter, double control);
		void setSquelch(int sq) { squelch = sq; };
		int getSquelch() { return squelch; };
		

	};
}//end of namespace

#endif //(__Rwe____Convolution__)
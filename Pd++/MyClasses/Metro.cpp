/*
  Metro.cpp
  Pd++

  Created by Robert Esler on 10/29/14.
  Copyright (c) 2014 Robert Esler. All rights reserved.
*/

/* A simple metronome that accepts milliseconds and beats per minute.
 
  Metro in its default state is set to a millisecond delay. You can set the
  function setBPM(bool) to true if you would rather use bpm as your argument
  value.
 
 
*/

#include "Metro.h"

namespace rwe {

Metro::Metro() {
    
}

Metro::~Metro() {
    
}

bool Metro::perform(double time) {

    bool metro = false;
    bool bpm = Metro::getBPM();
    double delayInSamples = 0;
    
    if(time != prevTime)
    {
        //reset
        metroCounter = 0;
        metro = false;
    }
    
    if(bpm)
        delayInSamples = Metro::getSampleRate() * (60/time);
    else
        delayInSamples = Metro::getSampleRate() * (time/1000);
    
    if(metroCounter >= delayInSamples)
    {
        metro = true;
        metroCounter = 0;
    }
    
    prevTime = time;
    
    metroCounter++;
    
    return metro;
    
}

void Metro::setBPM(bool bpm ) {

    bpmIsSet = bpm;
    
}

} // rwe namespace

/*
  SineWave.cpp

  Created by Robert Esler on 5/10/14.
  Copyright (c) 2014 Robert Esler. All rights reserved.
*/

/* A sine wave generator.
 
   This generator uses the Pd++ class phasor to generate its
   ramp.
 
*/

#include "SineWave.h"
#include "math.h"

namespace rwe {

SineWave::SineWave() {
    
   
}

SineWave::~SineWave() {
    
}

/*Every Pd++ class should have a perform() function that outputs a double
 The input should simulate the inlets of the Pd object.
  If your object outputs more than one data stream, use a typedef struct*/
double SineWave::perform(double input) {
    
    double output=0;
    SineWave::setFrequency(input);
//    samplesPerCycle = SineWave::getSampleRate()/SineWave::getFrequency();
//    factor = (2*3.14159265358979)/samplesPerCycle;
    volume = SineWave::getVolume();
//    output = (volume * sin(iterator++*factor));
    output = (volume * sin(2 * 3.14159265358979 * phasor.perform(SineWave::getFrequency())));
    return output;
    
}

void SineWave::setFrequency(double freq) {
    
    frequency = freq;
}

/*! Volume range is 0-1. */
void SineWave::setVolume(double v) {
    
    if(v > 1)
        v = 1;
    if (v < 0)
        v = 0;
    
    volume = v;
}

} //rwe namespace

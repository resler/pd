/*
  Timer.cpp
  Pd++

  Created by Robert Esler on 10/29/14.
  Copyright (c) 2014 Robert Esler. All rights reserved.
*/

/* A simple timer that outputs milliseconds
 
  The timer class only returns the time in ms. Use the functions start() and stop()
  to begin and end the time measurement.  Make sure to keep updating the perform()
  function in your routine.  The time is calculated based on the sample rate. 
 
*/
#include "Timer.h"

namespace rwe {

Timer::Timer() {
    
}

Timer::~Timer() {
    
}

double Timer::perform() {
    
    double output = 0;
    
    if(timerOn)
        output = timerCounter++/(Timer::getSampleRate()) * 1000;
    
    if(timerReset)
    {
        timerCounter = 0;
        timerReset = false;
    }
    
    return output;
}

void Timer::start() {
    timerOn = true;
}

void Timer::stop() {
    timerOn = false;
}

void Timer::reset() {
    timerReset = true;
}

} // rwe namespace
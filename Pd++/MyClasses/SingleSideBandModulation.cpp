/*
  SingleSideBandModulation.cpp
  Pd++

  Created by Robert Esler on 9/23/14.
  Copyright (c) 2014 Robert Esler. All rights reserved.
*/

/* Single Sideband Modulation. 
 
   This class is based directly the hilbert~ and complex-mod~ 
   abstractions in Pure Data.  It uses a hilbert filter by cascading
   a series of biquad filters, which offsets the phase of the signal by 180
   degrees. Then the real and imaginary parts are modulation using a cosine and
   sine wave respectively. The input is your signal to be modulated.
 
*/
#include "SingleSideBandModulation.h"

namespace rwe {
    
SSB::SSB() {
    
    /*Exact numbers from the hilbert~ pd abstraction*/
    biquad[0].setCoefficients(1.94632, -.94657, .94657, -1.94632, 1);
    biquad[1].setCoefficients(.83774, -.06338, .06338, -.83774, 1);
    biquad[2].setCoefficients(-.02569, .260502, -.260502, .02569, 1);
    biquad[3].setCoefficients(1.8685, -.870686, .870686, -1.8685, 1);
    sine.setVolume(.9);
    sine.setFrequency(100);
    cosine.setVolume(.9);
    cosine.setFrequency(100);
    
}

SSB::~SSB() {
    
}

double SSB::leftPhase(double input){
    
    double bq1 = biquad[2].perform(input);
    double output = biquad[3].perform(bq1);
    return output;
    
}

double SSB::rightPhase(double input) {
    
    double bq2 = biquad[0].perform(input);
    
    double output = biquad[1].perform(bq2);
//    if(output >=1)
//        std::cout << "rightphase: " << output << std::endl;
    return output;
}

double SSB::perform(double input1) {
    
    double output = 0;
    
    /*get our L and R phase from the biquad chains*/
    double biquadLeft = SSB::leftPhase(input1);
    double biquadRight = SSB::rightPhase(input1);
  
//    if(biquadRight >=1 || biquadLeft >=1)
//        std::cout << "l: " << biquadLeft << "  r: " << biquadRight << std::endl;

    /*perform the phasor 90 deg apart*/
    
    double cosinePhase = cosine.perform(SSB::getOscFreq());
    double sinePhase = sine.perform(SSB::getOscFreq());
    
    /*multiply the biquad with the phasor*/
    double leftProduct = biquadLeft * cosinePhase;
    double rightProduct = biquadRight * sinePhase;
    
    /*subtract and add*/
    double ssbOutPositive = leftProduct - rightProduct;
    double ssbOutNegative = leftProduct + rightProduct;
    
    /*add them both and output. this step is not performed in the pd version*/
    output = (ssbOutPositive * .5) + (ssbOutNegative * .5);
    
    return output;
    
}

/*! This is the modulation frequency. */
void SSB::setOscFreq(double input) {
    
    phasorFreq = input;
    
}

double SSB::getOscFreq() {
    
    return phasorFreq;
    
}

} //rwe namespace
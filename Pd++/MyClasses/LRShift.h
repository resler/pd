#ifndef __Rwe____LRShift__
#define __Rwe____LRShift__

#include "PdMaster.h"
#include <iostream>

namespace rwe {
	class LRShift : public PdMaster {

	private:
		double* tmp;
		double* tmpIn;
	public:
		LRShift();
		LRShift(int ws);
		~LRShift();
		double* perform(double* in, int shift);
	};
}
#endif //(__Rwe____LRShift___)
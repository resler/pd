/*
 myLine.h
 Pd++

  Created by Robert Esler on 10/28/14.
  Copyright (c) 2014 Robert Esler. All rights reserved.
*/



#ifndef __Pd____myLine__
#define __Pd____myLine__

#include <stdio.h>
#include "PdMaster.h"
#include <string>

namespace rwe {
    /*! \brief Generates a ramp over a specified time.
     
     My line takes a starting value, ending value and a time length
     to generate a ramp from start to end.  The time increment is
     based on the sample rate.  The function stop() and start()
     can be used to begin and end the ramp.
     */
class myLine : public PdMaster {
        
    private:
        double startingValue;
        double endingValue;
        double time;
        bool isStopped = false; //starts ON
        long sampleCounter = 0;
        double valueIncrementor = 0;
        
        
    public:
        myLine();
        ~myLine();
        double perform(double arg1, double arg2, double arg3);
        void stop();
        void start();
        
};
    
    
} // rwe namespace


#endif /* defined(__Pd____myLine__) */

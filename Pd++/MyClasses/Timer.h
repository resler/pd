/*
 Timer.cpp
 Pd++
 
 Created by Robert Esler on 10/29/14.
 Copyright (c) 2014 Robert Esler. All rights reserved.
*/

#ifndef __Pd____Timer__
#define __Pd____Timer__

#include <stdio.h>
#include "PdMaster.h"

namespace rwe {

/*! \brief A simple timer that outputs milliseconds
     
     The timer class only returns the time in ms. Use the functions start() and stop()
     to begin and end the time measurement.  Make sure to keep updating the perform()
     function in your routine.  The time is calculated based on the sample rate.
     
*/
    
class Timer : public PdMaster {
    
private:
    bool timerOn = false;
    bool timerReset = false;
    double timerCounter = 0;
    
public:
    Timer();
    ~Timer();
    double perform();
    void start();
    void stop();
    void reset();
    
};

} // rwe namespace

#endif /* defined(__Pd____Timer__) */

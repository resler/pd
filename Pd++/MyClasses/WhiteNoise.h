/*
 WhiteNoise.cpp
 Pd++
 
 Created by Esler,Robert Wadhams on 10/14/14.
 Copyright (c) 2014 Robert Esler. All rights reserved.
 */

#ifndef __Pd____WhiteNoise__
#define __Pd____WhiteNoise__

#include <iostream>
#include <time.h>
#include <stdlib.h>
#include "PdMaster.h"

namespace rwe {

/*! \brief A white noise generator.*/

    
class WhiteNoise : public PdMaster {
    
private:
    
    
public:
    WhiteNoise();
    ~WhiteNoise();
    double perform();
    
};

    
} // rwe namespace

#endif /* defined(__Pd____WhiteNoise__) */

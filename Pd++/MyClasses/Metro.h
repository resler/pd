/*
 Metro.cpp
 Pd++
 
 Created by Robert Esler on 10/29/14.
 Copyright (c) 2014 Robert Esler. All rights reserved.
 */

#ifndef __Pd____Metro__
#define __Pd____Metro__

#include <stdio.h>
#include "PdMaster.h"

namespace rwe {

/*! \brief A simple metronome that accepts milliseconds and beats per minute.
     
     Metro in its default state is set to a millisecond delay. You can set the
     function setBPM(bool) to true if you would rather use bpm as your argument
     value.
 
*/
    
class Metro : public PdMaster {
    
private:
    
    double metroCounter = 0;
    bool bpmIsSet = false;
    double prevTime = 0;
    
public:
    
    Metro();
    ~Metro();
    bool perform(double time);
    void setBPM(bool );
    bool getBPM() {return bpmIsSet;};
};

} // rwe namespace
#endif /* defined(__Pd____Metro__) */

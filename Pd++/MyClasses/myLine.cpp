/*
 myLine.h
 Pd++
 
 Created by Robert Esler on 10/28/14.
 Copyright (c) 2014 Robert Esler. All rights reserved.
*/

/*Generates a ramp over a specified time.
 
 My line takes a starting value, ending value and a time length
 to generate a ramp from start to end.  The time increment is
 based on the sample rate.  The function stop() and start()
 can be used to begin and end the ramp.
 */

#include "myLine.h"

namespace rwe {
    
    myLine::myLine() {
        
    }
    
    myLine::~myLine() {
        
    }
    
    /*Performs a ramp from a given value up or down to a second value in ms of time.
     Time is based on the sample rate.*/
    double myLine::perform(double start, double end, double t) {
        
        double output = 0;
        startingValue = start;
        endingValue = end;
        time = t;
        
        if(!isStopped)
        {
            
            long sampleTime = myLine::getTimeInMilliSeconds(time);
            
            double range = endingValue - startingValue;
            double incrementAmount = 1/static_cast<double>(sampleTime);
            
            if (sampleTime >= sampleCounter++)
            {
                
                valueIncrementor += incrementAmount;
                output += startingValue + (valueIncrementor * range);
            }
            else
            {
                myLine::stop();
            }
        }
        else
        {
            sampleCounter = 0;
            valueIncrementor = 0;
            output = endingValue;
        }
        
        
        return output;
        
        
    }
    
    void myLine::stop() {
        
        isStopped = true;
        
    }
    
    void myLine::start() {
        
        isStopped = false;
    }
    
} // rwe namespace
#include "PhaseVocoder.h"

/*
 PhaseVocoder.cpp
 Pd++

 Created by Esler,Robert Wadhams on 10/1/14.
 Copyright (c) 2014 Robert Esler. All rights reserved.

 Some of the code in Pd++ is Copyright (c) 1997-1999 Miller Puckette.
 For information on usage and redistribution, and for a DISCLAIMER OF ALL
 WARRANTIES, see the file, "LICENSE.txt," in this distribution.
*/

/*
This emulates the Phase Vocoder from Pd as per
the I07.phase.vocoder.pd help patch.
It reads a sample via inSample()
Then you can set the speed or transposition (transpo)
 to get pitch w/o speed changes, or speed w/o pitch
 changes.
*/

namespace rwe {

	PhaseVocoder::PhaseVocoder() {

		this->setFFTWindow(2048);
		int ws = this->getFFTWindow();
		currentWindowSize = ws;

		rfft = new pd::realFFT(ws);
		rfft2 = new pd::realFFT(ws);
		rifft = new pd::realIFFT(ws);

		lrshiftA = new rwe::LRShift(ws);
		lrshiftB = new rwe::LRShift(ws);
		lrshiftC = new rwe::LRShift(ws);
		lrshiftD = new rwe::LRShift(ws);

        real1 = new (std::nothrow) double[ws] {};
        real2 = new (std::nothrow) double[ws] {};
        imag1 = new (std::nothrow) double[ws] {};
        imag2 = new (std::nothrow) double[ws] {};

        hann = new (std::nothrow) double[ws] {};
        ifftWas = new (std::nothrow) double[ws] {};
        tmp = new (std::nothrow) double[ws] {};
        output = new (std::nothrow) double[2] {};
		PhaseVocoder::createHann(ws);
		PhaseVocoder::init_rsqrt();

	}

	PhaseVocoder::PhaseVocoder(int w, int o) {

		this->setFFTWindow(w);
		overlap = o;
		int ws = this->getFFTWindow();
		currentWindowSize = ws;

		rfft = new pd::realFFT(ws);
		rfft2 = new pd::realFFT(ws);
		rifft = new pd::realIFFT(ws);

		lrshiftA = new rwe::LRShift(ws);
		lrshiftB = new rwe::LRShift(ws);
		lrshiftC = new rwe::LRShift(ws);
		lrshiftD = new rwe::LRShift(ws);

        real1 = new (std::nothrow) double[ws] {};
        real2 = new (std::nothrow) double[ws] {};
        imag1 = new (std::nothrow) double[ws] {};
        imag2 = new (std::nothrow) double[ws] {};

        hann = new (std::nothrow) double[ws] {};
        ifftWas = new (std::nothrow) double[ws] {};
        tmp = new (std::nothrow) double[ws] {};
        output = new (std::nothrow) double[2] {};
		PhaseVocoder::createHann(ws);
		PhaseVocoder::init_rsqrt();

	}

	PhaseVocoder::~PhaseVocoder() {

		delete rfft;
		delete rfft2;
		delete rifft;

		delete lrshiftA;
		delete lrshiftB;
		delete lrshiftC;
		delete lrshiftD;

		delete[] real1;
		delete[] real2;
		delete[] imag1;
		delete[] imag2;

		delete[] hann;
		delete[] ifftWas;
		delete[] tmp;
		delete[] output;
	}

	double PhaseVocoder::perform() {
		
		/*
		//This was used for testing to loop the sample
		double strWs = currentWindowSize * this->mtof((transpo * .01) + 69) / 440;
		double length = 0;
		
		if (transpo < 0)
		{
			length = (currentWindowSize / strWs) * 100/speed;
		}
		else
		{
			length = (strWs /currentWindowSize) * 100/speed;
		}
	
		loopCounter++;
		if (loopCounter > (sampleSize*length) )
		{
			loopCounter = 0;
			rewind = true;
		}
		*/
		
		return PhaseVocoder::doFFT();
	}

	double PhaseVocoder::doFFT() {

		int hop = this->getFFTWindow() / overlap;
		double* sum = ifftWas;
	
		if (sampleCounter == hop - 1)
		{
			double* fft1 = nullptr;
			double* fft2 = nullptr;
			bang = true;

			for (int i = 0; i < this->getFFTWindow(); i++)
			{
				double* out = readWindows();
				bang = false;
				fft1 = rfft->perform(out[0] * hann[i]);
				fft2 = rfft2->perform(out[1] * hann[i]);
			}

			for (int i = 0, j = this->getFFTWindow() - 1; i < this->getFFTWindow() / 2; i++, j--)
			{
				real1[i] = fft1[i];
				imag1[i] = fft1[j];
				real2[i] = fft2[i];
				imag2[i] = fft2[j];
			}

			/********************* BEGINNING of Analysis *******************/

			for (int i = 0; i < this->getFFTWindow(); i++)
			{
				//recall previous output amplitude, real/imag
				double p_r = prevReal[i];
				double p_i = prevImag[i];
				double mag = PhaseVocoder::q8_rsqrt((p_r * p_r) + (p_i * p_i) + 1e-20);
				double magReal = p_r * mag;
				double magImag = p_i * mag;


				//calculate conjugates
				double a = magReal * real1[i];
				double b = magImag * imag1[i];
				double c = magImag * real1[i];
				double d = magReal * imag1[i];
				neighbor1[i] = a + b;
				neighbor2[i] = c - d;
			}

			//shift our neighboring bins 
			double* shiftA = lrshiftA->perform(neighbor1, 1);
			double* shiftB = lrshiftB->perform(neighbor1, -1);
			double* shiftC = lrshiftC->perform(neighbor2, 1);
			double* shiftD = lrshiftD->perform(neighbor2, -1);
			
		
			//take the previous fft of the forward window, and store in our prevReal/prevImag arrays
			for (int i = 0; i < this->getFFTWindow(); i++)
			{
				double x = ((shiftA[i] + shiftB[i]) * this->lock) + neighbor1[i] + 1e-15;
				double y = ((shiftC[i] + shiftD[i]) * this->lock) + neighbor2[i];
				//std::cout << i << ": " << "A " << shiftA[i] << " | B " << shiftB[i] << " | N1 " << neighbor1[i] << std::endl;
				double mag = PhaseVocoder::q8_rsqrt((x * x) + (y * y));
				double magReal = x * mag;
				double magImag = y * mag;
			
				double a = magReal * real2[i];
				double b = magImag * imag2[i];
				double c = magReal * imag2[i];
				double d = magImag * real2[i];
				double e = a - b;
				double f = c + d;
				prevReal[i] = e;
				prevImag[i] = f;
			}
		
			for (int i = 0, j = this->getFFTWindow() - 1; i < this->getFFTWindow() / 2; i++, j--)
			{
				phaseVocoder[i] = prevReal[i];
				phaseVocoder[j] = prevImag[i];
			}

		/********************* END of Analysis *******************/

		  //resynthesize our FFT block, multiply by our Hann window
			for (int i = 0; i < this->getFFTWindow(); i++)
			{
				ifft[i] = (rifft->perform(phaseVocoder) * hann[i]);
			}

			// Now we overlap our windows, and add them together
			for (int i = 0; i < this->getFFTWindow(); i++)
			{
				sum[i] = ifft[i] + (i + hop < currentWindowSize ? ifftWas[i + hop] : 0);
			}

			ifftWas = sum;
			sampleCounter = -1;
		}

		sampleCounter++;
		//return a each sample from our previous block * our normalizer
		return  sum[sampleCounter] / (currentWindowSize);

	}

	double* PhaseVocoder::readWindows() {
		int windowSize = currentWindowSize;
		double window = (((float)windowSize / this->getSampleRate()) * 1000);
		double stretchedWindowSize = windowSize * this->mtof((transpo * .01) + 69) / 440;
		blockCounter++;
		//double x = blockCounter * (stretchedWindowSize / windowSize);
		double y = line.perform(stretchedWindowSize, window);
		

		//update the read location every fft block
		if (bang)
		{
			seeLoc = location + (window * (speed/4 ) * .01);
			readLocation = (location * (this->getSampleRate() / 1000)) - (stretchedWindowSize / 2);
			location = seeLoc;
			tab1.setOnset(readLocation);
			tab2.setOnset(readLocation);
			line.perform(0, 0);
			blockCounter = 0;
		}

		//if we rewind reset location.
		if (rewind)
		{
			location = ((stretchedWindowSize / this->getSampleRate()) * 1000) * -.5;
			rewind = false;
		}

		//read through one block of our sample, with tab1 being 1/4 cycle behind
		index1 = y - (stretchedWindowSize / 4.);
		index2 = y;

		output[0] = tab1.perform(index1);
		output[1] = tab2.perform(index2);
		return output;

	}

	void PhaseVocoder::createHann(int ws) {
		float winHz = 0;
		int windowSize = ws;

		if (windowSize != 0) {
			winHz = this->getSampleRate() / windowSize;
		}
		else {
			windowSize = 512;
			this->setFFTWindow(windowSize);
			std::cout << "Window size cannot be zero! Default is 512";
		}

		osc.setPhase(0);

		for (int i = 0; i < windowSize; i++)
		{
			hann[i] = (osc.perform(winHz) * -.5) + .5;
			prevReal[i] = 1;
			prevImag[i] = 1;
			tmp[i] = 0;
		}
	}

	/***********
	 We're going to try Miller Puckette's q8_rsqrt and see if we get a different result.
	***********/

	void PhaseVocoder::init_rsqrt()
	{
		int i;
		for (i = 0; i < DUMTAB1SIZE; i++)
			{

			union {
				float f;
				long l;
			} u;
			int32_t l =
				(i ? (i == DUMTAB1SIZE - 1 ? DUMTAB1SIZE - 2 : i) : 1) << 23;
			u.l = l;
			rsqrt_exptab[i] = 1. / sqrt(u.f);
		}
		for (i = 0; i < DUMTAB2SIZE; i++)
		{
			float f = 1 + (1. / DUMTAB2SIZE) * i;
			rsqrt_mantissatab[i] = 1. / sqrt(f);
		}
		
	}

	//Based on Pd's [q8_rsqrt~]
	float PhaseVocoder::q8_rsqrt(float x) {
		float out = 0;
		
		union {
			float f;
			long l;
		} u;
		u.f = x;
		if (x < 0) 
			out = 0;
		else
		{
			float g = rsqrt_exptab[(u.l >> 23) & 0xff] *
				rsqrt_mantissatab[(u.l >> 13) & 0x3ff];
			out = 1.5 * g - 0.5 * g * g * g * x;
		}
		
		return out;
	}

	//This was taken from sforgeeks.org/fast-inverse-square-root/
	float PhaseVocoder::rsqrt(float x) {
		const float threehalfs = 1.5F;

		float x2 = x * 0.5F;
		float y = x;

		// evil floating point bit level hacking
		long i = *(long*)&y;

		// value is pre-assumed
		i = 0x5f3759df - (i >> 1);
		y = *(float*)&i;

		// 1st iteration
		y = y * (threehalfs - (x2 * y * y));

		return y;
	}


	void PhaseVocoder::inSample(std::string file) {
		sampleSize = soundfiler.read(file);
		sample = soundfiler.getArray();
		tab1.setTable(sample);
		tab2.setTable(sample);
	}

	void PhaseVocoder::setSpeed(double s) {
		speed = s;
	}

	void PhaseVocoder::setTranspo(double t) {
		transpo = t;
	}

	void PhaseVocoder::setLock(int l) {
		lock = l;
	}

	void PhaseVocoder::setRewind() {
		rewind = true;
	}

}//end namespace


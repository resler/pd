#include "Convolution.h"

namespace rwe {

	Convolution::Convolution() {
		//window
		this->setFFTWindow(1024);
		int ws = this->getFFTWindow();
		
		rfft = new (std::nothrow) pd::realFFT(ws);
		rfft2 = new (std::nothrow) pd::realFFT(ws);
		rifft = new (std::nothrow) pd::realIFFT(ws);
		

		hann = new (std::nothrow) double[ws] {};
		buffer = new (std::nothrow) double[ws] {};
		buffer2 = new (std::nothrow) double[ws] {};

		//window/overlap
		in = new (std::nothrow)  double[ws / overlap] {};
		cIn = new (std::nothrow) double[ws / overlap] {};

		
		//window
		
		ifft = new(std::nothrow)  double[ws] {};
		ifftWas = new (std::nothrow) double[ws] {};
	
		Convolution::createHann();
	}

	Convolution::Convolution(int ws, int o) {
		//window
		this->setFFTWindow(ws);
		overlap = o;
		rfft = new pd::realFFT(ws);
		rfft2 = new pd::realFFT(ws);
		rifft = new pd::realIFFT(ws);
		overlap = o;
		
		hann = new (std::nothrow)double[ws] {};
		buffer = new (std::nothrow)double[ws] {};
		buffer2 = new (std::nothrow)double[ws] {};

		//window/overlap
		in = new (std::nothrow)double[ws / overlap] {};
		cIn = new (std::nothrow)double[ws / overlap] {};

		//window
		ifft = new (std::nothrow)double[ws] {};
		ifftWas = new (std::nothrow)double[ws] {};
		Convolution::createHann();
	}

	Convolution::~Convolution() {
		
		delete rfft;
		delete rfft2;
		delete rifft;
	
		//window
		delete[] ifft;
		delete[] ifftWas;
		delete[] hann;
		delete[] buffer;
		delete[] buffer2;

		//window/overlap
		delete[] in;
		delete[] cIn;
	}

	void Convolution::createHann() {
		
		float winHz = 0;
		int windowSize = this->getFFTWindow();
		
		if (windowSize != 0) {
			winHz = this->getSampleRate() / windowSize;
		}
		else {
			windowSize = 512;
			this->setFFTWindow(windowSize);
			std::cout << "Window size cannot be zero! Default is 512";
		}

		osc.setPhase(0);
		
		for (int i = 0; i < windowSize; i++)
		{
			hann[i] = (osc.perform(winHz) * -.5) + .5;
		}
		
	}

	double Convolution::clip(double a, double b, double c) {
		if (a < b)
			return b;
		else if (a > c)
			return c;
		else
			return a;
	}

	//This was taken from sforgeeks.org/fast-inverse-square-root/
	float Convolution::rsqrt(float x) {
		const float threehalfs = 1.5F;

		float x2 = x * 0.5F;
		float y = x;

		// evil floating point bit level hacking
		long i = *(long*)&y;

		// value is pre-assumed
		i = 0x5f3759df - (i >> 1);
		y = *(float*)&i;

		// 1st iteration
		y = y * (threehalfs - (x2 * y * y));

		return y;
	}

	double Convolution::perform(double filter, double control) {

		int hop = this->getFFTWindow() / overlap;
		double* sum = ifftWas;
		
		in[sampleCounter] = filter;
		cIn[sampleCounter] = control;
		
		sampleCounter++;

		if (sampleCounter == hop)
		{
			
			//1st we overlap our windows
			for (int i = 0; i < this->getFFTWindow()-hop; i++)
			{
				buffer[i] = buffer[i + hop];
				buffer2[i] = buffer2[i + hop];
			}

			int ws = this->getFFTWindow();
			for (int i = 0; i < hop; i++)
			{
				buffer[i + (ws - hop)] = in[i];
				buffer2[i + (ws - hop)] = cIn[i];
			}

			
			//Now we perform our FFTs and multiply by our Hann window
			for (int i = 0; i < this->getFFTWindow(); i++)
			{
				fft = rfft->perform(buffer[i] * hann[i]);
				fft2 = rfft2->perform(buffer2[i] * hann[i]);
			}
			
			//multiply the magnitude of our control freq bins by our filter freq bins, aka vocoding via convolution
			convolution = convolve(fft, fft2);

			//resynthesize our FFT block, multiply by our Hann window again
			for (int i = 0; i < this->getFFTWindow(); i++)
				ifft[i] = rifft->perform(convolution) * hann[i];
			
			// Now we overlap our windows, and add them together
			for (int i = 0; i < this->getFFTWindow(); i++)
				sum[i] = ifft[i] + (i + hop < this->getFFTWindow() ? ifftWas[i + hop] : 0);

				ifftWas = sum;
				
			sampleCounter = 0;
		}

		return  sum[sampleCounter] / (this->getFFTWindow() * 1.5);//divide by 3N/2
	}

	double* Convolution::convolve(double* filter, double* control) {
		
		double* bins = filter;
		//Real FFT puts the real on the front half or the window array, and imaginary on the back half
		for (int i = 0, j = this->getFFTWindow() - 1; i < this->getFFTWindow() / 2; i++, j--) 
		{

			//Get the magnitude of each bin of our filter input (live mic or recording)
			double realFilter = filter[i];
			double imagFilter = filter[j];
			//rsqrt( real^2 + imag^2) = freq bin magnitude
			double magnitudeFilter = rsqrt((realFilter * realFilter) + (imagFilter * imagFilter)
				+ 1e-020);

			double sq = getSquelch();
			magnitudeFilter = clip(magnitudeFilter, 0, 0.01 * sq * sq);

			//Get the magnitude of our control input, our synth or whatever else you like.
			double realControl = control[i];
			double imagControl = control[j];
			//sqrt( real^2 + imag^2) = freq bin magnitude
			double magnitudeControl = sqrt((realControl * realControl) + (imagControl * imagControl));


			double f = (magnitudeFilter * magnitudeControl);
			bins[i] = realFilter * f;
			bins[j] = imagFilter * f;

		}

		return bins;
	}

}
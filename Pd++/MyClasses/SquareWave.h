//
//  SquareWave.hpp
//  Pd++
//
//  Created by Rob Esler on 5/6/17.
//  Copyright © 2017 Robert Esler. All rights reserved.
//

#ifndef SquareWave_hpp
#define SquareWave_hpp

#include "PdMaster.h"

namespace rwe {
 
    /*! \brief A square wave generator.
     
     This class employs a variable width pulse wave.
     
     */
    
class SquareWave : public PdMaster {
    
    double frequency;
    double phase;
    double pulseWidth = 1;
    double period = 0;
    long counter = 0;
   
public:
    
    SquareWave(double frequency, double phase);
    SquareWave();
    ~SquareWave();
    double perform(double frequency);
    void calculatePhase();
    void setFrequency(double frequency);
    void setPhase(double phase);
    void setPulseWidth(double pw);
    void setPeriod(double p);
    
    double getFrequency() {return frequency;};
    double getPhase() {return phase;};
    double getPulseWidth() {return pulseWidth;};
    double getPeriod() {return period;};
};
    
}// end namespace rwe

#endif /* SquareWave_hpp */

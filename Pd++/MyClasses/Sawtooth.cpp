/*
  Sawtooth.cpp
  Pd++

  Created by Robert Esler on 9/17/14.
  Copyright (c) 2014 Robert Esler. All rights reserved.
*/

/*  A sawtooth wave generator.
 
    This is simple iteration of a sawtooth. Similar to phasor but uses
    a much simpler routine.
 
*/

#include "Sawtooth.h"

namespace rwe {

Sawtooth::Sawtooth() {
    
}

Sawtooth::~Sawtooth() {

}

double Sawtooth::perform(double input) {
    
    double output;
    Sawtooth::setFrequency(input);
    samplesPerCycle = Sawtooth::getSampleRate()/Sawtooth::getFrequency();
    output = iterator++/samplesPerCycle;
    
    if(iterator == static_cast<int>(samplesPerCycle)) iterator = 0;
    
    return output;
}

void Sawtooth::setFrequency(double f) {
    
    frequency = f;
}

double Sawtooth::getFrequency() {
    return frequency;
}

/*! Volume range is 0-1. */
void Sawtooth::setVolume(double v) {
    
    if(v > 1)
        v = 1;
    if (v < 0)
        v = 0;
    
    volume = v;
}

double Sawtooth::getVolume() {
    
    return volume;
}

} // rwe namespace
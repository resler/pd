/*
 SineWave.cpp
 
 Created by Robert Esler on 5/10/14.
 Copyright (c) 2014 Robert Esler. All rights reserved.
 */

#ifndef __aggregate___SineWave__
#define __aggregate___SineWave__

#include "PdMaster.h"
#include "Phasor.h"

namespace rwe {

/*! \brief A sine wave generator.
     
     This generator uses the Pd++ class phasor to generate its
     ramp.
     
*/
    
class SineWave : public PdMaster {
    
private:
    double factor = 0;
    int iterator = 0;
    double volume = 1;
    double multiplier = 0;
    double frequency = 0;
    double samplesPerCycle;
    double phase = 0;
    void setPhase(double );
    pd::Phasor phasor;
    
public:
    SineWave();
    SineWave(double, double);
    ~SineWave();
    double perform(double );
    
    void setFrequency(double);
    double getFrequency() {return frequency;};
    void setVolume(double);
    double getVolume() {return volume;};
    
    
    
};
    
} //rwe namespace

#endif /* defined(__aggregate___SineWave__) */

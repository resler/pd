#ifndef __Rwe____PhaseVocoder__
#define __Rwe____PhaseVocoder__

#include "PdMaster.h"
#include "LRShift.h"
#include "MasterObjectList.h"

#define DUMTAB1SIZE 256
#define DUMTAB2SIZE 1024

namespace rwe {

	class PhaseVocoder : public PdMaster {

		private:
			//these have to be initialized in the constructor
			pd::realFFT *rfft;
			pd::realFFT *rfft2;
			pd::realIFFT* rifft;
			rwe::LRShift* lrshiftA;
			rwe::LRShift* lrshiftB;
			rwe::LRShift* lrshiftC;
			rwe::LRShift* lrshiftD;

			pd::Oscillator osc;
			pd::TabRead4 tab1;
			pd::TabRead4 tab2;
			pd::Line line;
			pd::SoundFiler soundfiler;
			
			double* hann;
			double* ifftWas;
			double* tmp;
			double* real1;
			double* real2;
			double* imag1;
			double* imag2;
			double* output;
			float rsqrt_exptab[DUMTAB1SIZE];
			float rsqrt_mantissatab[DUMTAB2SIZE];

			//max fft block size will be 4096 based on the example
			double buffer1[4096];
			double buffer2[4096];
			double prevReal[4096];
			double prevImag[4096];
			double neighbor1[4096];
			double neighbor2[4096];
			double phaseVocoder[4096];
			double ifft[4096];
			double in1[4096];
			double in2[4096];
			
			std::vector<double> sample; //our sample table
			double index1 = 0;
			double index2 = 0;
			double location = 0;
			double seeLoc = 0;
			double speed = 100;
			bool rewind = true;
			double transpo = 100;
			int lock = 1;
			int currentWindowSize;
			int sampleCounter = 0;
			int loopCounter = 0;
			double readLocation = 0;
			int blockCounter = 0;
			long sampleSize = 0;
			int overlap = 4;
			bool bang = false;

			double doFFT();
			double* readWindows();
			void createHann(int ws);
			void init_rsqrt();
			float q8_rsqrt(float x);
			float rsqrt(float x);

		public:
			PhaseVocoder();
			PhaseVocoder(int ws, int o);
			~PhaseVocoder();
			double perform();
			void inSample(std::string file);
			void setSpeed(double s);
			void setTranspo(double t);
			void setLock(int l);
			void setRewind();
	};

}//end namespace
#endif //(__Rwe____PhaseVocoder___)


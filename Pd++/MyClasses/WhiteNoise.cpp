/*
  WhiteNoise.cpp
  Pd++

  Created by Esler,Robert Wadhams on 10/14/14.
  Copyright (c) 2014 Robert Esler. All rights reserved.
*/

/*
 A white noise generator.
*/

#include "WhiteNoise.h"

namespace rwe {

WhiteNoise::WhiteNoise() {
    
    srand(static_cast<unsigned int>(time(NULL)));
}

WhiteNoise::~WhiteNoise() {}

double WhiteNoise::perform() {
    
    double output = 0;
    int denominator = sizeof(int);
    denominator *= 8;
    denominator = 1<<(denominator-1);
    
    
    output = rand() % (denominator);
    output /= denominator;
    output += .5;
    output *= 2;
    
    
    return output;
    
    
}
    
} // rwe namespace

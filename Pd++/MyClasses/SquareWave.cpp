//
//  SquareWave.cpp
//  Pd++
//
//  Created by Rob Esler on 5/6/17.
//  Copyright © 2017 Robert Esler. All rights reserved.
//

#include "SquareWave.h"

namespace rwe {
    
SquareWave::SquareWave() {
    SquareWave::setFrequency(220);
    SquareWave::setPhase(0);
}

SquareWave::SquareWave(double f, double ph) {
    SquareWave::setFrequency(f);
    SquareWave::setPhase(ph);
    SquareWave::calculatePhase();
}

SquareWave::~SquareWave() {

}

//Square wave --__--__--__
double SquareWave::perform(double f) {
    
    double output = 0;
    double frequency = f;
    long sampleRate = SquareWave::getSampleRate();
    double pulseWidth = SquareWave::getPulseWidth();
    
    SquareWave::setPeriod( sampleRate/frequency );
    
    //calculate positive phase
    double posPhase = SquareWave::getPeriod() * .5;
    posPhase *= pulseWidth;
    
    if(counter > posPhase)
        output = -1;
    else
        output = 1;
    
    counter++;
    counter %= static_cast<long>(SquareWave::getPeriod());

    return output;
}
    
/* You will have to time the call of this function to only execute
 when you want a phase change.  Otherwise, it will yield unpredictable 
 results. */
void SquareWave::calculatePhase() {
    double p = SquareWave::getPeriod();
    double ph = SquareWave::getPhase();
    counter = p*ph;
}
    
void SquareWave::setFrequency(double f) {
    frequency = f;
}
/*! Phase range is 0 - 1 */
void SquareWave::setPhase(double ph) {
    phase = ph;
}
    
void SquareWave::setPulseWidth(double pw) {
        pulseWidth = pw;
}
    
void SquareWave::setPeriod(double p) {
        period = p;
}
    
} // end namespace rwe
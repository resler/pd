/*
 * $Id: portaudio.h 1859 2012-09-01 00:10:13Z philburk $
 * PortAudio Portable Real-Time Audio Library
 * PortAudio API Header File
 * Latest version available at: http://www.portaudio.com/
 *
 * Copyright (c) 1999-2002 Ross Bencina and Phil Burk
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * The text above constitutes the entire PortAudio license; however,
 * the PortAudio community also makes the following non-binding requests:
 *
 * Any person wishing to distribute modifications to the Software is
 * requested to send the modifications to the original developer so that
 * they can be incorporated into the canonical version. It is also
 * requested that these non-binding requests be included along with the
 * license above.
 */

/* 
 The audio API for Pd++, to get portaudio go to http://www.portaudio.com.
*/

#include "paRender++.h"

paRender::paRender() {
    
}

paRender::~paRender() {

        Pa_Terminate();    
}

bool paRender::open(PaDeviceIndex index, PaDeviceIndex input)
{
    PaStreamParameters outputParameters, inputParameters;
    
    /*Intialize output parameters*/
    outputParameters.device = index;
    if (outputParameters.device == paNoDevice) {
        return false;
    }
    
    inputParameters.device = input;
    if (inputParameters.device == paNoDevice) {
        return false;
    }
    
    outputParameters.channelCount = 2;       /* stereo output */
    outputParameters.sampleFormat = paFloat32; /* 32 bit floating point output */
    outputParameters.suggestedLatency = Pa_GetDeviceInfo( outputParameters.device )->defaultLowOutputLatency;
    outputParameters.hostApiSpecificStreamInfo = NULL;
    
    inputParameters.channelCount = 2;
    inputParameters.sampleFormat = paFloat32;
    inputParameters.suggestedLatency = Pa_GetDeviceInfo(inputParameters.device)->defaultLowInputLatency;
    inputParameters.hostApiSpecificStreamInfo = NULL;
    
    PaError err = Pa_OpenStream(
                                &stream,
                                &inputParameters, 
                                &outputParameters,
                                paRender::getSampleRate(),
                                framesPerBuffer,
                                paNoFlag,      /* we won't output out of range samples so don't bother clipping them */
                                &paRender::paCallback,
                                this            /* Using 'this' for userData so we can cast to paRender* in paCallback method */
                                );
    
    if (err != paNoError)
    {
        /* Failed to open stream to device !!! */
        std::cout << "no device!\n";
        return false;
    }
    
    err = Pa_SetStreamFinishedCallback( stream, &paRender::paStreamFinished );
    
    if (err != paNoError)
    {
        Pa_CloseStream( stream );
        stream = 0;
        
        return false;
    }
    
    return true;
}

bool paRender::close()
{
    if (stream == 0)
        return false;
    
    PaError err = Pa_CloseStream( stream );
    stream = 0;
    
    return (err == paNoError);
}


bool paRender::start()
{
    if (stream == 0)
        return false;
    
    PaError err = Pa_StartStream( stream );
    
    return (err == paNoError);
}

bool paRender::stop()
{
    if (stream == 0)
        return false;
    
    PaError err = Pa_StopStream( stream );
    
    return (err == paNoError);
}

/*! The instance callback, where we have access to every method/variable in object of class paRender */
int paRender::paCallbackMethod(const void *inputBuffer, void *outputBuffer,
                     unsigned long framesPerBuffer,
                     const PaStreamCallbackTimeInfo* timeInfo,
                     PaStreamCallbackFlags statusFlags)
{
    float *out = (float*)outputBuffer;
    float *in = (float*)inputBuffer;
    unsigned long i;
    
    (void) timeInfo; /* Prevent unused variable warnings. */
    (void) statusFlags;
    
    
    for( i=0; i<framesPerBuffer; i++ )
    {
        theBuffer.inbuf1 = *in++;
        theBuffer.inbuf2 = *in++;
        theBuffer = pd.runAlgorithm(theBuffer.inbuf1, theBuffer.inbuf2); // from PdAlgorithm
        *out++ = theBuffer.outbufL;  /* left */
        *out++ = theBuffer.outbufR;  /* right */
    }
    
    return paContinue;
    
}

/*! This routine will be called by the PortAudio engine when audio is needed.
 ** It may be called at interrupt level on some machines so don't do anything
 ** that could mess up the system like calling malloc() or free().
 */
int paRender::paCallback( const void *inputBuffer, void *outputBuffer,
                      unsigned long framesPerBuffer,
                      const PaStreamCallbackTimeInfo* timeInfo,
                      PaStreamCallbackFlags statusFlags,
                      void *userData )
{
    /* Here we cast userData to paRender* type so we can call the instance method paCallbackMethod, we can do that since
     we called Pa_OpenStream with 'this' for userData */
    return ((paRender*)userData)->paCallbackMethod(inputBuffer, outputBuffer,
                                               framesPerBuffer,
                                               timeInfo,
                                               statusFlags);
}


void paRender::paStreamFinishedMethod()
{
    printf( "Stream Completed: %s\n", message );
}

/*!
  This routine is called by portaudio when playback is done.
 */
void paRender::paStreamFinished(void* userData)
{
    return ((paRender*)userData)->paStreamFinishedMethod();
}

void paRender::sendError(int error) {
    if(error)
    {
        Pa_Terminate();
        fprintf( stderr, "An error occured while using the portaudio stream\n" );
        fprintf( stderr, "Error number: %d\n", error );
        fprintf( stderr, "Error message: %s\n", Pa_GetErrorText( error ) );
    }
}






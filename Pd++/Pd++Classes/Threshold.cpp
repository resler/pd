/*
 Threshold.cpp
 Pd++
 
 Created by Robert Esler on 11/28/16.
 Copyright (c) 2016 Robert Esler. All rights reserved.
 
 Some of the code in Pd++ is Copyright (c) 1997-1999 Miller Puckette.
 For information on usage and redistribution, and for a DISCLAIMER OF ALL
 WARRANTIES, see the file, "LICENSE.txt," in this distribution.
 */

#include "Threshold.h"

namespace pd {
    


Threshold::Threshold(){
    

    double highThreshold = 1;           /* value of high threshold */
    double lowThreshold = 0;           /* value of low threshold */
    double deadWait = 0;           /* msec remaining in dead period */
    double highDebounce = 1000;         /* hi debounce in msec */
    double lowDebounce = 100;         /* lo debounce in msec */
    msecPerTick = 1000. * (1.0/(double)Threshold::getSampleRate());
}

Threshold::Threshold(double ht, double hd, double lt, double ld) {
    if (lt > ht)
        lt = ht;
    highThreshold = ht;
    highDebounce = hd;
    lowThreshold = lt;
    lowDebounce = ld;
    deadWait = 0;
    msecPerTick = 1000. * (1.0/(double)Threshold::getSampleRate());
}

Threshold::~Threshold() {
    
}

int Threshold::perform(double input) {
    int value = -1;
  
    if (deadWait > 0)
    {
        deadWait -= msecPerTick;
        
    }
    else if (state)
    {
        /* we're high; look for low sample */
        if (input < lowThreshold)
        {
            state = 0;
            deadWait = lowDebounce;
            value = 0;
        }
        
    }
    else
    {
        /* we're low; look for high sample */
        if (input >= highThreshold)
        {
            state = 1;
            deadWait = highDebounce;
            value = 1;
        }
        
    }
    msecPerTick = 1000. * (1.0/(double)Threshold::getSampleRate());

    return value;
}

void Threshold::setValues(double ht, double hd, double lt, double ld) {
    if (lt > ht)
        lt = ht;
    highThreshold = ht;
    highDebounce = hd;
    lowThreshold = lt;
    lowDebounce = ld;
    
}

void Threshold::setState(int s) {
    state = (s != 0);
    deadWait = 0;
}

} //namespace pd
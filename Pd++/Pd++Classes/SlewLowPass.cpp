#include "SlewLowPass.h"

namespace pd {
    
    
    SlewLowPass::SlewLowPass() {
        x_coef = 0;
	x_last = 0;
    }
    
    SlewLowPass::~SlewLowPass() {
        //do nothing
    }

    double SlewLowPass::perform(double input, double freq, double posLimitIn, double posFreqIn, double negLimitIn, double negFreqIn ) {
   
    double sigin = input;
    double freqin = freq;
    double neglimit = negLimitIn;
    double negfreqin = negFreqIn;
    double poslimit = posLimitIn;
    double posfreqin = posFreqIn;
    double coef = x_coef;
    
    double output = 0;
    double last = x_last;
    
        double diff = sigin - last;
        double inc = freqin * coef, diffinc;
        double posinc = posfreqin * coef;
        double neginc = negfreqin * coef;
        double maxdiff = poslimit;
        double mindiff = neglimit;
        if (inc < 0.0)
            inc = 0.0;
        else if (inc > 1.0)
            inc = 1.0;
        if (posinc < 0.0)
            posinc = 0.0;
        else if (posinc > 1.0)
            posinc = 1.0;
        if (neginc < 0.0)
            neginc = 0.0;
        else if (neginc > 1.0)
            neginc = 1.0;
        if (maxdiff < 0.0)
            maxdiff = 0.0;
        if (mindiff < 0.0)
            mindiff = 0.0;
        if (diff > maxdiff)
            diffinc = posinc * (diff- maxdiff) + inc * maxdiff;
        else if (diff < -mindiff)
            diffinc = neginc * (diff + mindiff) - inc * mindiff;
        else diffinc = inc * diff;
        last = output = last + diffinc;
    
    if (SlewLowPass::PD_BIGORSMALL(last))
        last = 0;
    
    x_last = last;
    x_coef = (2 * 3.14159265358979) / SlewLowPass::getSampleRate();
    return output;     
    }
    
    void SlewLowPass::set(double last) {
        x_last = last;
    }
}
/*
  RawFilters.h
  Pd++

  Created by Robert Esler on 11/3/14.
  Copyright (c) 2014 Robert Esler. All rights reserved.
*/

/* Some of the code in Pd++ is Copyright (c) 1997-1999 Miller Puckette.
   For information on usage and redistribution, and for a DISCLAIMER OF ALL
   WARRANTIES, see the file, "LICENSE.txt," in this distribution.
*/

#ifndef __Pd____RawFilters__
#define __Pd____RawFilters__

#include <string>
#include "PdMaster.h"

namespace pd {


/*! \brief An abstract class for all of Pd's raw filters. */

    //Real Filter abstract class
class RealRawFilter : public PdMaster {
    
protected:
    double lastValue = 0;
    double lastReal = 0;
    double lastImaginary = 0;
    
    
    
public:
    RealRawFilter();
    ~RealRawFilter();
    //! For real filters
    virtual double perform(double, double)=0;
    //! For real filters
    void set(double value) {lastValue = value;};
    //! For complex filters
    void set(double real, double imaginary) {lastReal = real; lastImaginary = imaginary;};
    //! For all types
    void clear() {lastValue = lastReal = lastImaginary = 0;};
    
    
    void setLast(double last);
    void setLastReal(double lastreal);
    void setLastImaginary(double lastimag);
    
    double getLast() {return lastValue;};
    double getLastReal() {return lastReal;};
    double getLastImaginary() {return lastImaginary;};
    
};

//Complex Abstract Class
class ComplexRawFilter : public PdMaster {

protected:
    double lastValue = 0;
    double lastReal = 0;
    double lastImaginary = 0;



public:
    ComplexRawFilter();
    ~ComplexRawFilter();
    //! For complex filters
    virtual complexOutput perform(double, double, double, double) = 0;
    //! For real filters
    void set(double value) { lastValue = value; };
    //! For complex filters
    void set(double real, double imaginary) { lastReal = real; lastImaginary = imaginary; };
    //! For all types
    void clear() { lastValue = lastReal = lastImaginary = 0; };


    void setLast(double last);
    void setLastReal(double lastreal);
    void setLastImaginary(double lastimag);

    double getLast() { return lastValue; };
    double getLastReal() { return lastReal; };
    double getLastImaginary() { return lastImaginary; };

};

/* Raw: Real Filters*/

/*! \brief Raw real one pole filter. */
class RealPole : public RealRawFilter {
    
public:
    RealPole();
    ~RealPole();
    double perform(double, double);
    std::string pdName = "rpole~";
    
};
/*! \brief Raw real one zero filter. */
class RealZero : public RealRawFilter {
    
public:
    RealZero();
    ~RealZero();
    double perform(double, double);
    std::string pdName = "rzero~";
    
};
/*! \brief Raw real reverse one zero filter. */
class RealZeroReverse : public RealRawFilter {
    
public:
    RealZeroReverse() ;
    ~RealZeroReverse();
    double perform(double, double);
    std::string pdName = "rzero_rev~";
};

/*Raw: Complex Filters*/
/*! \brief Raw complex one pole filter. */
class ComplexPole : public ComplexRawFilter {

public:
    ComplexPole() ;
    ~ComplexPole();
    /*! The output of this function is a struct with real and imag parts. */
    complexOutput perform(double, double, double, double);
    std::string pdName = "cpole~";
    
};
/*! \brief Raw complex one zero filter. */
class ComplexZero : public ComplexRawFilter {
    
public:
    ComplexZero();
    ~ComplexZero();
    /*! The output of this function is a struct with real and imag parts. */
    complexOutput perform(double, double, double, double);
    std::string pdName = "czero~";
    
};
/*! \brief Raw complex reverse one zero filter. */
class ComplexZeroReverse : public ComplexRawFilter {
    
public:
    ComplexZeroReverse();
    ~ComplexZeroReverse();
    /*! The output of this function is a struct with real and imag parts. */
    complexOutput perform(double, double, double, double);
    std::string pdName = "czero_rev~";
};
    
} // pd namespace
#endif /* defined(__Pd____RawFilters__) */

/*
 SoundFiler.cpp
 Pd++
 
 Created by Esler,Robert Wadhams on 10/1/14.
 Copyright (c) 2014 Robert Esler. All rights reserved.

 Some of the code in Pd++ is Copyright (c) 1997-1999 Miller Puckette.
 For information on usage and redistribution, and for a DISCLAIMER OF ALL
 WARRANTIES, see the file, "LICENSE.txt," in this distribution.
*/

/* Reads and writes sound files.
 
 Instead of trying to detach the Pd soundfiler code, which is very difficult to
 port over to C++ due to the number of dependencies, I chose to use STK's
 read and write methods.  Since they are already in C++ this saves a lot of
 time and really shouldn't sound any different...(?)
 
 This does not apply to table reading, such as tabwrite4~, which uses interpolation.
 When reading from arrays into the audio domain we will still use this method...
 
 more to come.
 

 */
#include "SoundFiler.h"

namespace pd {
    
    
SoundFiler::SoundFiler() {}
    
SoundFiler::~SoundFiler() {}

/*! Just like Pd's soundfiler, this class will read a file and store it in a vector which can be accessed using getArray().
 Remember that the audio file data will be stored in RAM so large files can eat up lots of your applications' memory.
 If you want to read from disk look at the ReadSoundFile class*/
    
double SoundFiler::read(std::string file){
    
    try
    {
        input.openFile(file);
    }
    catch ( stk::StkError & )
    {
        exit( 1 );
    }

    // Set input read rate based on the definded sample rate.
    double rate = 1.0;
    rate = input.getFileRate() / this->getSampleRate();
    input.setRate( rate );
    
    input.ignoreSampleRateChange();
    
    // Find out how many channels we have.
    int channels = input.channelsOut();
    
    // Resize the StkFrames object appropriately.
    unsigned int bufferFrames = SoundFiler::getBlockSize();
    array.resize( bufferFrames, channels );
    std::vector<double> anArray;
    
    while(!input.isFinished())
    {
        input.tick(array);
        for (int i = 0; i < array.size(); i++)
        {
            anArray.push_back(array[i]);
        }
    }
    
    SoundFiler::setArray(anArray);
    input.closeFile();
    
    return anArray.size();
    
}
    
void SoundFiler::setArray(std::vector<double> sf) {
     
    soundFileData = sf;
    
}

/*! This method takes a filled array, and writes it to an audio format.
 This is similar to how Pd writes the contents of an array directly to
 a .wav.  If you want to use buffered sound file writing look for
 writesf~ or the class WriteSF in the future...*/
    
void SoundFiler::write(std::string fileName,
                        unsigned int nChannels,
                        stk::FileWrite::FILE_TYPE type,
                        Stk::StkFormat format,
                        std::vector<double> array) {
    
    /*This is where I have to figure out how to protect similar methods
     from PdMaster and STK.  But as long as we don't rely on STK outside 
     this method, it should be fine.  Notice the different scope operators (::).
     A great example of why not to use the "using" keyword.*/
    
    Stk::setSampleRate( SoundFiler::getSampleRate() );
    stk::FileWrite fileWrite;
    fileWrite.open( fileName, nChannels, type, format);
    
    stk::StkFrames frame(1, nChannels);
    
    /*The for loop is so far the only way I can get multi-channel
     files to write properly.*/

    for (int i = 0; i < array.size(); i++)
    {
        if (nChannels == 1)
            frame[0] = array[i];
        
        if (nChannels == 2)
        {
            frame[0] = array[i];
            frame[1] = array[++i];
        }
        
        if (nChannels == 3)
        {
            frame[0] = array[i];
            frame[1] = array[++i];
            frame[2] = array[++i];
        }
        
        if (nChannels == 4)
        {
            frame[0] = array[i];
            frame[1] = array[++i];
            frame[2] = array[++i];
            frame[3] = array[++i];
        }
        
        if (nChannels == 5)
        {
            frame[0] = array[i];
            frame[1] = array[++i];
            frame[2] = array[++i];
            frame[3] = array[++i];
            frame[4] = array[++i];
        }
        
        if (nChannels == 6)
        {
            frame[0] = array[i];
            frame[1] = array[++i];
            frame[2] = array[++i];
            frame[3] = array[++i];
            frame[4] = array[++i];
            frame[5] = array[++i];
        }
        /*You can add more channels here*/
        
        fileWrite.write(frame);
       
         
    }
    
    fileWrite.close();
    
   
}

} //pd namespace

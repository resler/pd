/*
  rFFT.cpp
  Pd++

  Created by Rob Esler on 9/29/14.
  Copyright (c) 2014 Robert Esler. All rights reserved.

  Some of the code in Pd++ is Copyright (c) 1997-1999 Miller Puckette.
  For information on usage and redistribution, and for a DISCLAIMER OF ALL
  WARRANTIES, see the file, "LICENSE.txt," in this distribution.

*/

/* Real Fast Fourier Transform
 
  Outputs a window, or block of samples based on the FFT window.
 
*/

#include "rFFT.h"

namespace pd {

/*! If you would like to change the window size, default is 64, use ::setFFTWindow()
 which is inherited from PdMaster.*/
realFFT::realFFT() {
    
    windowSize = realFFT::getFFTWindow();
    signal = new (std::nothrow) double[windowSize] {};
    if (signal == nullptr) printf("Bad pointer in rfft!\n");
   
}
/*! Copy constructor. You can manually set the window size here.*/
realFFT::realFFT(int ws) {
    
    windowSize = ws;
    realFFT::setFFTWindow(windowSize);
    signal = new (std::nothrow) double[windowSize] {};
    if (signal == nullptr)  printf("Bad Pointer in rfft!\n"); 
}

realFFT::~realFFT() {
    delete [] signal;
}

    /*! realFFT returns a block of samples that are processed by
     mayer_realfft().  The Mayer FFT methods pass all values
     by reference. You can do whatever with these samples.
     To resynthesize uses rIFFT and divide by window size.*/
double *realFFT::perform(double input) {
    
    signal[count] = input; //input to FFT
   
    
    if (count == realFFT::getFFTWindow() - 1)
        realFFT::mayer_realfft(realFFT::getFFTWindow(), signal);

    count++;
    if(count == realFFT::getFFTWindow()) count = 0;
    

    return signal;

}

    
} // pd namespace

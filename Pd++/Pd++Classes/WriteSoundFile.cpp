#include "WriteSoundFile.h"

namespace pd {

	WriteSoundFile::WriteSoundFile() {
		output = new stk::FileWvOut(bufferSize);
		Stk::setSampleRate(WriteSoundFile::getSampleRate());

	}

	WriteSoundFile::~WriteSoundFile() {
		delete output;
	}
	/*This function will default to WAV and 16-bit file format*/
	void WriteSoundFile::open(std::string filePath, unsigned int nChannels) {
		
		try {
			output->openFile(filePath, nChannels, stk::FileWrite::FILE_WAV, stk::FileWrite::STK_SINT16);
			// Resize the StkFrames object appropriately.
			unsigned int bufferFrames = bufferSize;
			array.resize(bufferFrames, nChannels);
			channels = nChannels;
			stopWrite = false;
		}
		catch (stk::StkError&)
		{
			exit(1);
		}
	}
	/*This overrided function can specify the format type and format*/
	void WriteSoundFile::open(std::string filePath, unsigned int nChannels,
		stk::FileWrite::FILE_TYPE type, Stk::StkFormat format) {
		
		try {
			output->openFile(filePath, nChannels, type, format);
			// Resize the StkFrames object appropriately.
			unsigned int bufferFrames = bufferSize;
			array.resize(bufferFrames, nChannels);
			channels = nChannels;
			stopWrite = false;
		}
		catch (stk::StkError&)
		{
			exit(1);
		}
	}

	/*This function takes an array of doubles as input to the file.
	 The size of the array should reflect the number of channels of your file.*/
	void WriteSoundFile::start(double* i) {

		if (!stopWrite)
		{

			if (sampleCounter == array.size())
			{
				sampleCounter = 0;
			}

			switch (channels) {
			case 1:
				array[sampleCounter++] = i[0];
				break;
			case 2:
				array[sampleCounter++] = i[0];
				array[sampleCounter++] = i[1];
				break;
			case 3:
				array[sampleCounter++] = i[0];
				array[sampleCounter++] = i[1];
				array[sampleCounter++] = i[2];
				break;
			case 4:
				array[sampleCounter++] = i[0];
				array[sampleCounter++] = i[1];
				array[sampleCounter++] = i[2];
				array[sampleCounter++] = i[3];
				break;
			case 5:
				array[sampleCounter++] = i[0];
				array[sampleCounter++] = i[1];
				array[sampleCounter++] = i[2];
				array[sampleCounter++] = i[3];
				array[sampleCounter++] = i[4];
				break;
			case 6:
				array[sampleCounter++] = i[0];
				array[sampleCounter++] = i[1];
				array[sampleCounter++] = i[2];
				array[sampleCounter++] = i[3];
				array[sampleCounter++] = i[4];
				array[sampleCounter++] = i[5];
				break;
			case 7:
				array[sampleCounter++] = i[0];
				array[sampleCounter++] = i[1];
				array[sampleCounter++] = i[2];
				array[sampleCounter++] = i[3];
				array[sampleCounter++] = i[4];
				array[sampleCounter++] = i[5];
				array[sampleCounter++] = i[6];
				break;
			case 8:
				array[sampleCounter++] = i[0];
				array[sampleCounter++] = i[1];
				array[sampleCounter++] = i[2];
				array[sampleCounter++] = i[3];
				array[sampleCounter++] = i[4];
				array[sampleCounter++] = i[5];
				array[sampleCounter++] = i[6];
				array[sampleCounter++] = i[7];
				break;
			default:
				std::cout << "Channels exceeded max channels or was set to 0, choose 1-8 channels in open()\n";
				break;
			}

			if (sampleCounter == array.size())
			{
				output->tick(array);
			}
		}
	}

	void WriteSoundFile::stop() {
		stopWrite = true;
		
		output->closeFile();
	}

	void WriteSoundFile::print() {
		std::cout << "Time in secs: " << output->getTime() << std::endl;
		std::cout << "Endian: " << output->getEndian() << std::endl;
		std::cout << "Frame count: " << output->getFrameCount() << std::endl;
	}

}
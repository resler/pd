/*
 TabRead4.cpp
 Pd++
 
 Created by Esler,Robert Wadhams on 10/13/14.
 Copyright (c) 2014 Robert Esler. All rights reserved.

 Some of the code in Pd++ is Copyright (c) 1997-1999 Miller Puckette.
 For information on usage and redistribution, and for a DISCLAIMER OF ALL
 WARRANTIES, see the file, "LICENSE.txt," in this distribution.
*/

#ifndef __Pd____TabRead4__
#define __Pd____TabRead4__

#include <string>
#include <vector>
#include "PdMaster.h"

namespace pd {

/*! \brief A struct that holds the four interpolation points for TabRead4. */
struct tableData {
    double point1;
    double point2;
    double point3;
    double point4;
};
   
    
/*!  \brief A 4-point table lookup routine.
     
     This class will read from an array or vector using 4-point polynomial interpolation.
     
     Remember when using this class to read table values 1 to n-2 to get meaningful data.
     Otherwise you will just get zeros at the end of your table reads.
     
     This class could be used for creating wavetable synthesizers.
     
*/
class TabRead4 : public PdMaster {
    
    
private:
    
    std::vector<double> theArray;
    int x_npoints;
    double x_onset = 0;
    tableData getTableData(int index); // get the four points
    
public:
    
    TabRead4();
    TabRead4(std::vector<double> anArray);
    ~TabRead4();
    double perform(double index);
    
    /*getters and setters*/
    void setTable(std::vector<double> table);
    size_t getTableSize();
    void setOnset(double);
    double getOnset();
    
    const std::string pdName = "tabread4~";
    
        
};
    
} // pd namespace

#endif /* defined(__Pd____TabRead4__) */

/*
 SoundFiler.cpp
 Pd++
 
 Created by Esler,Robert Wadhams on 10/1/14.
 Copyright (c) 2014 Robert Esler. All rights reserved.

 Some of the code in Pd++ is Copyright (c) 1997-1999 Miller Puckette.
 For information on usage and redistribution, and for a DISCLAIMER OF ALL
 WARRANTIES, see the file, "LICENSE.txt," in this distribution.
*/

#ifndef __Pd____SoundFiler__
#define __Pd____SoundFiler__

#include <string>
#include <vector>
#include "FileWvIn.h"
#include "FileWrite.h"
#include "Stk.h"
#include "PdMaster.h"

namespace pd {

    
/*! \brief Reads and writes sound files.
     
     Instead of trying to detach the Pd soundfiler code, which is very difficult to
     port over to C++ due to the number of dependencies, I chose to use STK's
     read and write methods.  Since their classes are already written in C++ this saves a lot of
     time and really shouldn't sound any different.
     
     This does not apply to table reading, such as tabwrite4~, which uses interpolation.
     When reading from arrays into the audio domain we will still use this method.
          
*/
    
class SoundFiler : public PdMaster, public stk::Stk {
    
private:
    stk::StkFrames array; //generic array used for input
    std::vector<double> soundFileData; //this is the actual input data as a vector
    stk::FileWvIn input;
        
    void setArray(std::vector<double> );
    
public:
    SoundFiler();
    ~SoundFiler();
    double read(std::string file);
    void write(std::string fileName,
               unsigned int nChannels,
               stk::FileWrite::FILE_TYPE type,
               Stk::StkFormat format,
               std::vector<double> array);
    
    std::vector<double> getArray() {return soundFileData;};
    
    const std::string pdName = "soundfiler";
    
};

} //pd namespace
#endif /* defined(__Pd____SoundFiler__) */

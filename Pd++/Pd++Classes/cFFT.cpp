/*
 rFFT.cpp
 Pd++
 
 Created by Rob Esler on 9/29/14.
 Copyright (c) 2014 Robert Esler. All rights reserved.

 Some of the code in Pd++ is Copyright (c) 1997-1999 Miller Puckette.
 For information on usage and redistribution, and for a DISCLAIMER OF ALL
 WARRANTIES, see the file, "LICENSE.txt," in this distribution.
*/

/* Complex Fast Fourier Transform
 
 Outputs a window of frequency bins, or block of samples based on the FFT window.
 
*/

#include "cFFT.h"
#include <iostream>

namespace pd {
    
/*! If you would like to change the window size, default is 64, use ::setFFTWindow()
 which is inherited from PdMaster.*/
complexFFT::complexFFT() {
        
    windowSize = complexFFT::getFFTWindow();
    realFFT = new (std::nothrow) double[windowSize] {};
    imagFFT = new (std::nothrow) double[windowSize] {};
    output = new (std::nothrow) double[windowSize * 2] {};
    if (realFFT == nullptr) printf("Bad pointer in fft!\n");
    
}

/*! Copy constructor. You can manually set the window size here. */
complexFFT::complexFFT(int ws) {
        
    windowSize = ws;
    complexFFT::setFFTWindow(windowSize);
    realFFT = new (std::nothrow) double[windowSize] {};
    imagFFT = new (std::nothrow) double[windowSize] {};
    output = new (std::nothrow) double[windowSize * 2] {};// for interleaved data
    if (realFFT == nullptr) printf("Bad Pointer in fft!\n");
    
}
    
complexFFT::~complexFFT() {
    delete [] realFFT;
    delete [] imagFFT;
    delete [] output;
}
    
/*! complexFFT returns a block of samples that are processed by
 mayer_fft().  For complex FFT it returns the real and imaginary parts.
 The Mayer FFT methods pass all values by reference. You can do whatever with these samples.
To resynthesize use cIFFT and divide by window size.*/

double *complexFFT::perform(double real, double imaginary) {
    
    realFFT[count] = real; //input to FFT
    imagFFT[count] = imaginary;
    
 
    if(count == windowSize-1)
    {
        /*Perform FFT, real and imag inputs are passed by reference pointers.*/
        complexFFT::mayer_fft(complexFFT::getFFTWindow(), realFFT, imagFFT);
        
        int j = 0;
        /*Copy the frequency domain data to the output array.
          Real and imag values are interleaved.*/
        for(int i = 0; i < complexFFT::getFFTWindow(); i++)
        {
            output[j++] = realFFT[i];   
            output[j++] = imagFFT[i];
        }
    }
    
    count++;
    if(count == complexFFT::getFFTWindow()) count = 0;

    return output;
        
}
    
    
} // pd namespace


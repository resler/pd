/*
 WriteSoundFile.cpp
 Pd++
 
 alpha release, not fully tested.

 This code will write a sound file to disk as a stream
instead of directly into RAM like SoundFiler.

It writes chunks of data, e.g bufferSize, directly from 
RAM to disk so it can support unknown file lengths.

Use open() before calling start().
Use stop() to close and finish creating the file.

This library will only support uncompressed file formats (so no .mp3, .m4a, .flac, etc.)

This code is not threaded (yet)

 Created by Esler,Robert Wadhams on 7/26/2021.
 Copyright (c) 2021 Robert Esler. All rights reserved.

 Some of the code in Pd++ is Copyright (c) 1997-1999 Miller Puckette.
 For information on usage and redistribution, and for a DISCLAIMER OF ALL
 WARRANTIES, see the file, "LICENSE.txt," in this distribution.
*/

#ifndef __Pd____WriteSoundFile__
#define __Pd____WriteSoundFile__

#include <string>
#include <vector>
#include "FileWvOut.h"
#include "FileWrite.h"
#include "Stk.h"
#include "PdMaster.h"

namespace pd {

	class WriteSoundFile : public PdMaster, public stk::Stk {

	public: 
		WriteSoundFile();
		~WriteSoundFile();
		void open(std::string f, unsigned int nChannels);
		void open(std::string f, unsigned int nChannels,
			stk::FileWrite::FILE_TYPE type, Stk::StkFormat format);
		void start(double* i);
		void stop();
		void print();

		std::string pdName = "writesf~";

	private:
		stk::FileWvOut* output = nullptr;
		bool stopWrite = false;
		stk::StkFrames array;
		long sampleCounter = 0;
		int bufferSize = 1024;
		int channels = 2;
	};

}//namespace pd
#endif
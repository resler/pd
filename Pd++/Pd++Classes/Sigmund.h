/*
  Sigmund.hpp
  Pd++
 
   A high quality pitch detector

  Created by Robert Esler on 12/5/16.
  Copyright © 2016 Robert Esler. All rights reserved.
*/

/* sigmund.c code: Copyright (c) 2005 Miller Puckette.  BSD licensed.  No warranties. */

/*
 fix parameter settings
 not to report pitch if evidence too scanty?
 note-on detection triggered by falling envelope (a posteriori)
 reentrancy bug setting loud flag (other parameters too?)
 tweaked freqs still not stable enough
 implement block ("-b") mode
 */


#ifndef Sigmund_h
#define Sigmund_h

#include <stdio.h>
#include <iostream>
#include <string>
#include "PdMaster.h"
#include "fft.h"
#ifdef _MSC_VER /* this is only needed with Microsoft's compiler */
__declspec(dllimport) extern
#endif

/* this routine is passed a buffer of npoints values, and returns the
 N/2+1 real parts of the DFT (frequency zero through Nyquist), followed
 by the N/2-1 imaginary points, in order of decreasing frequency.  Pd 0.41,
 for example, defines this in the file d_fft_mayer.c or d_fft_fftsg.c. */

#include <math.h>
#include <stdio.h>
#include <string.h>
#ifdef _WIN32
#include <malloc.h>
#elif ! defined(_MSC_VER)
#include <alloca.h>
#endif
#include <stdlib.h>
#ifdef _MSC_VER
#pragma warning( disable : 4244 )
#pragma warning( disable : 4305 )
#endif

#define NHISTPOINT 100
#define NHIST 100

#define MODE_STREAM 1
#define MODE_BLOCK 2        /* unimplemented */
#define MODE_TABLE 3

#define NPOINTS_DEF 1024
#define NPOINTS_MIN 128

#define HOP_DEF 512
#define NPEAK_DEF 20

#define VIBRATO_DEF 1
#define STABLETIME_DEF 50
#define MINPOWER_DEF 50
#define GROWTH_DEF 7

#define OUT_PITCH 0
#define OUT_ENV 1
#define OUT_NOTE 2
#define OUT_PEAKS 3
#define OUT_TRACKS 4
#define OUT_SMSPITCH 5
#define OUT_SMSNONPITCH 6

/*!
 \brief This is a structure for the Sigmund class
 */
struct sigmundPackage {
    double pitch = 0;
    double notes = 0;
    double envelope = 0;
    double **peaks;
    double **tracks;
    int peakSize = 0;
    int trackSize = 0;
    
};

namespace pd {

/*! \brief  A powerful pitch detector.
 
   Sigmund is a pitch detector that allows you to detect the pitch and amplitude of a signal, or
   report a certain number of peaks and their respective amplitudes.  The output of this perform
   function is a special structure that will return all the different data.  
You can set several parameters of the analysis: number of points in the analysis window (setNumOfPoints) should be power of 2, hop size (setHopSize) power of 2, number of sinusoidal peaks, max frequency (setMaxFrequency), vibrato, use this if your signal has vibrato, value is in semitones (setVibrato), stable time, time the routine will wait to report notes (setStableTime), min power, minimum dB to report a note (setMinPower), growth, growth in dB to report a repeated note (setGrowth)
 */


class Sigmund : public PdMaster, public FFT {

    //I'm leaving all of the original c structs as is, because it's too much code
    //to change.
    typedef struct peak
    {
        double p_freq;
        double p_amp;
        double p_ampreal;
        double p_ampimag;
        double p_pit;
        double p_db;
        double p_salience;
        double p_tmp;
    } t_peak;
    
    typedef struct _histpoint
    {
        double h_freq;
        double h_power;
    } t_histpoint;
    
    typedef struct _notefinder
    {
        double n_age;
        double n_hifreq;
        double n_lofreq;
        int n_peaked;
        t_histpoint n_hist[NHISTPOINT];
        int n_histphase;
    } t_notefinder;
    
    typedef struct _varout
    {
        int v_what;
    } t_varout;
    
    typedef struct _sigmund
    {
        t_varout *x_varoutv;
        int x_nvarout;
        double x_sr;       /* sample rate */
        int x_mode;         /* MODE_STREAM, etc. */
        int x_npts;         /* number of points in analysis window */
        int x_npeak;        /* number of peaks to find */
        int x_loud;         /* debug level */
        double *x_inbuf;  /* input buffer */
        int x_infill;       /* number of points filled */
        int x_countdown;    /* countdown to start filling buffer */
        int x_hop;          /* samples between analyses */
        double x_maxfreq;    /* highest-frequency peak to report */
        double x_vibrato;    /* vibrato depth in half tones */
        double x_stabletime; /* period of stability needed for note */
        double x_growth;     /* growth to set off a new note */
        double x_minpower;   /* minimum power, in DB, for a note */
        double x_param1;     /* three parameters for temporary use */
        double x_param2;
        double x_param3;
        t_notefinder x_notefinder;  /* note parsing state */
        t_peak *x_trackv;           /* peak tracking state */
        int x_ntrack;               /* number of peaks tracked */
        unsigned int x_dopitch:1;   /* which things to calculate */
        unsigned int x_donote:1;
        unsigned int x_dotracks:1;
    } t_sigmund;
    
    int sigmund_ilog2(int n);
    double sigmund_ftom(double f);
    double sigmund_powtodb(double f);
    double sinx(double theta, double sintheta);
    double window_hann_mag(double pidetune, double sinpidetune);
    double window_mag(double pidetune, double cospidetune);
    
    /*********** Routines to analyze a window into sinusoidal peaks *************/
    static int sigmund_cmp_freq(const void *p1, const void *p2);
    void sigmund_tweak(int npts, double *ftreal, double *ftimag,
                       int npeak, t_peak *peaks, double fperbin, int loud);
    void sigmund_remask(int maxbin, int bestindex, double powmask,
                        double maxpower, double *maskbuf);
    void sigmund_getrawpeaks(int npts, double *insamps,
                             int npeak, t_peak *peakv, int *nfound, double *power, double srate, int loud,
                             double hifreq);
    
    /*************** Routines for finding fundamental pitch *************/
    void sigmund_getpitch(int npeak, t_peak *peakv, double *freqp,
                          double npts, double srate, double nharmonics, double amppower, int loud);
    /*************** gather peak lists into sinusoidal tracks *************/
    void sigmund_peaktrack(int ninpeak, t_peak *inpeakv,
                           int noutpeak, t_peak *outpeakv, float maxerror, int loud);
    /**************** parse continuous pitch into note starts ***************/
    void notefinder_init(t_notefinder *x);
    void notefinder_doit(t_notefinder *x, double freq, double power,
                         double *note, double vibrato, int stableperiod, double powerthresh,
                         double growththresh, int loud);
    
    void sigmund_preinit();
    void sigmund_npts(double f);
    void sigmund_hop(double f);
    void sigmund_npeak(double f);
    void sigmund_maxfreq(double f);
    void sigmund_vibrato(double f);
    void sigmund_stabletime(double f);
    void sigmund_growth(double f);
    void sigmund_minpower(double f);
    void sigmund_doit(int npts, double *arraypoints, int loud, double srate);
    
    void *resizebytes(void *old, size_t oldsize, size_t newsize);
    void freebytes(void *fatso, size_t nbytes);
    void *getbytes(size_t nbytes);
    
    t_sigmund *x;
    
    //These are so we can track whether we allocated memory to the multi-dimesional arrays
    bool peakArrayCreated = false;
    bool trackArrayCreated = false;
    sigmundPackage output;
    int peakCols = 5;//based on how lists were created in the original code, we have 5 values
    int trackCols = 4;//4 values for tracks.
    
public:
    
    Sigmund();
    Sigmund(std::string, std::string);//pitch, env
    Sigmund(std::string, int numOfPeaks);//tracks or peaks
    ~Sigmund();
    sigmundPackage perform(double input);
    void setMode(int mode);
    void setNumOfPoints(double n);
    void setHop(double h);
    void setNumOfPeaks(double p);
    void setMaxFrequency(double mf);
    void setVibrato(double v);
    void setStableTime(double st);
    void setMinPower(double mp);
    void setGrowth(double g);
    void print();//print all parameters
    sigmundPackage list(double *array, int numOfPoints, int index, long sr, int debug);//read from an array
    void clear();
    
    //getters
    int getMode() { return x->x_mode;};
    double getNumOfPoints(){return x->x_npts;};
    double getHop(){return x->x_hop;};
    double getNumOfPeaks(){return x->x_npeak;};
    double getMaxFrequency(){return x->x_maxfreq;};
    double getVibrato(){return x->x_vibrato;};
    double getStableTime(){return x->x_stabletime;};
    double getMinPower(){return x->x_minpower;};
    double getGrowth(){return x->x_growth;};
    int getPeakColumnNumber(){return peakCols;};
    int getTrackColumnNumber(){return trackCols;};
    int getPeakBool() { return peakArrayCreated; };
    int getTrackBool() { return trackArrayCreated; };
    std::string pdName = "sigmund~";
    
};

}//namespace pd
#endif /* Sigmund_h */

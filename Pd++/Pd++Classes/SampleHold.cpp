/*
 SampleHold.cpp
 Pd++
 
 Created by Robert Esler on 11/3/14.
 Copyright (c) 2014 Robert Esler. All rights reserved.
 
 Some of the code in Pd++ is Copyright (c) 1997-1999 Miller Puckette.
 For information on usage and redistribution, and for a DISCLAIMER OF ALL
 WARRANTIES, see the file, "LICENSE.txt," in this distribution.
 */

#include "SampleHold.h"

namespace pd {

SampleHold::SampleHold() {
    
    lastIn = 0;
    lastOut = 0;
}

SampleHold::~SampleHold() {
    
}

double SampleHold::perform(double input, double control) {
    
    double output = 0;
    
    double lastInCopy = lastIn;
    double lastOutCopy = lastOut;
   
    double next = control;
    if (next < lastInCopy) lastOutCopy = input;
    output = lastOutCopy;
    lastInCopy = next;
    
    lastIn = lastInCopy;
    lastOut = lastOutCopy;
    
    return output;
}

/*! Use the value 1e20 for "infinity" which forces the next input to be sampled */
void SampleHold::reset(double value) {
    if (value > 0)
        lastIn = value;
}
/*! Sets the output value. */
void SampleHold::set(double value) {
    lastOut = value;
}

} // pd namespace
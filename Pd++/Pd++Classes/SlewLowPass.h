/*
 SlewLowPass.cpp
 Pd++
 
 Created by Robert Esler on 6/18/2020
 Copyright (c) 2020 Robert Esler. All rights reserved.
 
 Some of the code in Pd++ is Copyright (c) 1997-1999 Miller Puckette.
 For information on usage and redistribution, and for a DISCLAIMER OF ALL
 WARRANTIES, see the file, "LICENSE.txt," in this distribution.
 */

#ifndef __Pd____SlewLowPass__
#define __Pd____SlewLowPass__

#include "PdMaster.h"
#include <string>

namespace pd {

/*! \brief A slewing low pass filter
     
  A low pass filter that applies a slew, or change of amplitude over time.  This can be used to create limiters, companders, peak detection
or even some fancy EQ/compressors.  
     
*/
    

    
class SlewLowPass : public PdMaster {
  
public:
    SlewLowPass();
    ~SlewLowPass();
    double perform(double input, double freq, double posLimitIn, double posFreqIn, double negLimitIn, double negFreqIn );
    void set(double last);
    
private:
    
    double x_coef;
    double x_last;
    
    const std::string pdName = "slop~";
    
};

} // pd namespace

#endif /* defined(__Pd____SlewLowPass__) */

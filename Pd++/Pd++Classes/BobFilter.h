/*
  BobFilter.hpp
  Pd++

 This is directly from Miller Puckette's code, "use a differential equation solver to imitate an analogue circuit".  In plainspeak, this is an emulation of a Moog or analog style resonant filter.
 
  Created by Robert Esler on 11/30/16.
  Copyright © 2016 Robert Esler. All rights reserved.
*/

#ifndef BobFilter_h
#define BobFilter_h

#include <stdio.h>
#include <iostream>
#include "PdMaster.h"
#include <math.h>
#include <string>
//I guess I'll leave this the way it is.  This is a messy way to do it.
#define DIM 4
#define FLOAT double

namespace pd {

/*! \brief An emulation of a Moog resonant filter.  
 
   This filter uses a 4th order Runge-Kutte calculation per sample.  This will work fine up to about 1/2 of the Nyquist.  If you want to go above that, use the oversampling parameter. 
     Oversampling will affect performance, but unless you are using crazy large numbers, it shouldn't be too dramatic.  (Big O I think is just O(n) )  Oversampling also increases stability.
    Saturation represents the transistor saturation level.  
    Clear will discharge the "capacitors" in case the filter goes unstable.  
    
 */
class BobFilter : public PdMaster {
    
private:
    typedef struct _params
    {
        FLOAT p_input;
        FLOAT p_cutoff;
        FLOAT p_resonance;
        FLOAT p_saturation;
        FLOAT p_derivativeswere[DIM];
    } t_params;

    FLOAT clip(FLOAT value, FLOAT saturation, FLOAT saturationinverse);
    FLOAT clip(FLOAT value, FLOAT saturation, FLOAT saturationinverse, void *none);
    void calc_derivatives(FLOAT *dstate, FLOAT *state, t_params *params);
    void solver_euler(FLOAT *state, FLOAT *errorestimate, FLOAT stepsize, t_params *params);
    void solver_rungekutte(FLOAT *state, FLOAT *errorestimate, FLOAT stepsize, t_params *params);
    
#ifdef CALCERROR
    FLOAT x_cumerror;
#endif
    t_params x_params;
    FLOAT x_state[DIM];
    FLOAT x_sr;
    int x_oversample;
    int x_errorcount;
    
public:
    BobFilter();
    ~BobFilter();
    double perform(double input);
    void setCutoffFrequency(double cf);
    void setResonance(double r);
    void setSaturation(double s);
    void setOversampling(double o);
    double getCutoffFrequency() {return x_params.p_cutoff;};
    double getResonance() {return x_params.p_resonance;};
    t_params getParameters() {return x_params;};
    void error();
    void clear();
    void print();
    std::string pdName = "bob~";
    
};

} //namespace pd
#endif /* BobFilter_h */

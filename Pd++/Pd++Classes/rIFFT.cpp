/*
  rIFFT.cpp
  Pd++
 
  Created by Rob Esler on 9/29/14.
  Copyright (c) 2014 Robert Esler. All rights reserved.

  Some of the code in Pd++ is Copyright (c) 1997-1999 Miller Puckette.
  For information on usage and redistribution, and for a DISCLAIMER OF ALL
  WARRANTIES, see the file, "LICENSE.txt," in this distribution.

*/

/* Real Inverse Fast Fourier Transform.
 
 This is an emulation of the rifft~ object.  It takes a window of
 samples and resynthesizes them back into the time domain.
 This is different from several other Pd++ classes as it
 accepts a double*, which is a c-style reference to our input window.
 Real Inverse Fast Fourier Transform only provides the real (or cosine part I believe)
 of an FFT.  See FFT or fft_mayer.c for more information on the algorithm.
 
 */
#include "rIFFT.h"

namespace pd {
    
    
/*! If you would like to change the window size, default is 64, use ::setFFTWindow()
 which is inherited from PdMaster.*/
realIFFT::realIFFT() {
    
    windowSize = realIFFT::getFFTWindow();
    signal = new (std::nothrow)double[windowSize] {};

}
/*! Copy constructor. You can manually set the window size here. */
realIFFT::realIFFT(int ws) {
        
    windowSize = ws;
    realIFFT::setFFTWindow(windowSize);
    signal = new (std::nothrow)double[windowSize] {};
}
    
realIFFT::~realIFFT() {
    delete[] signal;
}
    
double realIFFT::perform(double *input) {
        
    double output = 0;
    
    if (count == realIFFT::getFFTWindow() - 1)
    {
        for (int i = 0; i < realIFFT::getFFTWindow(); i++)
        {
            signal[i] = input[i]; // block of samples probably from realFFT.
        }
            
        realIFFT::mayer_realifft(realIFFT::getFFTWindow(), signal);
    }
   
    count++;
    if( count == realIFFT::getFFTWindow() ) count = 0;
    output = signal[count];
    
    return output;
}
    

    
} // pd namespace

/*
  cIFFT.h
  Pd++

  Created by Robert Esler on 11/11/14.
  Copyright (c) 2014 Robert Esler. All rights reserved.
 
  Some of the code in Pd++ is Copyright (c) 1997-1999 Miller Puckette.
  For information on usage and redistribution, and for a DISCLAIMER OF ALL
  WARRANTIES, see the file, "LICENSE.txt," in this distribution.
*/

#ifndef __Pd____cIFFT__
#define __Pd____cIFFT__

#include <string>
#include "PdMaster.h"
#include "fft.h"

namespace pd {
    
/*! \brief Complex Inverse Fast Fourier Transform.
     
     This is an emulation of the ifft~ object.  It takes a window of
     samples and resynthesizes them back into the time domain.
     This is different from several other Pd++ classes as it
     accepts a complex*, which is a c-style reference to our input window.
     Complex Inverse Fast Fourier Transform only provides the real and imaginary parts
     of an FFT.  See FFT or fft_mayer.c for more information on the algorithm.
     
*/
    
class complexIFFT : public PdMaster,  public FFT {
        
private:
    int windowSize = 64;
    double *realIFFT = nullptr;
    double *imagIFFT = nullptr;
    double *output = new double[2]; // 0 real, 1 imaginary
    bool first = true;
    int count = 0;
        
        
public:
    complexIFFT();
    complexIFFT(int);
    ~complexIFFT();
    
    /*! This function takes a block of complex samples, which are the real and imaginary parts.*/
    double *perform(double *input);
        
    const std::string pdName = "ifft~";
};
    
} // pd namespace

#endif /* defined(__Pd____cIFFT__) */

/*
 A Collection of raw filters.
 This code is adapted from Miller Puckette's Pd code. File: d_filters.c
*/
/* Some of the code in Pd++ is Copyright (c) 1997-1999 Miller Puckette.
 For information on usage and redistribution, and for a DISCLAIMER OF ALL
 WARRANTIES, see the file, "LICENSE.txt," in this distribution.
 */

#include "RawFilters.h"

namespace pd {
    
    /*!  \brief Implementation of Real Filters */
RealRawFilter::RealRawFilter() {}
RealRawFilter::~RealRawFilter() {}
void RealRawFilter::setLast(double last) {lastValue = last;}
void RealRawFilter::setLastReal(double lastreal) {lastReal = lastreal;}
void RealRawFilter::setLastImaginary(double lastimag) {lastImaginary = lastimag;}

/*!  \brief Implementation of Complex Filters */
ComplexRawFilter::ComplexRawFilter() {}
ComplexRawFilter::~ComplexRawFilter() {}
void ComplexRawFilter::setLast(double last) { lastValue = last; }
void ComplexRawFilter::setLastReal(double lastreal) { lastReal = lastreal; }
void ComplexRawFilter::setLastImaginary(double lastimag) { lastImaginary = lastimag; }
    
/*! \brief Real one pole filter */

RealPole::RealPole() {
    
}

RealPole::~RealPole() {
    
}

double RealPole::perform(double input, double coef) {
    
    double output = 0;
    double last = RealPole::getLast();
    
    double next = input;
    double coefCopy = coef;
    output = last = coefCopy * last + next;
    
    if (RealPole::PD_BIGORSMALL(last))
        last = 0;
    RealPole::setLast(last);
    
    return output;
}

/*! \brief Real one zero filter. */

RealZero::RealZero() {
    
}

RealZero::~RealZero() {
    
}

double RealZero::perform(double input, double coef) {
    
    double output = 0;
    
    double last = RealZero::getLast();
   
    double next = input;
    double coefCopy = coef;
    output = next - coefCopy * last;
    last = next;
    
    RealZero::setLast(last);
    
    return output;
}

/*! \brief Real, reverse one zero filter */

RealZeroReverse::RealZeroReverse() {
    
}

RealZeroReverse::~RealZeroReverse() {
    
}

double RealZeroReverse::perform(double input, double coef) {
    
    double output = 0;
    
    double last = RealZeroReverse::getLast();
   
    double next = input;
    double coefCopy = coef;
    output = last - coefCopy * next;
    last = next;
    
    RealZeroReverse::setLast(last);
    
    return output;
    
}

/*! \brief Complex one pole filter. */

ComplexPole::ComplexPole() {
    
}

ComplexPole::~ComplexPole() {
    
}

complexOutput ComplexPole::perform(double real, double imag, double realCoef, double imagCoef) {
    
    complexOutput output;
    output.real = 0;
    output.imaginary = 0;
    
    double lastre = ComplexPole::getLastReal();
    double lastim = ComplexPole::getLastImaginary();
    
        double nextre = real;
        double nextim = imag;
        double coefre = realCoef;
        double coefim = imagCoef;
        double tempre = output.real = nextre + lastre * coefre - lastim * coefim;
        lastim = output.imaginary = nextim + lastre * coefim + lastim * coefre;
        lastre = tempre;
    
    if (ComplexPole::PD_BIGORSMALL(lastre))
        lastre = 0;
    if (ComplexPole::PD_BIGORSMALL(lastim))
        lastim = 0;
    ComplexPole::setLastReal(lastre);
    ComplexPole::setLastImaginary(lastim);
    
    return output;
    
}

/*! \brief Complex one zero filter. */

ComplexZero::ComplexZero() {
    
}

ComplexZero::~ComplexZero() {
    
}

complexOutput ComplexZero::perform(double real, double imag, double realCoef, double imagCoef) {

    complexOutput output;
    output.real = 0;
    output.imaginary = 0;
    
    double lastre = ComplexZero::getLastReal();
    double lastim = ComplexZero::getLastImaginary();
   
    double nextre = real;
    double nextim = imag;
    double coefre = realCoef;
    double coefim = imagCoef;
    output.real = nextre - lastre * coefre + lastim * coefim;
    output.imaginary = nextim - lastre * coefim - lastim * coefre;
    lastre = nextre;
    lastim = nextim;
    
    ComplexZero::setLastReal(lastre);
    ComplexZero::setLastImaginary(lastim);
    
    return output;
    
}

/*! \brief Complex, reverse one zero filter. */

ComplexZeroReverse::ComplexZeroReverse() {
    
}

ComplexZeroReverse::~ComplexZeroReverse() {
    
}

complexOutput ComplexZeroReverse::perform(double real, double imag, double realCoef, double imagCoef)
{
    complexOutput output;
    output.real = 0;
    output.imaginary = 0;
    
    double lastre = ComplexZeroReverse::getLastReal();
    double lastim = ComplexZeroReverse::getLastImaginary();
   
    double nextre = real;
    double nextim = imag;
    double coefre = realCoef;
    double coefim = imagCoef;
    /* transfer function is (A bar) - Z^-1, for the same
       frequency response as 1 - AZ^-1 from czero_tilde. */
    output.real = lastre - nextre * coefre - nextim * coefim;
    output.imaginary = lastim - nextre * coefim + nextim * coefre;
    lastre = nextre;
    lastim = nextim;
    
    ComplexZeroReverse::setLastReal(lastre);
    ComplexZeroReverse::setLastImaginary(lastim);
    
    return output;
    
}

} // pd namespace
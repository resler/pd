/*
  MasterObjectList.h
  Pd++

  Created by Esler,Robert Wadhams on 10/15/14.
  Copyright (c) 2014 Robert Esler. All rights reserved.
*/

/*!
    \brief The master list of all Pd++ .h files.
*/

#ifndef Pd___MasterObjectList_h
#define Pd___MasterObjectList_h

//Filters
#include "LowPass.h"
#include "HighPass.h"
#include "BandPass.h"
#include "BiQuad.h"
#include "VoltageControlFilter.h"
#include "SampleHold.h"
#include "RawFilters.h"
#include "Envelope.h"
#include "Threshold.h"
#include "SlewLowPass.h"

//Extras
#include "BobFilter.h"
#include "Sigmund.h"

//Unit Generators
#include "Cosine.h"
#include "Oscillator.h"
#include "Phasor.h"
#include "Noise.h"

//FFT
#include "rFFT.h" // realFFT
#include "rIFFT.h" // realIFFT
#include "cFFT.h" // complexFFT
#include "cIFFT.h" // complexIFFT

//Time
#include "Line.h"
#include "Delay.h"
#include "VariableDelay.h"

//Sound Files and Table Reading
#include "SoundFiler.h"
#include "TabRead4.h"
#include "ReadSoundFile.h"
#include "WriteSoundFile.h"

//My Classes

    //Unit Generators
#include "Sawtooth.h"
#include "SineWave.h"
#include "CosineWave.h"
#include "WhiteNoise.h"
#include "SquareWave.h"
    //Other
#include "SingleSideBandModulation.h"
#include "Convolution.h"
#include "PhaseVocoder.h"
#include "LRShift.h"
    //Time
#include "myLine.h"    
#include "Metro.h"
#include "Timer.h"

#endif

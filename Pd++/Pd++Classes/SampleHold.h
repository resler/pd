//
/* 
  SampleHold.h
  Pd++

  Created by Robert Esler on 11/3/14.
  Copyright (c) 2014 Robert Esler. All rights reserved.

  Some of the code in Pd++ is Copyright (c) 1997-1999 Miller Puckette.
  For information on usage and redistribution, and for a DISCLAIMER OF ALL
  WARRANTIES, see the file, "LICENSE.txt," in this distribution.
*/

#ifndef __Pd____SampleHold__
#define __Pd____SampleHold__

#include <string>
#include "PdMaster.h"

namespace pd {
/*!
 \class SampleHold
 \brief Sample and Hold a number until the control input decreases in value.
     
*/
class SampleHold : public PdMaster {

private:
    double lastIn;
    double lastOut;
    
public:
    
    SampleHold();
    ~SampleHold();
    double perform(double input, double control);
    void reset(double value);
    void set(double value);
    
    std::string pdName = "samphold~";
    
};

} // pd namespace
#endif /* defined(__Pd____SampleHold__) */

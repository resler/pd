/*
 rFFT.cpp
 Pd++
 
 Created by Rob Esler on 9/29/14.
 Copyright (c) 2014 Robert Esler. All rights reserved.
 
 Some of the code in Pd++ is Copyright (c) 1997-1999 Miller Puckette.
 For information on usage and redistribution, and for a DISCLAIMER OF ALL
 WARRANTIES, see the file, "LICENSE.txt," in this distribution.

*/

#ifndef __Pd____cFFT__
#define __Pd____cFFT__

#include <string>
#include "PdMaster.h"
#include "fft.h"

namespace pd {
    
    
/*! \brief Complex Fast Fourier Transform
     
    Outputs a window, or block of samples based on the FFT window.
    Complex FFT returns the real and imaginary parts via a struct.
     
*/

class complexFFT : public PdMaster,  public FFT {

private:
    int windowSize = 64;
    double *realFFT = nullptr;
    double *imagFFT = nullptr;
    double *output = nullptr;
    int count = 0;
        
public:
    complexFFT();
    complexFFT(int);
    ~complexFFT();
    /*! This function returns a block of complex samples, which are the real and imaginary parts.*/
    double *perform(double real, double imaginary);
        
    const std::string pdName = "fft~";
};
    
    
    
} // pd namespace

#endif /* defined(__Pd____cFFT__) */

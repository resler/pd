/*
 rFFT.cpp
 Pd++
 
 Created by Rob Esler on 9/29/14.
 Copyright (c) 2014 Robert Esler. All rights reserved.
 
 Some of the code in Pd++ is Copyright (c) 1997-1999 Miller Puckette.
 For information on usage and redistribution, and for a DISCLAIMER OF ALL
 WARRANTIES, see the file, "LICENSE.txt," in this distribution.
*/

#ifndef __Pd____rFFT__
#define __Pd____rFFT__

#include <string>
#include "PdMaster.h"
#include "fft.h"

namespace pd {
    
/*! \brief Real Fast Fourier Transform
     
  Outputs a window, or block of samples based on the FFT window.
     
*/
    
class realFFT : public PdMaster,  public FFT {
        
private:
    int windowSize = 64;
    double *signal = nullptr;
    int count = 0;
    

public:
    realFFT();
    realFFT(int);
    ~realFFT();
    
    double *perform(double input);
    
    const std::string pdName = "rfft~";
};
    
    
    
} // pd namespace

#endif /* defined(__Pd____rFFT__) */

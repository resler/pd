/*
 Sigmund.hpp
 Pd++
 
 A high quality pitch detector
 
 Created by Robert Esler on 12/5/16.
 Copyright © 2016 Robert Esler. All rights reserved.
 This code retains the same BSD license as the original Pd code.
 */

/* sigmund.c code: Copyright (c) 2005 Miller Puckette.  BSD licensed.  No warranties. */


#include "Sigmund.h"

namespace pd {

/*!
   This will create a Sigmund object that will return pitch and evelope values from the output struct.
 */
Sigmund::Sigmund() {
    x = new t_sigmund;
    
    Sigmund::sigmund_preinit();
    
    //This defaults to the output struct processing pitch and envelope
    if (!x->x_nvarout)
    {
        x->x_varoutv = (t_varout *)Sigmund::resizebytes(x->x_varoutv,
                                                 0, 2*sizeof(t_varout));
        x->x_varoutv[0].v_what = OUT_PITCH;
        x->x_varoutv[1].v_what = OUT_ENV;
        x->x_nvarout = 2;
        x->x_dopitch = 1;
    }

    x->x_infill = 0;
    x->x_countdown = 0;
    Sigmund::sigmund_npts(x->x_npts);
    Sigmund::notefinder_init(&x->x_notefinder);
    Sigmund::clear();
    std::cout << "Default to pitch and envelope output.\n";

    
}
/*!
  This option allows you to choose pitch or notes with their respective envelope.  Pitch will change with every new detection, this could happen every sample.  Notes will only change when it detects a new note (in the MIDI paradigm).  In my experience, note is not very reliable.  The envelope will return the amplitude in dB, similar to the Envelope class.
 */
    
Sigmund::Sigmund(std::string pitch, std::string env) {
    x = new t_sigmund;
    
    Sigmund::sigmund_preinit();
    
    if(!pitch.compare("pitch"))
    {
        int n2 = x->x_nvarout+1;
        x->x_varoutv = (t_varout *)Sigmund::resizebytes(x->x_varoutv,
                                                 x->x_nvarout*sizeof(t_varout), n2*sizeof(t_varout));
        x->x_varoutv[x->x_nvarout].v_what = OUT_PITCH;
        x->x_nvarout = n2;
        x->x_dopitch = 1;
    }
    else if (!pitch.compare("notes") || !pitch.compare("note"))
    {
        int n2 = x->x_nvarout+1;
        x->x_varoutv = (t_varout *)Sigmund::resizebytes(x->x_varoutv,
                                                 x->x_nvarout*sizeof(t_varout), n2*sizeof(t_varout));
        x->x_varoutv[x->x_nvarout].v_what = OUT_NOTE;
        x->x_nvarout = n2;
        x->x_dopitch = x->x_donote = 1;
    }
    else
    {
        std::cout << "Error: Your first argument is not a valid option. Use \'pitch\' or \'notes\', default to pitch and envelope.\n";
        if (!x->x_nvarout)
        {
            x->x_varoutv = (t_varout*)Sigmund::resizebytes(x->x_varoutv,
                0, 2 * sizeof(t_varout));
            x->x_varoutv[0].v_what = OUT_PITCH;
            x->x_varoutv[1].v_what = OUT_ENV;
            x->x_nvarout = 2;
            x->x_dopitch = 1;
        }

    }

    if(!env.compare("envelope") || !env.compare("env"))
    {
        int n2 = x->x_nvarout+1;
        x->x_varoutv = (t_varout *)Sigmund::resizebytes(x->x_varoutv,
                                                 x->x_nvarout*sizeof(t_varout), n2*sizeof(t_varout));
        x->x_varoutv[x->x_nvarout].v_what = OUT_ENV;
        x->x_nvarout = n2;
    }
    else
    {
        std::cout << "Error: Your second argument is not a valid option. Use \'env\' or \'envelope\', default to pitch and envelope.\n";
        int n2 = x->x_nvarout + 1;
        x->x_varoutv = (t_varout*)Sigmund::resizebytes(x->x_varoutv,
            x->x_nvarout * sizeof(t_varout), n2 * sizeof(t_varout));
        x->x_varoutv[x->x_nvarout].v_what = OUT_ENV;
        x->x_nvarout = n2;
    }
   
    x->x_infill = 0;
    x->x_countdown = 0;
    Sigmund::sigmund_npts(x->x_npts);
    Sigmund::notefinder_init(&x->x_notefinder);
    Sigmund::clear();
}

/*!
 
 This option will turn on the peak or track detection.  This will return groups of frequencies that were found to have a clear peak in the fft and return them in order of loudest to quietest.
    Peaks will return five numbers respectively: index, frequency, envelope, real, imaginary
    Tracks will return four numbers respectively: index, frequency, envelope, and a flag (1 = new track, 0 = continuation and -1 = empty track).  I have never used tracks, so I can't elaborate on their usefulness.
 
 */
Sigmund::Sigmund(std::string peakOrTrack, int np) {
    
    x = new t_sigmund;
    
    Sigmund::sigmund_preinit();
    
    if(!peakOrTrack.compare("peak") || !peakOrTrack.compare("peaks"))
    {
        int n2 = x->x_nvarout+1;
        x->x_varoutv = (t_varout *)Sigmund::resizebytes(x->x_varoutv,
                                                 x->x_nvarout*sizeof(t_varout), n2*sizeof(t_varout));
        x->x_varoutv[x->x_nvarout].v_what = OUT_PEAKS;
        x->x_nvarout = n2;
        x->x_npeak = np;
        //double our array size so we can grow and shrink x->x_npeak, check this on
        //the setter function.
        output.peakSize = np*2;
        //allocate memory to our peak array
        output.peaks = new double*[output.peakSize];
        for(int i = 0;  i < output.peakSize; i++ )
            output.peaks[i] = new double[peakCols];
        peakArrayCreated = true;
    }
    else if(!peakOrTrack.compare("track") || !peakOrTrack.compare("tracks"))
    {
        int n2 = x->x_nvarout+1;
        x->x_varoutv = (t_varout *)Sigmund::resizebytes(x->x_varoutv,
                                                 x->x_nvarout*sizeof(t_varout), n2*sizeof(t_varout));
        x->x_varoutv[x->x_nvarout].v_what = OUT_TRACKS;
        x->x_nvarout = n2;
        x->x_dotracks = 1;
        
        //allocate memory to our track array
        x->x_ntrack = np;
        
        //double our array size so we can grow and shrink x->x_ntrack
        output.trackSize = np*2;
        output.tracks = new double*[output.trackSize];
        for(int i = 0;  i < output.trackSize; i++ )
            output.tracks[i] = new double[trackCols];
        trackArrayCreated = true;
    }
    else
    {
        std::cout << "Error: Your argument is not a valid option. Use \'peak\' or \'track\', default to peak.\n";
        int n2 = x->x_nvarout+1;
        x->x_varoutv = (t_varout *)Sigmund::resizebytes(x->x_varoutv,
                                               x->x_nvarout*sizeof(t_varout), n2*sizeof(t_varout));
        x->x_varoutv[x->x_nvarout].v_what = OUT_PEAKS;
        x->x_nvarout = n2;
        
        x->x_npeak = np;
        //allocate memory to our peak array
        //double our array size so we can grow and shrink x->x_npeak
        output.peakSize = np*2;
        output.peaks = new double*[output.peakSize];
        for(int i = 0;  i < output.peakSize; i++ )
            output.peaks[i] = new double[peakCols];
        peakArrayCreated = true;
    }

    if (x->x_dotracks)
    {
        //x->x_ntrack = x->x_npeak;
        x->x_trackv = (t_peak*)getbytes(x->x_ntrack * sizeof(*x->x_trackv));
    }

    x->x_infill = 0;
    x->x_countdown = 0;
    Sigmund::sigmund_npts(x->x_npts);
    Sigmund::notefinder_init(&x->x_notefinder);
    Sigmund::clear();
}

Sigmund::~Sigmund() {
    
    //These are the c style arrays
    if (x->x_inbuf)
    {
        Sigmund::freebytes(x->x_inbuf, x->x_npts * sizeof(*x->x_inbuf));
    }
    if (x->x_trackv)
        Sigmund::freebytes(x->x_trackv, x->x_ntrack * sizeof(*x->x_trackv));
    
    //these are my C++ arrays
    if(peakArrayCreated)
    {
        for (int i=0; i< output.peakSize; i++)
            delete [] output.peaks[i];
        delete [] output.peaks;
    }
    
    if(trackArrayCreated)
    {
        for (int i=0; i< output.trackSize; i++)
            delete [] output.tracks[i];
        delete [] output.tracks;
    }
    
    delete x;//our t_sigmund struct
}

/********************** service routines **************************/

/* these three are adapted from elsewhere in Pd but included here for
 completeness */
int Sigmund::sigmund_ilog2(int n)
{
    int ret = -1;
    while (n)
    {
        n >>= 1;
        ret++;
    }
    return (ret);
}

double Sigmund::sigmund_ftom(double f)
{
    return (f > 0 ? 17.3123405046 * log(.12231220585 * f) : -1500);
}

#define LOGTEN 2.302585092994
double Sigmund::sigmund_powtodb(double f)
{
    if (f <= 0) return (0);
    else
    {
        double val = 100 + 10./LOGTEN * log(f);
        return (val < 0 ? 0 : val);
    }
}

/* parameters for von Hann window (change these to get Hamming if desired) */
#define W_ALPHA 0.5
#define W_BETA 0.5
#define NEGBINS 4   /* number of bins of negative frequency we'll need */

#define PI 3.141592653589793
#define LOG2  0.693147180559945
#define LOG10 2.302585092994046

double Sigmund::sinx(double theta, double sintheta)
{
    if (theta > -0.003 && theta < 0.003)
        return (1);
    else return (sintheta/theta);
}

double Sigmund::window_hann_mag(double pidetune, double sinpidetune)
{
    return (W_ALPHA * sinx(pidetune, sinpidetune)
            - 0.5 * W_BETA *
            (sinx(pidetune+PI, sinpidetune) + sinx(pidetune-PI, sinpidetune)));
}

double Sigmund::window_mag(double pidetune, double cospidetune)
{
    return (sinx(pidetune + (PI/2), cospidetune)
            + sinx(pidetune - (PI/2), -cospidetune));
}

/*********** Routines to analyze a window into sinusoidal peaks *************/
//This is a static function used with qsort in sigumnd_tweak
int Sigmund::sigmund_cmp_freq(const void *p1, const void *p2)
{
    if ((*(t_peak **)p1)->p_freq > (*(t_peak **)p2)->p_freq)
        return (1);
    else if ((*(t_peak **)p1)->p_freq < (*(t_peak **)p2)->p_freq)
        return (-1);
    else return (0);
}

void Sigmund::sigmund_tweak(int npts, double *ftreal, double *ftimag,
                          int npeak, t_peak *peaks, double fperbin, int loud)
{
    t_peak **peakptrs = (t_peak **)alloca(sizeof (*peakptrs) * (npeak+1));
    t_peak negpeak;
    int peaki, j, k;
    double ampreal[3], ampimag[3];
    double binperf = 1./fperbin;
    double phaseperbin = (npts-0.5)/npts, oneovern = 1./npts;
    if (npeak < 1)
        return;
    for (peaki = 0; peaki < npeak; peaki++)
        peakptrs[peaki+1] = &peaks[peaki];
    qsort(peakptrs+1, npeak, sizeof (*peakptrs), sigmund_cmp_freq);
    peakptrs[0] = &negpeak;
    negpeak.p_ampreal = peakptrs[1]->p_ampreal;
    negpeak.p_ampimag = -peakptrs[1]->p_ampimag;
    negpeak.p_freq = -peakptrs[1]->p_freq;
    for (peaki = 1; peaki <= npeak; peaki++)
    {
        int cbin = peakptrs[peaki]->p_freq*binperf + 0.5;
        int nsub = (peaki == npeak ? 1:2);
        double windreal, windimag, windpower, detune, pidetune, sinpidetune,
        cospidetune, ampcorrect, ampout, ampoutreal, ampoutimag, freqout;
        /* post("3 nsub %d amp %f freq %f", nsub,
         peakptrs[peaki]->p_amp, peakptrs[peaki]->p_freq); */
        if (cbin < 0 || cbin > 2*npts - 3)
            continue;
        for (j = 0; j < 3; j++)
            ampreal[j] = ftreal[cbin+2*j-2], ampimag[j] = ftimag[cbin+2*j-2];
        /* post("a %f %f", ampreal[1], ampimag[1]); */
        for (j = 0; j < nsub; j++)
        {
            t_peak *neighbor = peakptrs[(peaki-1) + 2*j];
            double neighborreal = npts * neighbor->p_ampreal;
            double neighborimag = npts * neighbor->p_ampimag;
            for (k = 0; k < 3; k++)
            {
                double freqdiff = (0.5*PI) * ((cbin + 2*k-2)
                                               -binperf * neighbor->p_freq);
                double sx = sinx(freqdiff, sin(freqdiff));
                double phasere = cos(freqdiff * phaseperbin);
                double phaseim = sin(freqdiff * phaseperbin);
                ampreal[k] -=
                sx * (phasere * neighborreal - phaseim * neighborimag);
                ampimag[k] -=
                sx * (phaseim * neighborreal + phasere * neighborimag);
            }
            /* post("b %f %f", ampreal[1], ampimag[1]); */
        }
        
        windreal = W_ALPHA * ampreal[1] -
        (0.5 * W_BETA) * (ampreal[0] + ampreal[2]);
        windimag = W_ALPHA * ampimag[1] -
        (0.5 * W_BETA) * (ampimag[0] + ampimag[2]);
        windpower = windreal * windreal + windimag * windimag;
        detune = (
                  W_BETA*(ampreal[0] - ampreal[2]) *
                  (2.0*W_ALPHA * ampreal[1] - W_BETA * (ampreal[0] + ampreal[2]))
                  +
                  W_BETA*(ampimag[0] - ampimag[2]) *
                  (2.0*W_ALPHA * ampimag[1] - W_BETA * (ampimag[0] + ampimag[2]))
                  ) / (4.0 * windpower);
        if (detune > 0.5)
            detune = 0.5;
        else if (detune < -0.5)
            detune = -0.5;
        /* if (loud > 0)
         post("tweak: windpower %f, bin %d, detune %f",
         windpower, cbin, detune); */
        pidetune = PI * detune;
        sinpidetune = sin(pidetune);
        cospidetune = cos(pidetune);
        
        ampcorrect = 1.0 / window_hann_mag(pidetune, sinpidetune);
        
        ampout = oneovern * ampcorrect *sqrt(windpower);
        ampoutreal = oneovern * ampcorrect *
        (windreal * cospidetune - windimag * sinpidetune);
        ampoutimag = oneovern * ampcorrect *
        (windreal * sinpidetune + windimag * cospidetune);
        freqout = (cbin + 2*detune) * fperbin;
        /* if (loud > 1)
         post("amp %f, freq %f", ampout, freqout); */
        
        peakptrs[peaki]->p_freq = freqout;
        peakptrs[peaki]->p_amp = ampout;
        peakptrs[peaki]->p_ampreal = ampoutreal;
        peakptrs[peaki]->p_ampimag = ampoutimag;
    }
}

void Sigmund::sigmund_remask(int maxbin, int bestindex, double powmask,
                           double maxpower, double *maskbuf)
{
    int bin;
    int bin1 = (bestindex > 52 ? bestindex-50:2);
    int bin2 = (maxbin < bestindex + 50 ? bestindex + 50 : maxbin);
    for (bin = bin1; bin < bin2; bin++)
    {
        double bindiff = bin - bestindex;
        double mymask;
        mymask = powmask/ (1. + bindiff * bindiff * bindiff * bindiff);
        if (bindiff < 2 && bindiff > -2)
            mymask = 2*maxpower;
        if (mymask > maskbuf[bin])
            maskbuf[bin] = mymask;
    }
}

#define PEAKMASKFACTOR 1.
#define PEAKTHRESHFACTOR 0.6

void Sigmund::sigmund_getrawpeaks(int npts, double *insamps,
                                int npeak, t_peak *peakv, int *nfound, double *power, double srate, int loud,
                                double hifreq)
{
    double oneovern = 1.0/ (double)npts;
    double fperbin = 0.5 * srate * oneovern, totalpower = 0;
    int npts2 = 2*npts, i, bin;
    int peakcount = 0;
    double *fp1, *fp2;
    double *rawreal, *rawimag, *maskbuf, *powbuf;
    double *bigbuf = (double *)alloca(sizeof(double) * (2*NEGBINS + 6*npts));
   
    int maxbin = hifreq/fperbin;
    if (maxbin > npts - NEGBINS)
        maxbin = npts - NEGBINS;
    /* if (loud) post("tweak %d", tweak); */
    maskbuf = bigbuf + npts2;
    powbuf = maskbuf + npts;
    rawreal = powbuf + npts+NEGBINS;
    rawimag = rawreal+npts+NEGBINS;
    for (i = 0; i < npts; i++)
        maskbuf[i] = 0;
    
    for (i = 0; i < npts; i++)
        bigbuf[i] = insamps[i];
    for (i = npts; i < 2*npts; i++)
        bigbuf[i] = 0;
    Sigmund::mayer_realfft(npts2, bigbuf);
    for (i = 0; i < npts; i++)
        rawreal[i] = bigbuf[i];
    for (i = 1; i < npts-1; i++)
        rawimag[i] = bigbuf[npts2-i];
    rawreal[-1] = rawreal[1];
    rawreal[-2] = rawreal[2];
    rawreal[-3] = rawreal[3];
    rawreal[-4] = rawreal[4];
    rawimag[0] = rawimag[npts-1] = 0;
    rawimag[-1] = -rawimag[1];
    rawimag[-2] = -rawimag[2];
    rawimag[-3] = -rawimag[3];
    rawimag[-4] = -rawimag[4];
#if 1
    for (i = 0, fp1 = rawreal, fp2 = rawimag; i < maxbin; i++, fp1++, fp2++)
    {
        double x1 = fp1[1] - fp1[-1], x2 = fp2[1] - fp2[-1], p = powbuf[i] = x1*x1+x2*x2;
        if (i >= 2)
            totalpower += p;
    }
    powbuf[maxbin] = powbuf[maxbin+1] = 0;
    *power = 0.5 * totalpower *oneovern * oneovern;
#endif
    for (peakcount = 0; peakcount < npeak; peakcount++)
    {
        double pow1, maxpower = 0, windreal, windimag, windpower,
        detune, pidetune, sinpidetune, cospidetune, ampcorrect, ampout,
        ampoutreal, ampoutimag, freqout, powmask;
        int bestindex = -1;
        
        for (bin = 2, fp1 = rawreal+2, fp2 = rawimag+2;
             bin < maxbin; bin++, fp1++, fp2++)
        {
            pow1 = powbuf[bin];
            if (pow1 > maxpower && pow1 > maskbuf[bin])
            {
                double thresh = PEAKTHRESHFACTOR * (powbuf[bin-2]+powbuf[bin+2]);
                if (pow1 > thresh)
                    maxpower = pow1, bestindex = bin;
            }
        }
        
        if (totalpower <= 0 || maxpower < 1e-10*totalpower || bestindex < 0)
            break;
        fp1 = rawreal+bestindex;
        fp2 = rawimag+bestindex;
        powmask = maxpower * PEAKMASKFACTOR;
        /* if (loud > 2)
         post("maxpower %f, powmask %f, param1 %f",
         maxpower, powmask, param1); */
        Sigmund::sigmund_remask(maxbin, bestindex, powmask, maxpower, maskbuf);
        
        /* if (loud > 1)
         post("best index %d, total power %f", bestindex, totalpower); */
        
        windreal = fp1[1] - fp1[-1];
        windimag = fp2[1] - fp2[-1];
        windpower = windreal * windreal + windimag * windimag;
        detune = ((fp1[1] * fp1[1] - fp1[-1]*fp1[-1])
                  + (fp2[1] * fp2[1] - fp2[-1]*fp2[-1])) / (2 * windpower);
        
        if (detune > 0.5)
            detune = 0.5;
        else if (detune < -0.5)
            detune = -0.5;
        /* if (loud > 1)
         post("windpower %f, index %d, detune %f",
         windpower, bestindex, detune); */
        pidetune = PI * detune;
        sinpidetune = sin(pidetune);
        cospidetune = cos(pidetune);
        ampcorrect = 1.0 / Sigmund::window_mag(pidetune, cospidetune);
        
        ampout = ampcorrect *sqrt(windpower);
        ampoutreal = ampcorrect *
        (windreal * cospidetune - windimag * sinpidetune);
        ampoutimag = ampcorrect *
        (windreal * sinpidetune + windimag * cospidetune);
        
        /* the frequency is the sum of the bin frequency and detuning */
        
        peakv[peakcount].p_freq = (freqout = (bestindex + 2*detune)) * fperbin;
        peakv[peakcount].p_amp = oneovern * ampout;
        peakv[peakcount].p_ampreal = oneovern * ampoutreal;
        peakv[peakcount].p_ampimag = oneovern * ampoutimag;
    }
    Sigmund::sigmund_tweak(npts, rawreal, rawimag, peakcount, peakv, fperbin, loud);
    Sigmund::sigmund_tweak(npts, rawreal, rawimag, peakcount, peakv, fperbin, loud);
    for (i = 0; i < peakcount; i++)
    {
        peakv[i].p_pit = Sigmund::sigmund_ftom(peakv[i].p_freq);
        peakv[i].p_db = Sigmund::sigmund_powtodb(peakv[i].p_amp);
    }
    *nfound = peakcount;
}

/*************** Routines for finding fundamental pitch *************/

#define PITCHNPEAK 12
#define HALFTONEINC 0.059
#define SUBHARMONICS 16
#define DBPERHALFTONE 0.0

void Sigmund::sigmund_getpitch(int npeak, t_peak *peakv, double *freqp,
                             double npts, double srate, double nharmonics, double amppower, int loud)
{
    double fperbin = 0.5 * srate / npts;
    int npit = 48 * sigmund_ilog2(npts), i, j, k, nsalient;
    double bestbin, bestweight, sumamp, sumweight, sumfreq, freq;
    double *weights =  (double *)alloca(sizeof(double) * npit);
    t_peak *bigpeaks[PITCHNPEAK];
    if (npeak < 1)
    {
        freq = 0;
        goto done;
    }
    for (i = 0; i < npit; i++)
        weights[i] = 0;
    for (i = 0; i < npeak; i++)
    {
        peakv[i].p_tmp = 0;
        peakv[i].p_salience = peakv[i].p_db - DBPERHALFTONE * peakv[i].p_pit;
    }
    for (nsalient = 0; nsalient < PITCHNPEAK; nsalient++)
    {
        t_peak *bestpeak = 0;
        double bestsalience = -1e20;
        for (j = 0; j < npeak; j++)
            if (peakv[j].p_tmp == 0 && peakv[j].p_salience > bestsalience)
            {
                bestsalience = peakv[j].p_salience;
                bestpeak = &peakv[j];
            }
        if (!bestpeak)
            break;
        bigpeaks[nsalient] = bestpeak;
        bestpeak->p_tmp = 1;
        /* post("peak f=%f a=%f", bestpeak->p_freq, bestpeak->p_amp); */
    }
    sumweight = 0;
    for (i = 0; i < nsalient; i++)
    {
        t_peak *thispeak = bigpeaks[i];
        double weightindex = (48./LOG2) *
        log(thispeak->p_freq/(2.*fperbin));
        double loudness = pow(thispeak->p_amp, amppower);
        /* post("index %f, uncertainty %f", weightindex, pitchuncertainty); */
        for (j = 0; j < SUBHARMONICS; j++)
        {
            double subindex = weightindex -
            (48./LOG2) * log(j + 1.);
            int loindex = subindex - 0.5;
            int hiindex = loindex+2;
            if (hiindex < 0)
                break;
            if (hiindex >= npit)
                continue;
            if (loindex < 0)
                loindex = 0;
            for (k = loindex; k <= hiindex; k++)
                weights[k] += loudness * nharmonics / (nharmonics + j);
        }
        sumweight += loudness;
    }
    bestbin = -1;
    bestweight = -1e20;
    for (i = 0; i < npit; i++)
        if (weights[i] > bestweight)
            bestweight = weights[i], bestbin = i;
    if (bestweight < sumweight * 0.4)
        bestbin = -1;
    
    if (bestbin < 0)
    {
        freq = 0;
        goto done;
    }
    if (bestbin > 0 && bestbin < npit-1)
    {
        int ibest = bestbin;
        bestbin += (weights[ibest+1] - weights[ibest-1]) /
        (weights[ibest+1] +  weights[ibest] + weights[ibest-1]);
    }
    freq = 2*fperbin * exp((LOG2/48.)*bestbin);
    for (sumamp = sumweight = sumfreq = 0, i = 0; i < nsalient; i++)
    {
        t_peak *thispeak = bigpeaks[i];
        double thisloudness = thispeak->p_amp;
        double thisfreq = thispeak->p_freq;
        double harmonic = thisfreq/freq;
        double intpart = (int)(0.5 + harmonic);
        double inharm = harmonic - intpart;
#if 0
        if (loud)
            printf("freq %f intpart %f inharm %f", freq, intpart, inharm);
#endif
        if (intpart >= 1 && intpart <= 16 &&
            inharm < 0.015 * intpart && inharm > - (0.015 * intpart))
        {
            double weight = thisloudness * intpart;
            sumweight += weight;
            sumfreq += weight*thisfreq/intpart;
#if 0
            if (loud)
                printf("weight %f freq %f", weight, thisfreq);
#endif
        }
    }
    if (sumweight > 0)
        freq = sumfreq / sumweight;
done:
    if (!(freq >= 0 || freq <= 0))
    {
        /* post("freq nan cancelled"); */
        freq = 0;
    }
    *freqp = freq;
}

/*************** gather peak lists into sinusoidal tracks *************/

void Sigmund::sigmund_peaktrack(int ninpeak, t_peak *inpeakv,
                              int noutpeak, t_peak *outpeakv, float maxerror, int loud)
{
    int incnt, outcnt;
    for (outcnt = 0; outcnt < noutpeak; outcnt++)
        outpeakv[outcnt].p_tmp = -1;
    
    /* first pass. Match each "in" peak with the closest previous
     "out" peak, but no two to the same one. */
    for (incnt = 0; incnt < ninpeak; incnt++)
    {
        double besterror = 1e20;
        int bestcnt = -1;
        inpeakv[incnt].p_tmp = -1;
        for (outcnt = 0; outcnt < noutpeak; outcnt++)
        {
            double thiserror;
            if (outpeakv[outcnt].p_amp == 0)
                continue;
            thiserror = inpeakv[incnt].p_freq - outpeakv[outcnt].p_freq;
            if (thiserror < 0)
                thiserror = -thiserror;
            if (thiserror < besterror)
            {
                besterror = thiserror;
                bestcnt = outcnt;
            }
        }
        if (bestcnt >= 0 && besterror < maxerror && outpeakv[bestcnt].p_tmp < 0)
        {
            outpeakv[bestcnt] = inpeakv[incnt];
            inpeakv[incnt].p_tmp = 0;
            outpeakv[bestcnt].p_tmp = 0;
        }
    }
    /* second pass.  Unmatched "in" peaks assigned to free "out"
     peaks */
    for (incnt = 0; incnt < ninpeak; incnt++)
        if (inpeakv[incnt].p_tmp < 0)
        {
            for (outcnt = 0; outcnt < noutpeak; outcnt++)
                if (outpeakv[outcnt].p_tmp < 0)
                {
                    outpeakv[outcnt] = inpeakv[incnt];
                    inpeakv[incnt].p_tmp = 0;
                    outpeakv[outcnt].p_tmp = 1;
                    break;
                }
        }
    for (outcnt = 0; outcnt < noutpeak; outcnt++)
        if (outpeakv[outcnt].p_tmp == -1)
            outpeakv[outcnt].p_amp = 0;
}

/**************** parse continuous pitch into note starts ***************/


void Sigmund::notefinder_init(t_notefinder *x)
{
    int i;
    x->n_peaked = x->n_age = 0;
    x->n_hifreq = x->n_lofreq = 0;
    x->n_histphase = 0;
    for (i = 0; i < NHISTPOINT; i++)
        x->n_hist[i].h_freq =x->n_hist[i].h_power = 0;
}

void Sigmund::notefinder_doit(t_notefinder *x, double freq, double power,
                            double *note, double vibrato, int stableperiod, double powerthresh,
                            double growththresh, int loud)
{
    /* calculate frequency ratio between allowable vibrato extremes
     (equal to twice the vibrato deviation from center) */
    double vibmultiple = exp((2*LOG2/12) * vibrato);
    int oldhistphase, i, k;
    if (stableperiod > NHISTPOINT - 1)
        stableperiod = NHISTPOINT - 1;
    else if (stableperiod < 1)
        stableperiod = 1;
    if (++x->n_histphase == NHISTPOINT)
        x->n_histphase = 0;
    x->n_hist[x->n_histphase].h_freq = freq;
    x->n_hist[x->n_histphase].h_power = power;
    x->n_age++;
    *note = 0;
#if 0
    if (loud)
    {
        post("stable %d, age %d, vibmultiple %f, powerthresh %f, hifreq %f",
             stableperiod, (int)x->n_age ,vibmultiple, powerthresh, x->n_hifreq);
        post("histfreq %f %f %f %f",
             x->n_hist[x->n_histphase].h_freq,
             x->n_hist[(x->n_histphase+NHISTPOINT-1)%NHISTPOINT].h_freq,
             x->n_hist[(x->n_histphase+NHISTPOINT-2)%NHISTPOINT].h_freq,
             x->n_hist[(x->n_histphase+NHISTPOINT-3)%NHISTPOINT].h_freq);
        post("power %f %f %f %f",
             x->n_hist[x->n_histphase].h_power,
             x->n_hist[(x->n_histphase+NHISTPOINT-1)%NHISTPOINT].h_power,
             x->n_hist[(x->n_histphase+NHISTPOINT-2)%NHISTPOINT].h_power,
             x->n_hist[(x->n_histphase+NHISTPOINT-3)%NHISTPOINT].h_power);
        for (i = 0, k = x->n_histphase; i < stableperiod; i++)
        {
            post("pit %5.1f  pow %f", sigmund_ftom(x->n_hist[k].h_freq),
                 x->n_hist[k].h_power);
            if (--k < 0)
                k = NHISTPOINT - 1;
        }
    }
#endif
    /* look for shorter notes than "stableperiod" in length.
     The amplitude must rise and then fall while the pitch holds
     steady. */
    if (x->n_hifreq <= 0 && x->n_age > stableperiod)
    {
        double maxpow = 0, freqatmaxpow = 0,
        localhifreq = -1e20, locallofreq = 1e20;
        int startphase = x->n_histphase - stableperiod + 1;
        if (startphase < 0)
            startphase += NHISTPOINT;
        for (i = 0, k = startphase; i < stableperiod; i++)
        {
            if (x->n_hist[k].h_freq <= 0)
                break;
            if (x->n_hist[k].h_power > maxpow)
                maxpow = x->n_hist[k].h_power,
                freqatmaxpow = x->n_hist[k].h_freq;
            if (x->n_hist[k].h_freq > localhifreq)
                localhifreq = x->n_hist[k].h_freq;
            if (x->n_hist[k].h_freq < locallofreq)
                locallofreq = x->n_hist[k].h_freq;
            if (localhifreq > locallofreq * vibmultiple)
                break;
            if (maxpow > power * growththresh &&
                maxpow > x->n_hist[startphase].h_power * growththresh &&
                localhifreq < vibmultiple * locallofreq
                && freqatmaxpow > 0 && maxpow > powerthresh)
            {
                x->n_hifreq = x->n_lofreq = *note = freqatmaxpow;
                x->n_age = 0;
                x->n_peaked = 0;
                /* post("got short note"); */
                return;
            }
            if (++k >= NHISTPOINT)
                k = 0;
        }
        
    }
    if (x->n_hifreq > 0)
    {
        /* test if we're within "vibrato" range, and if so update range */
        if (freq * vibmultiple >= x->n_hifreq &&
            x->n_lofreq * vibmultiple >= freq)
        {
            if (freq > x->n_hifreq)
                x->n_hifreq = freq;
            if (freq < x->n_lofreq)
                x->n_lofreq = freq;
        }
        else if (x->n_hifreq > 0 && x->n_age > stableperiod)
        {
            /* if we've been out of range at least 1/2 the
             last "stableperiod+1" analyses, clear the note */
            int nbad = 0;
            for (i = 0, k = x->n_histphase; i < stableperiod + 1; i++)
            {
                if (--k < 0)
                    k = NHISTPOINT - 1;
                if (x->n_hist[k].h_freq * vibmultiple <= x->n_hifreq ||
                    x->n_lofreq * vibmultiple <= x->n_hist[k].h_freq)
                    nbad++;
            }
            if (2 * nbad >= stableperiod + 1)
            {
                x->n_hifreq = x->n_lofreq = 0;
                x->n_age = 0;
            }
        }
    }
    
    oldhistphase = x->n_histphase - stableperiod;
    if (oldhistphase < 0)
        oldhistphase += NHISTPOINT;
    
    /* look for envelope attacks */
    
    if (x->n_hifreq > 0 && x->n_peaked)
    {
        if (freq > 0 && power > powerthresh &&
            power > x->n_hist[oldhistphase].h_power *
            exp((LOG10*0.1)*growththresh))
        {
            /* clear it and fall through for new stable-note test */
            x->n_peaked = 0;
            x->n_hifreq = x->n_lofreq = 0;
            x->n_age = 0;
        }
    }
    else if (!x->n_peaked)
    {
        if (x->n_hist[oldhistphase].h_power > powerthresh &&
            x->n_hist[oldhistphase].h_power > power)
            x->n_peaked = 1;
    }
    
    /* test for a new note using a stability criterion. */
    
    if (freq >= 0 &&
        (x->n_hifreq <= 0 || freq > x->n_hifreq || freq < x->n_lofreq))
    {
        double testfhi = freq, testflo = freq,
        maxpow = x->n_hist[x->n_histphase].h_freq;
        for (i = 0, k = x->n_histphase; i < stableperiod-1; i++)
        {
            if (--k < 0)
                k = NHISTPOINT - 1;
            if (x->n_hist[k].h_freq > testfhi)
                testfhi = x->n_hist[k].h_freq;
            if (x->n_hist[k].h_freq < testflo)
                testflo = x->n_hist[k].h_freq;
            if (x->n_hist[k].h_power > maxpow)
                maxpow = x->n_hist[k].h_power;
        }
#if 0
        if (loud)
            post("freq %.2g testfhi %.2g  testflo %.2g maxpow %.2g",
                 freq, testfhi, testflo, maxpow);
#endif
        if (testflo > 0 && testfhi <= vibmultiple * testflo
            && maxpow > powerthresh)
        {
            /* report new note */
            double sumf = 0, sumw = 0, thisw;
            for (i = 0, k = x->n_histphase; i < stableperiod; i++)
            {
                thisw = x->n_hist[k].h_power;
                sumw += thisw;
                sumf += thisw*x->n_hist[k].h_freq;
                if (--k < 0)
                    k = NHISTPOINT - 1;
            }
            x->n_hifreq = x->n_lofreq = *note = (sumw > 0 ? sumf/sumw : 0);
#if 0
            /* debugging printout */
            for (i = 0; i < stableperiod; i++)
            {
                int k3 = x->n_histphase - i;
                if (k3 < 0)
                    k3 += NHISTPOINT;
                startpost("%5.1f ", sigmund_ftom(x->n_hist[k3].h_freq));
            }
            post("");
#endif
            x->n_age = 0;
            x->n_peaked = 0;
            return;
        }
    }
    *note = 0;
    return;
}

void Sigmund::sigmund_preinit()
{
    x->x_npts = NPOINTS_DEF;
    x->x_param1 = 6;
    x->x_param2 = 0.5;
    x->x_param3 = 0;
    x->x_hop = HOP_DEF;
    x->x_mode = MODE_STREAM;
    x->x_npeak = NPEAK_DEF;
    x->x_vibrato = VIBRATO_DEF;
    x->x_stabletime = STABLETIME_DEF;
    x->x_growth = GROWTH_DEF;
    x->x_minpower = MINPOWER_DEF;
    x->x_maxfreq = 1000000;
    x->x_loud = 0;
    x->x_sr = 1;
    x->x_nvarout = 0;
    x->x_varoutv = (t_varout *)Sigmund::getbytes(0);
    x->x_trackv = 0;
    x->x_ntrack = 0;
    x->x_dopitch = 1;
    x->x_donote = x->x_dotracks = 0;
    x->x_inbuf = 0;

}

void Sigmund::sigmund_npts(double f)
{
    int nwas = x->x_npts, npts = f;
    /* check parameter ranges */
    if (npts < NPOINTS_MIN)
        printf("sigmund~: minimum points %d", NPOINTS_MIN),
        npts = NPOINTS_MIN;
    if (npts != (1 << sigmund_ilog2(npts)))
        printf("sigmund~: adjusting analysis size to %d points",
             (npts = (1 << sigmund_ilog2(npts))));
    if (npts != nwas)
        x->x_countdown = x->x_infill  = 0;
    if (x->x_mode == MODE_STREAM)
    {
        if (x->x_inbuf)
        {
            x->x_inbuf = (double *)Sigmund::resizebytes(x->x_inbuf,
                                                   sizeof(*x->x_inbuf) * nwas, sizeof(*x->x_inbuf) * npts);
        }
        else
        {
            x->x_inbuf = (double *)Sigmund::getbytes(sizeof(*x->x_inbuf) * npts);
            memset((char *)(x->x_inbuf), 0, sizeof(*x->x_inbuf) * npts);
        }
    }
    else x->x_inbuf = 0;
    x->x_npts = npts;
}

void Sigmund::sigmund_hop(double f)
{
    x->x_hop = f;
    /* check parameter ranges */
    if (x->x_hop != (1 << Sigmund::sigmund_ilog2(x->x_hop)))
        printf("sigmund~: adjusting analysis size to %d points",
               (x->x_hop = (1 << Sigmund::sigmund_ilog2(x->x_hop))));
}

void Sigmund::sigmund_npeak(double f)
{
    if (f > output.peakSize)
    {
        f = output.peakSize;
        printf("You cannot exceed 2*npeak when this object is intialized. Default to %d", output.peakSize);
    }
    if(f < 1)
        f = 1;
    x->x_npeak = f;
}

void Sigmund::sigmund_maxfreq(double f)
{
    x->x_maxfreq = f;
}

void Sigmund::sigmund_vibrato(double f)
{
    if (f < 0)
        f = 0;
    x->x_vibrato = f;
}

void Sigmund::sigmund_stabletime(double f)
{
    if (f < 0)
        f = 0;
    x->x_stabletime = f;
}

void Sigmund::sigmund_growth(double f)
{
    if (f < 0)
        f = 0;
    x->x_growth = f;
}

void Sigmund::sigmund_minpower(double f)
{
    if (f < 0)
        f = 0;
    x->x_minpower = f;
}

void Sigmund::sigmund_doit(int npts, double *arraypoints,
                         int loud, double srate)
{
    t_peak *peakv = (t_peak *)alloca(sizeof(t_peak) * x->x_npeak);
    int nfound, i, cnt;
    double freq = 0, power, note = 0;
    Sigmund::sigmund_getrawpeaks(npts, arraypoints, x->x_npeak, peakv,
                        &nfound, &power, srate, loud, x->x_maxfreq);
    if (x->x_dopitch)
        Sigmund::sigmund_getpitch(nfound, peakv, &freq, npts, srate,
                         x->x_param1, x->x_param2, loud);
    if (x->x_donote)
        Sigmund::notefinder_doit(&x->x_notefinder, freq, power, &note, x->x_vibrato,
                        1 + x->x_stabletime * 0.001 * srate / (double)x->x_hop,
                        exp(LOG10*0.1*(x->x_minpower - 100)), x->x_growth, loud);
    if (x->x_dotracks)
        Sigmund::sigmund_peaktrack(nfound, peakv, x->x_ntrack, x->x_trackv,
                          2* srate / npts, loud);
    for (cnt = x->x_nvarout; cnt--;)
    {
        t_varout *v = &x->x_varoutv[cnt];
        
        switch (v->v_what)
        {
            case OUT_PITCH:
                output.pitch = Sigmund::sigmund_ftom(freq);
                break;
            case OUT_ENV:
                output.envelope = Sigmund::sigmund_powtodb(power);
                break;
            case OUT_NOTE:
                if (note > 0)
                    output.notes = Sigmund::sigmund_ftom(note);
                break;
            case OUT_PEAKS:
                for (i = 0; i < nfound; i++)
                {
                    output.peaks[i][0] = (double)i;
                    output.peaks[i][1] = peakv[i].p_freq;
                    output.peaks[i][2] = 2*peakv[i].p_amp;
                    output.peaks[i][3] = 2*peakv[i].p_ampreal;
                    output.peaks[i][4] = 2*peakv[i].p_ampimag;
                }
                break;
            case OUT_TRACKS:
                for (i = 0; i < x->x_ntrack; i++)
                {
                    output.tracks[i][0] = (double)i;
                    output.tracks[i][1] = x->x_trackv[i].p_freq;
                    output.tracks[i][2] = 2*x->x_trackv[i].p_amp;
                    output.tracks[i][3] = x->x_trackv[i].p_tmp;
                }
                break;
        }
    }
}



sigmundPackage Sigmund::perform(double input){
   
    x->x_sr = Sigmund::getSampleRate();
 
    int n = 1;
    
    if (x->x_hop % n)
        return output;
    if (x->x_countdown > 0)
        x->x_countdown -= n;
    else if (x->x_infill != x->x_npts)
    {
        int j;
        double *fp = x->x_inbuf + x->x_infill;
        for (j = 0; j < n; j++)
            *fp++ = input;
        x->x_infill += n;

        if (x->x_infill == x->x_npts)
        {
            //This is where output will be updated
            Sigmund::sigmund_doit(x->x_npts, x->x_inbuf, x->x_loud, x->x_sr);
            if (x->x_hop >= x->x_npts)
            {
                x->x_infill = 0;
                x->x_countdown = x->x_hop - x->x_npts;
            }
            else
            {
                memmove(x->x_inbuf, x->x_inbuf + x->x_hop,
                        (x->x_infill = x->x_npts - x->x_hop) * sizeof(*x->x_inbuf));
                x->x_countdown = 0;
            }
            if (x->x_loud)
                x->x_loud--;
        }
    }
    
    return output;
}

/*!

This doesn't seem to be doing much, so probably don't use it unless you know why.
*/
void Sigmund::setMode(int mode) {
    x->x_mode = mode;
}

void Sigmund::setNumOfPoints(double n) {
    Sigmund::sigmund_npts(n);
}

void Sigmund::setHop(double h) {
    Sigmund::sigmund_hop(h);
}
    /*! This changes the number of peaks that the class will detect.  This will max out at
     2*np when the object is created, so you cannot exceed this.  I would recommend not using this, unless you really need to, plus it not well tested what happens when this is changed a lot.  Look at the various constructors for more information.
     */

void Sigmund::setNumOfPeaks(double np) {
    Sigmund::sigmund_npeak(np);
}

void Sigmund::setMaxFrequency(double mf) {
    Sigmund::sigmund_maxfreq(mf);
}

void Sigmund::setVibrato(double v) {
    Sigmund::sigmund_vibrato(v);
}

void Sigmund::setStableTime(double st) {
    Sigmund::sigmund_stabletime(st);
}
void Sigmund::setMinPower(double mp) {
    Sigmund::sigmund_minpower(mp);
}
void Sigmund::setGrowth(double g) {
    Sigmund::sigmund_growth(g);
}

/*!
 This reads from an array instead of an input signal.  Use this routine instead of perform()
 and pass a valid array with appropriate arguments.  The arguments are: pointer to the array, array size (should be power of 2), index to start at (typically 0), sample rate (this could be file specfic), debug mode.  This routine doesn't do any size check on the array, so make sure you do this beforehand. 
 
 */
sigmundPackage Sigmund::list(double *array, int numOfPoints, int index, long sr, int debug) {
    
    int npts = numOfPoints;
    int onset = index;
    double srate = (double)sr;
    int loud = debug;
    int arraysize, i;
    double *arraypoints;
    
    
    if (npts < 64 || npts != (1 << Sigmund::sigmund_ilog2(npts)))
    {
        printf("Error: sigmund: bad numOfPoints");
        return output;
    }
    if (onset < 0)
    {
        printf("Error: sigmund: negative index");
        return output;
    }
    arraypoints = (double *)alloca(sizeof(double)*npts);
    arraysize = npts;
    if (arraysize < onset + npts)
    {
        printf("Error: Array too small");
        return output;
    }
    if (arraysize < npts)
    {
        printf("Error: sigmund~: too few points in array");
        return output;
    }
    for (i = 0; i < npts; i++)
        arraypoints[i] = array[i+onset];
    Sigmund::sigmund_doit(npts, arraypoints, loud, srate);
    
    return output;
}
void Sigmund::clear() {
    if (x->x_trackv)
        memset(x->x_trackv, 0, x->x_ntrack * sizeof(*x->x_trackv));
    x->x_infill = x->x_countdown = 0;
}

void *Sigmund::getbytes(size_t nbytes)
{
    void *ret;
    if (nbytes < 1) nbytes = 1;
    ret = (void *)calloc(nbytes, 1);
#ifdef LOUD
    fprintf(stderr, "new  %lx %d\n", (int)ret, nbytes);
#endif /* LOUD */
#ifdef DEBUGMEM
    totalmem += nbytes;
#endif
    if (!ret)
        printf("pd: getbytes() failed -- out of memory");
    return (ret);
}

void *Sigmund::resizebytes(void *old, size_t oldsize, size_t newsize)
{
    void *ret;
    if (newsize < 1) newsize = 1;
    if (oldsize < 1) oldsize = 1;
    ret = (void *)realloc((char *)old, newsize);
    if (newsize > oldsize && ret)
        memset(((char *)ret) + oldsize, 0, newsize - oldsize);
#ifdef LOUD
    fprintf(stderr, "resize %lx %d --> %lx %d\n", (int)old, oldsize, (int)ret, newsize);
#endif /* LOUD */
#ifdef DEBUGMEM
    totalmem += (newsize - oldsize);
#endif
    if (!ret)
        printf("pd: resizebytes() failed -- out of memory");
    return (ret);
}

void Sigmund::freebytes(void *fatso, size_t nbytes)
{
    if (nbytes == 0)
        nbytes = 1;
#ifdef LOUD
    fprintf(stderr, "free %lx %d\n", (int)fatso, nbytes);
#endif /* LOUD */
#ifdef DEBUGMEM
    totalmem -= nbytes;
#endif
    free(fatso);
}


void Sigmund::print()
{
    printf("sigmund~ settings:");
    printf("npts %d", (int)x->x_npts);
    printf("hop %d", (int)x->x_hop);
    printf("npeak %d", (int)x->x_npeak);
    printf("maxfreq %g", x->x_maxfreq);
    printf("vibrato %g", x->x_vibrato);
    printf("stabletime %g", x->x_stabletime);
    printf("growth %g", x->x_growth);
    printf("minpower %g", x->x_minpower);
    x->x_loud = 1;
}

}//namespace pd

/*
 rIFFT.cpp
 Pd++
 
 Created by Rob Esler on 9/29/14.
 Copyright (c) 2014 Robert Esler. All rights reserved.
 
 Some of the code in Pd++ is Copyright (c) 1997-1999 Miller Puckette.
 For information on usage and redistribution, and for a DISCLAIMER OF ALL
 WARRANTIES, see the file, "LICENSE.txt," in this distribution.

*/

/* Complex Inverse Fast Fourier Transform.
 
 This is an emulation of the ifft~ object.  It takes a window of
 frequency bins, or samples, and resynthesizes them back into the time domain.
 This is different from several other Pd++ classes as it
 accepts a double*, which is a c-style reference to our input window.
 Complex Inverse Fast Fourier Transform provides the real and imaginary values
 of an FFT.  See FFT or fft_mayer.c for more information on the algorithm.
 
 */
#include "cIFFT.h"

namespace pd {
    
    
/*! If you would like to change the window size, default is 64, use ::setFFTWindow()
 which is inherited from PdMaster.*/
complexIFFT::complexIFFT() {
    
    windowSize = complexIFFT::getFFTWindow();
    realIFFT = new (std::nothrow)double[windowSize] {};
    imagIFFT = new (std::nothrow)double[windowSize] {};
}
/*! Copy constructor. You can manually set the window size here. */
complexIFFT::complexIFFT(int ws) {
        
    windowSize = ws;
    complexIFFT::setFFTWindow(windowSize);
    realIFFT = new (std::nothrow)double[windowSize] {};
    imagIFFT = new (std::nothrow)double[windowSize] {};
}
    
complexIFFT::~complexIFFT() {
    delete [] realIFFT;
    delete [] imagIFFT;
    delete [] output;
}
    
double *complexIFFT::perform(double *input) {
        
    int j = 0;
    
    if(count == 0)
    {
        /*Copy the input to the real/imag arrays 
          The real and imag are interleaved. */
        for (int i = 0; i < complexIFFT::getFFTWindow(); i++)
        {
            realIFFT[i] = input[j++];
            imagIFFT[i] = input[j++];
        }
        
        /*Perform the FFT, the real and imag inputs are passed by reference pointers.*/
        complexIFFT::mayer_ifft(complexIFFT::getFFTWindow(), realIFFT, imagIFFT);
    }
    
    output[0] = realIFFT[count];
    output[1] = imagIFFT[count];
    
    count++;
    if( count == complexIFFT::getFFTWindow() ) count = 0;
    
    return output;
        
}
    
    
    
} // pd namespace

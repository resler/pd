/*
 Threshold.cpp
 Pd++
 
 Created by Robert Esler on 11/28/16.
 Copyright (c) 2016 Robert Esler. All rights reserved.
 
 Some of the code in Pd++ is Copyright (c) 1997-1999 Miller Puckette.
 For information on usage and redistribution, and for a DISCLAIMER OF ALL
 WARRANTIES, see the file, "LICENSE.txt," in this distribution.
 */

#ifndef Threshold_h
#define Threshold_h

#include <iostream>
#include <string>
#include "PdMaster.h"
namespace pd {
    
/*! \brief A threshold detector with debounce time.
 
 */

class Threshold : public PdMaster {
    
private:
   
    int state = 0;                /* 1 = high, 0 = low */
    double highThreshold;           /* value of high threshold */
    double lowThreshold;           /* value of low threshold */
    double deadWait = 0;           /* msec remaining in dead period */
    double msecPerTick;        /* msec per DSP tick */
    double highDebounce;         /* hi debounce in msec */
    double lowDebounce;         /* lo debounce in msec */
    
public:
    Threshold();
    Threshold(double ht, double hd, double lt, double ld);
    ~Threshold();
    int perform(double input); //returns 1 if you went above the threshold, or 0 if you went below the low threshold. -1 means neither, or that nothing has happened yet.
    void setValues(double ht, double hd, double lt, double ld);
    void setState(int s);
    
    
    
    std::string pdName = "threshold~";
    
};
} //namespace pd
#endif /* Threshold_h */

/*
 rIFFT.cpp
 Pd++
 
 Created by Rob Esler on 9/29/14.
 Copyright (c) 2014 Robert Esler. All rights reserved.

 Some of the code in Pd++ is Copyright (c) 1997-1999 Miller Puckette.
 For information on usage and redistribution, and for a DISCLAIMER OF ALL
 WARRANTIES, see the file, "LICENSE.txt," in this distribution.
*/

#ifndef __Pd____rIFFT__
#define __Pd____rIFFT__

#include <string>
#include "PdMaster.h"
#include "fft.h"

namespace pd {

/*! \brief Real Inverse Fast Fourier Transform.
     
     This is an emulation of the rifft~ object.  It takes a window of
     samples and resynthesizes them back into the time domain.
     This is different from several other Pd++ classes as it
     accepts a double*, which is a c-style reference to our input window.
     Real Inverse Fast Fourier Transform only provides the real (or cosine part I believe)
     of an FFT.  See FFT or fft_mayer.c for more information on the algorithm.
     
*/
    
class realIFFT : public PdMaster,  public FFT {
        
private:
    int windowSize = 64;
    double *signal = nullptr;
    bool first = true;
    int count = 0;
    
    
public:
    realIFFT();
    realIFFT(int);
    ~realIFFT();
        
    double perform(double *input);
    
    const std::string pdName = "rifft~";
};
    
    
    
} // pd namespace

#endif /* defined(__Pd____rFFT__) */


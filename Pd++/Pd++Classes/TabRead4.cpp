/*
  TabRead4.cpp
  Pd++

  Created by Esler,Robert Wadhams on 10/13/14.
  Copyright (c) 2014 Robert Esler. All rights reserved.

  Some of the code in Pd++ is Copyright (c) 1997-1999 Miller Puckette.
  For information on usage and redistribution, and for a DISCLAIMER OF ALL
  WARRANTIES, see the file, "LICENSE.txt," in this distribution.
*/


/* A 4-point table lookup routine.
 
 This class will read from an array or vector using 4-point polynomial interpolation.
 
 Remember when using this class to read table values 1 to n-2 to get meaningful data.
 Otherwise you will just get zeros at the end of your table reads.
 
 This class could be used for creating wavetable synthesizers.
 
*/
#include "TabRead4.h"

namespace pd {

    
TabRead4::TabRead4() {
   
}
    
TabRead4::TabRead4(std::vector<double> array) {
    
    TabRead4::setTable(array);
    x_npoints = (int)theArray.size();

}
    
TabRead4::~TabRead4() {}
 
tableData TabRead4::getTableData(int index) {
    
    tableData tData;
    
    if (index == 0)
    {
        tData.point1 = 0;
        tData.point2 = theArray[index];
        tData.point3 = theArray[index+1];
        tData.point4 = theArray[index+2];
    }
    else if (index > TabRead4::getTableSize()-3)
    {
        tData.point1 = 0;
        tData.point2 = 0;
        tData.point3 = 0;
        tData.point4 = 0;
    }
    else
    {
        tData.point1 = theArray[index-1];
        tData.point2 = theArray[index];
        tData.point3 = theArray[index+1];
        tData.point4 = theArray[index+2];
    }
    
    
        
    return tData;
        
}
    
void TabRead4::setTable(std::vector<double> table) {
    
    theArray = table;
}

/*code adapted from Miller Puckette's Pd source. File: d_array.c*/
double TabRead4::perform(double input) {
    
    
    double output = 0;
    int maxindex;
    
    
    double onset = TabRead4::getOnset();
    maxindex = (int)TabRead4::getTableSize() - 3;
    if(maxindex<0)  output = 0;;
    size_t buf = TabRead4::getTableSize();
    
    if (!buf)  output = 0;

    double findex = input + onset;
    int index = findex;
    double frac,  a,  b,  c,  d, cminusb;
    
    if (index < 1)
        index = 1, frac = 0;
    else if (index > maxindex)
        index = maxindex, frac = 1;
    else frac = findex - index;
    
    tableData tData = TabRead4::getTableData(index);
    /*Get the data points: previous, current, next, next+1*/
    a = tData.point1;
    b = tData.point2;
    c = tData.point3;
    d = tData.point4;
    cminusb = c-b; 
    output = b + frac * (cminusb - 0.1666667f * (1.-frac) *
                        ( (d - a - 3.0f * cminusb) * frac + (d + 2.0f*a - 3.0f*b) ) );
    
    return output;
    
}


size_t TabRead4::getTableSize() {

    size_t size = theArray.size();
    return size;
    
}

/*This mimics the second inlet of tabread4~ which is the onset.
 The onset allows for reading later in a table.*/
void TabRead4::setOnset(double os) {
    x_onset = os;
}
    
double TabRead4::getOnset() {
    return x_onset;
}

} // pd namespace
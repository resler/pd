/*
 ReadSoundFile.cpp
 Pd++

 alpha release, not fully tested.

 This code reads an uncompressed file formats from disk
 and stream them in chunks. It won't read compressed audio
 like .mp3, .m4a, .flac, .ogg, etc.

 Unlike SoundFiler which reads an entire file into RAM,
 ReadSoundFile only stores a small bit in RAM then plays it
 back immediately.  It's slower to play files this way,
 but saves on RAM.

 This code is not threaded (yet)

 Created by Esler,Robert Wadhams on 7/26/2021.
 Copyright (c) 2021 Robert Esler. All rights reserved.

 Some of the code in Pd++ is Copyright (c) 1997-1999 Miller Puckette.
 For information on usage and redistribution, and for a DISCLAIMER OF ALL
 WARRANTIES, see the file, "LICENSE.txt," in this distribution.
*/

#ifndef __Pd____ReadSoundFile__
#define __Pd____ReadSoundFile__

#include <string>
#include <vector>
#include <math.h>
#include "FileWvIn.h"
#include "Stk.h"
#include "PdMaster.h"

namespace pd {

class ReadSoundFile : public PdMaster, public stk::Stk {


public: 

	ReadSoundFile();
	~ReadSoundFile();
	bool open(std::string file);
	bool open(std::string file, double onset);
	double* start();
	void stop();
	void print();
	void setBufferSize(int bytes);
	int getBufferSize();
    int getChannels() {return channels;};
	bool isComplete();

	const std::string pdName = "readsf~";

private:
	stk::FileWvIn *input = nullptr;
	stk::StkFrames array;
	bool stopPlackback = false;
	bool bufferIsSet = false;
	long sampleCounter = 0;
	int channels = 2;
	double onset = 0;
	unsigned int bufferSize = 1024;
	double *output = nullptr;
};


}

#endif

//
//  BobFilter.cpp
//  Pd++
//
//  Created by Robert Esler on 11/30/16.
//  Copyright © 2016 Robert Esler. All rights reserved.
//

#include "BobFilter.h"

namespace pd {

/* 
 This function is not used in the original Pd code.  I will included for now as an overloaded 
 function in case in other versions this function is used.
 Imitate the (tanh) clipping function of a transistor pair.
 */
FLOAT BobFilter::clip(FLOAT value, FLOAT saturation, FLOAT saturationinverse, void *none) {
    return (saturation * tanh(value * saturationinverse));
}

/* cheaper way - to 4th order, tanh is x - x*x*x/3; this cubic's
 plateaus are at +/- 1 so clip to 1 and evaluate the cubic.
 This is pretty coarse - for instance if you clip a sinusoid this way you
 can sometimes hear the discontinuity in 4th derivative at the clip point */
FLOAT BobFilter::clip(FLOAT value, FLOAT saturation, FLOAT saturationinverse) {
    float v2 = (value*saturationinverse > 1 ? 1 :
                (value*saturationinverse < -1 ? -1:
                 value*saturationinverse));
    return (saturation * (v2 - (1./3.) * v2 * v2 * v2));
}


void BobFilter::calc_derivatives(FLOAT *dstate, FLOAT *state, t_params *params) {
    
    FLOAT k = ((float)(2*3.14159)) * params->p_cutoff;
    FLOAT sat = params->p_saturation, satinv = 1./sat;
    FLOAT satstate0 = clip(state[0], sat, satinv);
    FLOAT satstate1 = clip(state[1], sat, satinv);
    FLOAT satstate2 = clip(state[2], sat, satinv);
    dstate[0] = k *
    (clip(params->p_input - params->p_resonance * state[3], sat, satinv)
     - satstate0);
    dstate[1] = k * (satstate0 - satstate1);
    dstate[2] = k * (satstate1 - satstate2);
    dstate[3] = k * (satstate2 - clip(state[3], sat, satinv));
    
}
void BobFilter::solver_euler(FLOAT *state, FLOAT *errorestimate, FLOAT stepsize, t_params *params) {
    
    FLOAT cumerror = 0;
    int i;
    FLOAT derivatives[DIM];
    calc_derivatives(derivatives, state, params);
    *errorestimate = 0;
    for (i = 0; i < DIM; i++)
    {
        state[i] += stepsize * derivatives[i];
        *errorestimate += (derivatives[i] > params->p_derivativeswere[i] ?
                           derivatives[i] - params->p_derivativeswere[i] :
                           params->p_derivativeswere[i] - derivatives[i]);
    }
    for (i = 0; i < DIM; i++)
        params->p_derivativeswere[i] = derivatives[i];
    
}

void BobFilter::solver_rungekutte(FLOAT *state, FLOAT *errorestimate, FLOAT stepsize, t_params *params) {
    
    FLOAT cumerror = 0;
    int i;
    FLOAT deriv1[DIM], deriv2[DIM], deriv3[DIM], deriv4[DIM], tempstate[DIM];
    FLOAT oldstate[DIM], backstate[DIM];
#if CALCERROR
    for (i = 0; i < DIM; i++)
        oldstate[i] = state[i];
#endif
    *errorestimate = 0;
    calc_derivatives(deriv1, state, params);
    for (i = 0; i < DIM; i++)
        tempstate[i] = state[i] + 0.5 * stepsize * deriv1[i];
    calc_derivatives(deriv2, tempstate, params);
    for (i = 0; i < DIM; i++)
        tempstate[i] = state[i] + 0.5 * stepsize * deriv2[i];
    calc_derivatives(deriv3, tempstate, params);
    for (i = 0; i < DIM; i++)
        tempstate[i] = state[i] + stepsize * deriv3[i];
    calc_derivatives(deriv4, tempstate, params);
    for (i = 0; i < DIM; i++)
        state[i] += (1./6.) * stepsize *
        (deriv1[i] + 2 * deriv2[i] + 2 * deriv3[i] + deriv4[i]);
#if CALCERROR
    calc_derivatives(deriv1, state, params);
    for (i = 0; i < DIM; i++)
        tempstate[i] = state[i] - 0.5 * stepsize * deriv1[i];
    calc_derivatives(deriv2, tempstate, params);
    for (i = 0; i < DIM; i++)
        tempstate[i] = state[i] - 0.5 * stepsize * deriv2[i];
    calc_derivatives(deriv3, tempstate, params);
    for (i = 0; i < DIM; i++)
        tempstate[i] = state[i] - stepsize * deriv3[i];
    calc_derivatives(deriv4, tempstate, params);
    for (i = 0; i < DIM; i++)
    {
        backstate[i] = state[i ]- (1./6.) * stepsize *
        (deriv1[i] + 2 * deriv2[i] + 2 * deriv3[i] + deriv4[i]);
        *errorestimate += (backstate[i] > oldstate[i] ?
                           backstate[i] - oldstate[i] : oldstate[i] - backstate[i]);
    }
#endif
    
}

BobFilter::BobFilter() {
    BobFilter::clear();
    BobFilter::setSaturation(3);
    BobFilter::setOversampling(2);
#ifdef CALCERROR
    x_cumerror = 0;
    x_errorcount = 0;
#endif
}

BobFilter::~BobFilter() {
    
}

double BobFilter::perform(double input) {
    double output = 0;
    
    FLOAT stepsize = 1./(x_oversample * BobFilter::getSampleRate());
    FLOAT errorestimate;
    
    x_params.p_input = input;
    x_params.p_cutoff = BobFilter::getCutoffFrequency();
    if ((x_params.p_resonance = BobFilter::getResonance()) < 0)
        x_params.p_resonance = 0;
    for (int j = 0; j < x_oversample; j++)
        solver_rungekutte(x_state, &errorestimate,
                          stepsize, &x_params);
    output = x_state[0];
#if CALCERROR
    x_cumerror += errorestimate;
    x_errorcount++;
#endif
    return output;
}

void BobFilter::setCutoffFrequency(double cf) {
    x_params.p_cutoff = cf;
}

void BobFilter::setResonance(double r) {
    x_params.p_resonance = r;
}

void BobFilter::setSaturation(double saturation) {
    if (saturation <= 1e-3)
        saturation = 1e-3;
    x_params.p_saturation = saturation;
}

void BobFilter::setOversampling(double oversample) {
    if (oversample <= 1)
        oversample = 1;
    x_oversample = oversample;
}

void BobFilter::error() {
#ifdef CALCERROR
    double error = x_errorcount ? x_cumerror/x_errorcount : 0;
    std::cout << "Error: " << error << std::endl;
    x_cumerror = 0;
    x_errorcount = 0;
#else
    std::cout << "error estimate unavailable (not compiled in)\n";
#endif
}

void BobFilter::clear() {
   
    for (int i = 0; i < DIM; i++)
        x_state[i] = x_params.p_derivativeswere[i] = 0;
}

void BobFilter::print() {

    for (int i = 0; i < DIM; i++)
        std::cout << "state " << i << " , " << x_state[i] << std::endl;
    std::cout << "saturation " << x_params.p_saturation << std::endl;
    std::cout << "oversample " << x_oversample << std::endl;
}

} // namespace pd

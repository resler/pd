#include "com_pdplusplus_Oscillator.h"
#include "Oscillator.h"
#include <memory>

JNIEXPORT jlong JNICALL Java_com_pdplusplus_Oscillator_allocate0
(JNIEnv* env, jclass obj) {
	pd::Oscillator *osc = new pd::Oscillator();
	return (jlong)osc;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Oscillator_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::Oscillator *osc = (pd::Oscillator*) ptr;
	delete osc;
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_Oscillator_perform0
(JNIEnv* env, jobject obj, jdouble freq, jlong ptr) {
	pd::Oscillator* osc = (pd::Oscillator*) ptr;
	return (jdouble)osc->perform((double)freq);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Oscillator_setPhase0
(JNIEnv* env, jobject obj, jdouble phase, jlong ptr) {
	pd::Oscillator* osc = (pd::Oscillator*) ptr;
	osc->setPhase((double)phase);
}

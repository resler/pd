#include "com_pdplusplus_RealZero.h"
#include "RawFilters.h"


JNIEXPORT jlong JNICALL Java_com_pdplusplus_RealZero_allocate0
(JNIEnv* env, jclass obj) {
	pd::RealZero* rz = new pd::RealZero();
	return (jlong)rz;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_RealZero_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::RealZero* rz = (pd::RealZero*)ptr;
	delete rz;
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_RealZero_perform0
(JNIEnv* env, jclass obj, jdouble real, jdouble imag, jlong ptr) {
	pd::RealZero* rz = (pd::RealZero*)ptr;
	return (jdouble)rz->perform((double)real, (double)imag);

}


JNIEXPORT void JNICALL Java_com_pdplusplus_RealZero_set0
(JNIEnv* env, jclass obj, jdouble real, jdouble imag, jlong ptr) {
	pd::RealZero* rz = (pd::RealZero*)ptr;
	rz->set((double)real, (double)imag);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_RealZero_clear0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::RealZero* rz = (pd::RealZero*)ptr;
	rz->clear();
}

JNIEXPORT void JNICALL Java_com_pdplusplus_RealZero_setLast0
(JNIEnv* env, jclass obj, jdouble last, jlong ptr) {
	pd::RealZero* rz = (pd::RealZero*)ptr;
	rz->setLast(last);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_RealZero_setLastReal0
(JNIEnv* env, jclass obj, jdouble lr, jlong ptr) {
	pd::RealZero* rz = (pd::RealZero*)ptr;
	rz->setLastReal(lr);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_RealZero_setLastImaginary0
(JNIEnv* env, jclass obj, jdouble li, jlong ptr) {
	pd::RealZero* rz = (pd::RealZero*)ptr;
	rz->setLastImaginary(li);
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_RealZero_getLast0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::RealZero* rz = (pd::RealZero*)ptr;
	return (jdouble)rz->getLast();
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_RealZero_getLastReal0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::RealZero* rz = (pd::RealZero*)ptr;
	return (jdouble)rz->getLastReal();
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_RealZero_getLastImaginary0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::RealZero* rz = (pd::RealZero*)ptr;
	return (jdouble)rz->getLastImaginary();
}

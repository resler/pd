#pragma once
/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class com_pdplusplus_BiQuad */

#ifndef _Included_com_pdplusplus_BiQuad
#define _Included_com_pdplusplus_BiQuad
#ifdef __cplusplus
extern "C" {
#endif
	/*
	 * Class:     com_pdplusplus_BiQuad
	 * Method:    allocate0
	 * Signature: ()J
	 */
	JNIEXPORT jlong JNICALL Java_com_pdplusplus_BiQuad_allocate0
	(JNIEnv*, jclass);

	/*
	 * Class:     com_pdplusplus_BiQuad
	 * Method:    free0
	 * Signature: (J)V
	 */
	JNIEXPORT void JNICALL Java_com_pdplusplus_BiQuad_free0
	(JNIEnv*, jclass, jlong);

	/*
	 * Class:     com_pdplusplus_BiQuad
	 * Method:    perform0
	 * Signature: (DJ)D
	 */
	JNIEXPORT jdouble JNICALL Java_com_pdplusplus_BiQuad_perform0
	(JNIEnv*, jobject, jdouble, jlong);

	/*
	 * Class:     com_pdplusplus_BiQuad
	 * Method:    setCutoff0
	 * Signature: (DJ)V
	 */
	JNIEXPORT void JNICALL Java_com_pdplusplus_BiQuad_setCoefficients0
	(JNIEnv*, jobject, jdouble, jdouble, jdouble, jdouble, jdouble, jlong);

	/*
	 * Class:     com_pdplusplus_BiQuad
	 * Method:    clear0
	 * Signature: (J)V
	 */
	JNIEXPORT void JNICALL Java_com_pdplusplus_BiQuad_set0
	(JNIEnv*, jobject, jdouble, jdouble, jlong);

#ifdef __cplusplus
}
#endif
#endif

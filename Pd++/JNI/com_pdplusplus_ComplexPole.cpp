#include "com_pdplusplus_ComplexPole.h"
#include "RawFilters.h"


JNIEXPORT jlong JNICALL Java_com_pdplusplus_ComplexPole_allocate0
(JNIEnv* env, jclass obj) {
	pd::ComplexPole *cp = new pd::ComplexPole();
	return (jlong)cp;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_ComplexPole_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::ComplexPole* cp = (pd::ComplexPole*)ptr;
	delete cp;
}

JNIEXPORT jdoubleArray JNICALL Java_com_pdplusplus_ComplexPole_perform0
(JNIEnv* env, jclass obj, jdouble real, jdouble imag, jdouble realCoef, jdouble imagCoef, jlong ptr) {
	pd::ComplexPole* cp = (pd::ComplexPole*)ptr;
	 complexOutput out =  cp->perform((double)real, (double)imag, (double)realCoef, (double)imagCoef);

	 jdouble pair[] = { out.real, out.imaginary };

	 jdoubleArray output = env->NewDoubleArray(2);  // allocate
	 if (NULL == output) return NULL;
	 env->SetDoubleArrayRegion(output, 0, 2, pair);  // copy
	 return output;
}


JNIEXPORT void JNICALL Java_com_pdplusplus_ComplexPole_set0
(JNIEnv* env, jclass obj, jdouble real, jdouble imag, jlong ptr) {
	pd::ComplexPole* cp = (pd::ComplexPole*)ptr;
	cp->set((double)real, (double)imag);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_ComplexPole_clear0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::ComplexPole* cp = (pd::ComplexPole*)ptr;
	cp->clear();
}

JNIEXPORT void JNICALL Java_com_pdplusplus_ComplexPole_setLast0
(JNIEnv* env, jclass obj, jdouble last, jlong ptr) {
	pd::ComplexPole* cp = (pd::ComplexPole*)ptr;
	cp->setLast(last);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_ComplexPole_setLastReal0
(JNIEnv* env, jclass obj, jdouble lr, jlong ptr) {
	pd::ComplexPole* cp = (pd::ComplexPole*)ptr;
	cp->setLastReal(lr);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_ComplexPole_setLastImaginary0
(JNIEnv* env, jclass obj, jdouble li, jlong ptr) {
	pd::ComplexPole* cp = (pd::ComplexPole*)ptr;
	cp->setLastImaginary(li);
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_ComplexPole_getLast0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::ComplexPole* cp = (pd::ComplexPole*)ptr;
	return (jdouble)cp->getLast();
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_ComplexPole_getLastReal0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::ComplexPole* cp = (pd::ComplexPole*)ptr;
	return (jdouble)cp->getLastReal();
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_ComplexPole_getLastImaginary0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::ComplexPole* cp = (pd::ComplexPole*)ptr;
	return (jdouble)cp->getLastImaginary();
}

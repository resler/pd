#include "com_pdplusplus_SampleHold.h"
#include "SampleHold.h"

JNIEXPORT jlong JNICALL Java_com_pdplusplus_SampleHold_allocate0
(JNIEnv* env, jclass obj) {
	pd::SampleHold* samp = new pd::SampleHold();
	return (jlong)samp;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_SampleHold_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::SampleHold* samp = (pd::SampleHold*)ptr;
	delete samp;
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_SampleHold_perform0
(JNIEnv* env, jclass obj, jdouble input, jdouble control, jlong ptr) {
	pd::SampleHold* samp = (pd::SampleHold*)ptr;
	return (jdouble)samp->perform((double)input, (double)control);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_SampleHold_reset0
(JNIEnv* env, jclass obj, jdouble value, jlong ptr) {
	pd::SampleHold* samp = (pd::SampleHold*)ptr;
	samp->reset((double)value);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_SampleHold_set0
(JNIEnv* env, jclass obj, jdouble value, jlong ptr) {
	pd::SampleHold* samp = (pd::SampleHold*)ptr;
	samp->set((double)value);
}
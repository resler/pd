#include "com_pdplusplus_PdMaster.h"
#include "PdMaster.h"
#include "Dummy.h"
#include <iostream>

JNIEXPORT jlong JNICALL Java_com_pdplusplus_PdMaster_allocate0
(JNIEnv* env, jclass obj) {
	Dummy* dummy = new Dummy();
	return (jlong)dummy;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_PdMaster_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	Dummy* dummy = (Dummy*)ptr;
	delete dummy;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_PdMaster_setSampleRate0
(JNIEnv* env, jobject obj, jlong sr, jlong ptr) {
	Dummy* dummy = (Dummy*)ptr;
	dummy->setSampleRate((long)sr);

}

JNIEXPORT jlong JNICALL Java_com_pdplusplus_PdMaster_getSampleRate0
(JNIEnv* env, jobject obj, jlong ptr) {
	Dummy* dummy = (Dummy*)ptr;
	return (jlong)dummy->getSampleRate();
}

JNIEXPORT void JNICALL Java_com_pdplusplus_PdMaster_setBlockSize0
(JNIEnv* env, jobject obj, jint block, jlong ptr) {
	Dummy* dummy = (Dummy*)ptr;
	dummy->setBlockSize((int)block);
}

JNIEXPORT jint JNICALL Java_com_pdplusplus_PdMaster_getBlockSize0
(JNIEnv* env, jobject obj, jlong ptr) {
	Dummy* dummy = (Dummy*)ptr;
	return (jlong)dummy->getBlockSize();
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_PdMaster_getTimeInSampleTicks0
(JNIEnv* env, jobject obj, jlong ptr) {
	Dummy* dummy = (Dummy*)ptr;
	return(jdouble)dummy->getTimeInSampleTicks();
}

JNIEXPORT jlong JNICALL Java_com_pdplusplus_PdMaster_getTimeInMilliSeconds0
(JNIEnv* env, jobject obj, jdouble time, jlong ptr) {
	Dummy* dummy = (Dummy*)ptr;
	return (jlong)dummy->getTimeInMilliSeconds((double)time);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_PdMaster_setFFTWindow0
(JNIEnv* env, jobject obj, jint window, jlong ptr) {
	Dummy* dummy = (Dummy*)ptr;
	dummy->setFFTWindow((int)window);
}

JNIEXPORT jint JNICALL Java_com_pdplusplus_PdMaster_getFFTWindow0
(JNIEnv* env, jobject obj, jlong ptr) {
	Dummy* dummy = (Dummy*)ptr;
	return (jint)dummy->getFFTWindow();
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_PdMaster_mtof0
(JNIEnv* env, jobject obj, jdouble num, jlong ptr) {
	Dummy* dummy = (Dummy*)ptr;
	return (jdouble)dummy->mtof((double)num);
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_PdMaster_ftom0
(JNIEnv* env, jobject obj, jdouble freq, jlong ptr) {
	Dummy* dummy = (Dummy*)ptr;
	return (jdouble)dummy->ftom((double)freq);
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_PdMaster_powtodb0
(JNIEnv* env, jobject obj, jdouble num, jlong ptr) {
	Dummy* dummy = (Dummy*)ptr;
	return (jdouble)dummy->powtodb((double)num);
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_PdMaster_dbtopow0
(JNIEnv* env, jobject obj, jdouble num, jlong ptr) {
	Dummy* dummy = (Dummy*)ptr;
	return (jdouble)dummy->dbtopow((double)num);
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_PdMaster_rmstodb0
(JNIEnv* env, jobject obj, jdouble num, jlong ptr) {
	Dummy* dummy = (Dummy*)ptr;
	return (jdouble)dummy->rmstodb((double)num);
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_PdMaster_dbtorms0
(JNIEnv* env, jobject obj, jdouble num, jlong ptr) {
	Dummy* dummy = (Dummy*)ptr;
	return (jdouble)dummy->dbtorms((double)num);
}


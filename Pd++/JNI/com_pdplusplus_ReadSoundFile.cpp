
#include "com_pdplusplus_ReadSoundFile.h"
#include "ReadSoundFile.h"

JNIEXPORT jlong JNICALL Java_com_pdplusplus_ReadSoundFile_allocate0
(JNIEnv* env, jclass obj) {
	pd::ReadSoundFile *rsf = new pd::ReadSoundFile();
	return (jlong)rsf;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_ReadSoundFile_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::ReadSoundFile* rsf = (pd::ReadSoundFile*)ptr;
	delete rsf;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_ReadSoundFile_open0__Ljava_lang_String_2J
(JNIEnv* env, jclass obj, jstring file, jlong ptr) {
	pd::ReadSoundFile* rsf = (pd::ReadSoundFile*)ptr;

	const char* f = env->GetStringUTFChars(file, NULL);
	if (NULL == f) return ;

	std::string soundFile = f;

	rsf->open(soundFile);

	env->ReleaseStringUTFChars(file, f);
	
}

JNIEXPORT void JNICALL Java_com_pdplusplus_ReadSoundFile_open0__Ljava_lang_String_2DJ
(JNIEnv* env, jclass obj, jstring file, jdouble onset, jlong ptr) {
	pd::ReadSoundFile* rsf = (pd::ReadSoundFile*)ptr;

	const char* f = env->GetStringUTFChars(file, NULL);
	if (NULL == f) return;

	std::string soundFile = f;

	rsf->open(soundFile, onset);

	env->ReleaseStringUTFChars(file, f);
}

JNIEXPORT jdoubleArray JNICALL Java_com_pdplusplus_ReadSoundFile_start0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::ReadSoundFile* rsf = (pd::ReadSoundFile*)ptr;

	double* out = rsf->start();

	jdoubleArray output = env->NewDoubleArray(rsf->getBufferSize());  // allocate
	if (NULL == output) return NULL;
	env->SetDoubleArrayRegion(output, 0, rsf->getBufferSize(), (const jdouble*)out);  // copy

	return output;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_ReadSoundFile_stop0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::ReadSoundFile* rsf = (pd::ReadSoundFile*)ptr;
	rsf->stop();
}

JNIEXPORT void JNICALL Java_com_pdplusplus_ReadSoundFile_print0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::ReadSoundFile* rsf = (pd::ReadSoundFile*)ptr;
	rsf->print();
}

JNIEXPORT void JNICALL Java_com_pdplusplus_ReadSoundFile_setBufferSize0
(JNIEnv* env, jclass obj, jint buffer, jlong ptr) {
	pd::ReadSoundFile* rsf = (pd::ReadSoundFile*)ptr;
	rsf->setBufferSize((int)buffer);
}

JNIEXPORT jint JNICALL Java_com_pdplusplus_ReadSoundFile_getBufferSize0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::ReadSoundFile* rsf = (pd::ReadSoundFile*)ptr;
	return (int)rsf->getBufferSize();
}

JNIEXPORT jboolean JNICALL Java_com_pdplusplus_ReadSoundFile_isComplete0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::ReadSoundFile* rsf = (pd::ReadSoundFile*)ptr;
	return rsf->isComplete();
}
#include "com_pdplusplus_RealZeroReverse.h"
#include "RawFilters.h"


JNIEXPORT jlong JNICALL Java_com_pdplusplus_RealZeroReverse_allocate0
(JNIEnv* env, jclass obj) {
	pd::RealZeroReverse* rzr = new pd::RealZeroReverse();
	return (jlong)rzr;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_RealZeroReverse_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::RealZeroReverse* rzr = (pd::RealZeroReverse*)ptr;
	delete rzr;
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_RealZeroReverse_perform0
(JNIEnv* env, jclass obj, jdouble real, jdouble imag, jlong ptr) {
	pd::RealZeroReverse* rzr = (pd::RealZeroReverse*)ptr;
	return (jdouble)rzr->perform((double)real, (double)imag);

}


JNIEXPORT void JNICALL Java_com_pdplusplus_RealZeroReverse_set0
(JNIEnv* env, jclass obj, jdouble real, jdouble imag, jlong ptr) {
	pd::RealZeroReverse* rzr = (pd::RealZeroReverse*)ptr;
	rzr->set((double)real, (double)imag);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_RealZeroReverse_clear0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::RealZeroReverse* rzr = (pd::RealZeroReverse*)ptr;
	rzr->clear();
}

JNIEXPORT void JNICALL Java_com_pdplusplus_RealZeroReverse_setLast0
(JNIEnv* env, jclass obj, jdouble last, jlong ptr) {
	pd::RealZeroReverse* rzr = (pd::RealZeroReverse*)ptr;
	rzr->setLast(last);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_RealZeroReverse_setLastReal0
(JNIEnv* env, jclass obj, jdouble lr, jlong ptr) {
	pd::RealZeroReverse* rzr = (pd::RealZeroReverse*)ptr;
	rzr->setLastReal(lr);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_RealZeroReverse_setLastImaginary0
(JNIEnv* env, jclass obj, jdouble li, jlong ptr) {
	pd::RealZeroReverse* rzr = (pd::RealZeroReverse*)ptr;
	rzr->setLastImaginary(li);
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_RealZeroReverse_getLast0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::RealZeroReverse* rzr = (pd::RealZeroReverse*)ptr;
	return (jdouble)rzr->getLast();
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_RealZeroReverse_getLastReal0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::RealZeroReverse* rzr = (pd::RealZeroReverse*)ptr;
	return (jdouble)rzr->getLastReal();
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_RealZeroReverse_getLastImaginary0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::RealZeroReverse* rzr = (pd::RealZeroReverse*)ptr;
	return (jdouble)rzr->getLastImaginary();
}

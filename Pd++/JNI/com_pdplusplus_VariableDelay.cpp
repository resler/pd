#include "com_pdplusplus_VariableDelay.h"
#include "VariableDelay.h"

JNIEXPORT jlong JNICALL Java_com_pdplusplus_VariableDelay_allocate0
(JNIEnv* env, jclass obj) {
	pd::VariableDelay* vd = new pd::VariableDelay();
	return (jlong)vd;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_VariableDelay_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::VariableDelay* vd = (pd::VariableDelay*)ptr;
	delete vd;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_VariableDelay_delayWrite0
(JNIEnv* env, jclass obj, jdouble dt, jlong ptr) {
	pd::VariableDelay* vd = (pd::VariableDelay*)ptr;
	vd->delayWrite(dt);
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_VariableDelay_delayRead0
(JNIEnv* env, jclass obj, jdouble dt, jlong ptr) {
	pd::VariableDelay* vd = (pd::VariableDelay*)ptr;
	return (jdouble)vd->delayRead(dt);
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_VariableDelay_perform0
(JNIEnv* env, jclass obj, jdouble dt, jlong ptr) {
	pd::VariableDelay* vd = (pd::VariableDelay*)ptr;
	return (jdouble)vd->perform(dt);
}
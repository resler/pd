#include "com_pdplusplus_rIFFT.h"
#include "rIFFT.h"

JNIEXPORT jlong JNICALL Java_com_pdplusplus_rIFFT_allocate0__
(JNIEnv* env, jclass obj) {
	pd::realIFFT* rifft = new pd::realIFFT();
	
	return (jlong)rifft;
}

JNIEXPORT jlong JNICALL Java_com_pdplusplus_rIFFT_allocate0__I
(JNIEnv* env, jclass obj, jint ws) {
	pd::realIFFT* rifft = new pd::realIFFT(ws);

	return (jlong)rifft;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_rIFFT_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::realIFFT* rifft = (pd::realIFFT*)ptr;
	delete rifft;
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_rIFFT_perform0
(JNIEnv* env, jclass obj, jdoubleArray input, jlong ptr) {
	pd::realIFFT* rifft = (pd::realIFFT*)ptr;

	double* in = env->GetDoubleArrayElements(input, 0);
	double out = rifft->perform(in);
	env->ReleaseDoubleArrayElements(input, in, 0);

	
	return out;
}
#include "com_pdplusplus_cIFFT.h"
#include "cIFFT.h"
#include <iostream>

JNIEXPORT jlong JNICALL Java_com_pdplusplus_cIFFT_allocate0
(JNIEnv* env, jclass obj) {
	pd::complexIFFT* ifft = new pd::complexIFFT();
	return (jlong)ifft;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_cIFFT_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::complexIFFT* ifft = (pd::complexIFFT*)ptr;
	delete ifft;
}

JNIEXPORT jdoubleArray JNICALL Java_com_pdplusplus_cIFFT_perform0
(JNIEnv* env, jobject obj, jdoubleArray input, jlong ptr) {
	pd::complexIFFT* ifft = (pd::complexIFFT*)ptr;
	
	double* in = env->GetDoubleArrayElements(input, 0);

	double* out = ifft->perform(in);

	env->ReleaseDoubleArrayElements(input, in, 0);

	jdouble pair[] = { out[0], out[1] };

	jdoubleArray output = env->NewDoubleArray(2);  // allocate
	if (NULL == output) return NULL;
	env->SetDoubleArrayRegion(output, 0, 2, pair);  // copy
	return output;
}
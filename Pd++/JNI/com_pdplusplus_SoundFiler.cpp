#include "com_pdplusplus_SoundFiler.h"
#include "SoundFiler.h"
#include <vector>
#include <iostream>

JNIEXPORT jlong JNICALL Java_com_pdplusplus_SoundFiler_allocate0
(JNIEnv* env, jclass obj) {
	pd::SoundFiler* wav = new pd::SoundFiler();
	return (jlong)wav;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_SoundFiler_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::SoundFiler* wav = (pd::SoundFiler*)ptr;
	delete wav;
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_SoundFiler_read0
(JNIEnv* env, jclass obj, jstring file, jlong ptr) {
	pd::SoundFiler* wav = (pd::SoundFiler*)ptr;

	const char* f = env->GetStringUTFChars(file, NULL);
	if (NULL == f) return NULL;
	
	std::string soundFile = f;
    env->ReleaseStringUTFChars(file, f);
	return (jdouble)wav->read(soundFile);

	
}

JNIEXPORT void JNICALL Java_com_pdplusplus_SoundFiler_write0
(JNIEnv* env, jclass obj, jstring fileName, jint nChannels, jlong type, jlong formate, jdoubleArray array, jlong ptr) {
	pd::SoundFiler* wav = (pd::SoundFiler*)ptr;
	
	const char* f = env->GetStringUTFChars(fileName, NULL);
	if (NULL == f) return ;

	std::string soundFile = f;

	jdouble* data = env->GetDoubleArrayElements(array, 0);
	std::vector<double> buffer;
	for (int i = 0; i < env->GetArrayLength(array); i++)
	{
		buffer.push_back((double)data[i]);
	}
std::cout << "JNI buffer.size: " << buffer.size() << std::endl;
	wav->write(soundFile, nChannels, type, formate, buffer);

	env->ReleaseStringUTFChars(fileName, f);
	env->ReleaseDoubleArrayElements(array, data, 0);
}

JNIEXPORT jdoubleArray JNICALL Java_com_pdplusplus_SoundFiler_getArray0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::SoundFiler* wav = (pd::SoundFiler*)ptr;
	std::vector<double> array = wav->getArray();

	double* out = new double[array.size()];

	for (int i = 0; i < array.size(); i++)
	{
		out[i] = array[i];
	}

	jdoubleArray output = env->NewDoubleArray((long)array.size());  // allocate
	if (NULL == output) return NULL;
	env->SetDoubleArrayRegion(output, 0, (long)array.size(), (const jdouble*)out);  // copy
	delete [] out;
	return output;
}

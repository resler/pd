#include "com_pdplusplus_ComplexZeroReverse.h"
#include "RawFilters.h"


JNIEXPORT jlong JNICALL Java_com_pdplusplus_ComplexZeroReverse_allocate0
(JNIEnv* env, jclass obj) {
	pd::ComplexZeroReverse* czr = new pd::ComplexZeroReverse();
	return (jlong)czr;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_ComplexZeroReverse_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::ComplexZeroReverse* czr = (pd::ComplexZeroReverse*)ptr;
	delete czr;
}

JNIEXPORT jdoubleArray JNICALL Java_com_pdplusplus_ComplexZeroReverse_perform0
(JNIEnv* env, jclass obj, jdouble real, jdouble imag, jdouble realCoef, jdouble imagCoef, jlong ptr) {
	pd::ComplexZeroReverse* czr = (pd::ComplexZeroReverse*)ptr;
	complexOutput out = czr->perform((double)real, (double)imag, (double)realCoef, (double)imagCoef);

	jdouble pair[] = { out.real, out.imaginary };

	jdoubleArray output = env->NewDoubleArray(2);  // allocate
	if (NULL == output) return NULL;
	env->SetDoubleArrayRegion(output, 0, 2, pair);  // copy
	return output;
}


JNIEXPORT void JNICALL Java_com_pdplusplus_ComplexZeroReverse_set0
(JNIEnv* env, jclass obj, jdouble real, jdouble imag, jlong ptr) {
	pd::ComplexZeroReverse* czr = (pd::ComplexZeroReverse*)ptr;
	czr->set((double)real, (double)imag);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_ComplexZeroReverse_clear0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::ComplexZeroReverse* czr = (pd::ComplexZeroReverse*)ptr;
	czr->clear();
}

JNIEXPORT void JNICALL Java_com_pdplusplus_ComplexZeroReverse_setLast0
(JNIEnv* env, jclass obj, jdouble last, jlong ptr) {
	pd::ComplexZeroReverse* czr = (pd::ComplexZeroReverse*)ptr;
	czr->setLast(last);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_ComplexZeroReverse_setLastReal0
(JNIEnv* env, jclass obj, jdouble lr, jlong ptr) {
	pd::ComplexZeroReverse* czr = (pd::ComplexZeroReverse*)ptr;
	czr->setLastReal(lr);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_ComplexZeroReverse_setLastImaginary0
(JNIEnv* env, jclass obj, jdouble li, jlong ptr) {
	pd::ComplexZeroReverse* czr = (pd::ComplexZeroReverse*)ptr;
	czr->setLastImaginary(li);
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_ComplexZeroReverse_getLast0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::ComplexZeroReverse* czr = (pd::ComplexZeroReverse*)ptr;
	return (jdouble)czr->getLast();
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_ComplexZeroReverse_getLastReal0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::ComplexZeroReverse* czr = (pd::ComplexZeroReverse*)ptr;
	return (jdouble)czr->getLastReal();
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_ComplexZeroReverse_getLastImaginary0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::ComplexZeroReverse* czr = (pd::ComplexZeroReverse*)ptr;
	return (jdouble)czr->getLastImaginary();
}

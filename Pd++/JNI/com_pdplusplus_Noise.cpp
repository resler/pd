#include "com_pdplusplus_Noise.h"
#include "Noise.h"

JNIEXPORT jlong JNICALL Java_com_pdplusplus_Noise_allocate0
(JNIEnv* env, jclass obj) {
	pd::Noise *noise = new pd::Noise();
	return (jlong)noise;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Noise_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::Noise *noise = (pd::Noise*)ptr;
	delete noise;
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_Noise_perform0
(JNIEnv* env, jobject obj, jlong ptr) {
	pd::Noise *noise = (pd::Noise*)ptr;
	return (jdouble)noise->perform();
}
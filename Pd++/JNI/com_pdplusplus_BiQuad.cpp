#include "com_pdplusplus_BiQuad.h"
#include "BiQuad.h"

JNIEXPORT jlong JNICALL Java_com_pdplusplus_BiQuad_allocate0
(JNIEnv* env, jclass obj) {
	pd::BiQuad* bq = new pd::BiQuad();
	return (jlong)bq;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_BiQuad_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::BiQuad* bq = (pd::BiQuad*)ptr;
	delete bq;
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_BiQuad_perform0
(JNIEnv* env, jobject obj, jdouble input, jlong ptr) {
	pd::BiQuad* bq = (pd::BiQuad*)ptr;
	return (jdouble)bq->perform((double)input);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_BiQuad_setCoefficients0
(JNIEnv* env, jobject obj, jdouble fb1, jdouble fb2, jdouble ff1, jdouble ff2, jdouble ff3, jlong ptr) {
	pd::BiQuad* bq = (pd::BiQuad*)ptr;
	bq->setCoefficients((double)fb1, (double)fb2, (double)ff1, (double)ff2, (double)ff3);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_BiQuad_set0
(JNIEnv* env, jobject obj, jdouble a, jdouble b, jlong ptr) {
	pd::BiQuad* bq = (pd::BiQuad*)ptr;
	bq->set((double)a, (double)b);
}
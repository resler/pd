#include "com_pdplusplus_Cosine.h"
#include "Cosine.h"

JNIEXPORT jlong JNICALL Java_com_pdplusplus_Cosine_allocate0
(JNIEnv* env, jclass obj) {
	pd::Cosine *cos = new pd::Cosine();
	return (jlong)cos;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Cosine_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::Cosine* cos = (pd::Cosine*) ptr;
	delete cos;
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_Cosine_perform0
(JNIEnv* env, jobject obj, jdouble freq, jlong ptr) {
	pd::Cosine* cos = (pd::Cosine*) ptr;
	return (jdouble)cos->perform((double)freq);
}

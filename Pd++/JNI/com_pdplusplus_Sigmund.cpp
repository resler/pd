#include "com_pdplusplus_Sigmund.h"
#include "Sigmund.h"

JNIEXPORT jlong JNICALL Java_com_pdplusplus_Sigmund_allocate0__
(JNIEnv* env, jclass obj) {
	pd::Sigmund *sig = new pd::Sigmund();
	return (jlong)sig;
}

JNIEXPORT jlong JNICALL Java_com_pdplusplus_Sigmund_allocate0__Ljava_lang_String_2Ljava_lang_String_2
(JNIEnv* env, jclass obj, jstring pitch, jstring envelope) {
	const char* p = env->GetStringUTFChars(pitch, NULL);
	if (NULL == p) return NULL;

	const char* e = env->GetStringUTFChars(envelope, NULL);
	if (NULL == e) return NULL;

	std::string pitches = p;
	std::string envelopes = e;
	
	pd::Sigmund* sig = new pd::Sigmund(pitches, envelopes);

	env->ReleaseStringUTFChars(pitch, p);  // release resources
	env->ReleaseStringUTFChars(envelope, e);

	return (jlong)sig;
}

JNIEXPORT jlong JNICALL Java_com_pdplusplus_Sigmund_allocate0__Ljava_lang_String_2I
(JNIEnv* env, jclass obj, jstring tr, jint num) {
	const char* t = env->GetStringUTFChars(tr, NULL);
	if (NULL == t) return NULL;

	std::string tracks = t;

	pd::Sigmund* sig = new pd::Sigmund(tracks, num);

	env->ReleaseStringUTFChars(tr, t);  // release resources

	return (jlong)sig;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Sigmund_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::Sigmund* sig = (pd::Sigmund*)ptr;
	delete sig;
}

JNIEXPORT jobject JNICALL Java_com_pdplusplus_Sigmund_perform0
(JNIEnv* env, jclass obj, jdouble input, jlong ptr) {
	pd::Sigmund* sig = (pd::Sigmund*)ptr;

	/*create a new object of sigmundPackage*/
	jclass cls = env->FindClass("com/pdplusplus/SigmundPackage");
	jmethodID methodInit = env->GetMethodID(cls, "<init>", "(DDD[[D[[DII)V");
	if (NULL == methodInit) return NULL;

	//run sigmund perform function
	sigmundPackage sp = sig->perform((double)input);
	jobject sigPack = NULL;

	/******************** PEAKS ***************************/
	if (sig->getPeakBool())
	{
		//PEAKS: Get the length for the first and second dimensions
		unsigned int peaks = sig->getNumOfPeaks();
		unsigned int peakColumns = sig->getPeakColumnNumber();

		// Get the 2D float array we want to "Cast"
		double** sigmundPeaks = sp.peaks;

		// Get double array class
		jclass doubleArrayClass = env->FindClass("[D");

		// Check array
		if (doubleArrayClass == NULL)	return NULL;

		// Create the returnable 2D array
		jobjectArray jSigmundPeaks = env->NewObjectArray((jsize)peaks, doubleArrayClass, NULL);

		// PEAKS: Go through the first dimension and add the second dimension arrays
		for (unsigned int i = 0; i < peaks; i++)
		{
			jdoubleArray doubleArray = env->NewDoubleArray(peakColumns);
			env->SetDoubleArrayRegion(doubleArray, (jsize)0, (jsize)peakColumns, (jdouble*)sigmundPeaks[i]);
			env->SetObjectArrayElement(jSigmundPeaks, (jsize)i, doubleArray);
			env->DeleteLocalRef(doubleArray);
		}

		/****************** FINISH *****************/
	//put everything into a SigumndPackage object and send it back to JAVA
		 sigPack = env->NewObject(cls, methodInit, (jdouble)sp.pitch, (jdouble)sp.notes, (jdouble)sp.envelope,
			jSigmundPeaks, NULL, (jint)sp.peakSize, (jint)sp.trackSize);
	}
	/******************** TRACKS: ********************/
	else if (sig->getTrackBool())
	{
		unsigned int tracks = sig->getNumOfPeaks();
		unsigned int trackColumns = sig->getTrackColumnNumber();

		// Get the 2D float array
		double** sigmundTracks = sp.tracks;

		// Get double array class
		jclass doubleArrayClassTracks = env->FindClass("[D");

		// Check array
		if (doubleArrayClassTracks == NULL)	return NULL;

		// Create the returnable 2D array
		jobjectArray jSigmundTracks = env->NewObjectArray((jsize)tracks, doubleArrayClassTracks, NULL);

		// TRACKS: Go through the first dimension and add the second dimension arrays
		for (unsigned int i = 0; i < tracks; i++)
		{
			jdoubleArray doubleArrayTracks = env->NewDoubleArray(trackColumns);
			env->SetDoubleArrayRegion(doubleArrayTracks, (jsize)0, (jsize)trackColumns, (jdouble*)sigmundTracks[i]);
			env->SetObjectArrayElement(jSigmundTracks, (jsize)i, doubleArrayTracks);
			env->DeleteLocalRef(doubleArrayTracks);
		}

		/****************** FINISH *****************/
	//put everything into a SigumndPackage object and send it back to JAVA
		 sigPack = env->NewObject(cls, methodInit, (jdouble)sp.pitch, (jdouble)sp.notes, (jdouble)sp.envelope,
			NULL, jSigmundTracks, (jint)sp.peakSize, (jint)sp.trackSize);
	}
	else
	{
		/****************** FINISH *****************/
	//put everything into a SigumndPackage object and send it back to JAVA
		sigPack = env->NewObject(cls, methodInit, (jdouble)sp.pitch, (jdouble)sp.notes, (jdouble)sp.envelope,
			NULL, NULL, (jint)sp.peakSize, (jint)sp.trackSize);
	}
	
	return sigPack;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Sigmund_setMode0
(JNIEnv* env, jclass obj, jint mode, jlong ptr) {
	pd::Sigmund* sig = (pd::Sigmund*)ptr;
	sig->setMode(mode);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Sigmund_setNumOfPoints0
(JNIEnv* env, jclass obj, jdouble num, jlong ptr) {
	pd::Sigmund* sig = (pd::Sigmund*)ptr;
	sig->setNumOfPoints(num);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Sigmund_setHop0
(JNIEnv* env, jclass obj, jdouble hop, jlong ptr) {
	pd::Sigmund* sig = (pd::Sigmund*)ptr;
	sig->setHop(hop);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Sigmund_setNumOfPeaks0
(JNIEnv* env, jclass obj, jdouble num, jlong ptr) {
	pd::Sigmund* sig = (pd::Sigmund*)ptr;
	sig->setNumOfPeaks(num);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Sigmund_setMaxFrequency0
(JNIEnv* env, jclass obj, jdouble max, jlong ptr) {
	pd::Sigmund* sig = (pd::Sigmund*)ptr;
	sig->setMaxFrequency(max);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Sigmund_setVibrato0
(JNIEnv* env, jclass obj, jdouble vib, jlong ptr) {
	pd::Sigmund* sig = (pd::Sigmund*)ptr;
	sig->setVibrato(vib);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Sigmund_setStableTime0
(JNIEnv* env, jclass obj, jdouble st, jlong ptr) {
	pd::Sigmund* sig = (pd::Sigmund*)ptr;
	sig->setStableTime(st);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Sigmund_setMinPower0
(JNIEnv* env, jclass obj, jdouble pow, jlong ptr) {
	pd::Sigmund* sig = (pd::Sigmund*)ptr;
	sig->setMinPower(pow);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Sigmund_setGrowth0
(JNIEnv* env, jclass obj, jdouble g, jlong ptr) {
	pd::Sigmund* sig = (pd::Sigmund*)ptr;
	sig->setGrowth(g);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Sigmund_print0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::Sigmund* sig = (pd::Sigmund*)ptr;
	sig->print();
}

JNIEXPORT jobject JNICALL Java_com_pdplusplus_Sigmund_list0
(JNIEnv* env, jclass obj, jdoubleArray array, jint numOfPoints, jint index, jlong sr, jint debug, jlong ptr) {
	pd::Sigmund* sig = (pd::Sigmund*)ptr;
	/*create a new object of sigmundPackage*/
	jclass cls = env->FindClass("com/pdplusplus/SigmundPackage");
	jmethodID methodInit = env->GetMethodID(cls, "<init>", "(DDD[[D[[DII)V");
	if (NULL == methodInit) return NULL;
	//get the array elements
	double* arr = env->GetDoubleArrayElements(array, 0);
	//run sigmund list function
	sigmundPackage sp = sig->list(arr, numOfPoints, index, sr, debug);
	//copy to new object
	jobject sigPack = env->NewObject(cls, methodInit, sp.pitch, sp.notes, sp.envelope, sp.peaks, sp.tracks, sp.peakSize, sp.trackSize);
	env->ReleaseDoubleArrayElements(array, arr, 0);
	return sigPack;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Sigmund_clear0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::Sigmund* sig = (pd::Sigmund*)ptr;
	sig->clear();
}

JNIEXPORT jint JNICALL Java_com_pdplusplus_Sigmund_getMode0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::Sigmund* sig = (pd::Sigmund*)ptr;
	return (jint)sig->getMode();
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_Sigmund_getNumOfPoints0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::Sigmund* sig = (pd::Sigmund*)ptr;
	return (jdouble)sig->getNumOfPoints();
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_Sigmund_getHop0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::Sigmund* sig = (pd::Sigmund*)ptr;
	return (jdouble)sig->getHop();
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_Sigmund_getNumOfPeaks0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::Sigmund* sig = (pd::Sigmund*)ptr;
	return (jdouble)sig->getNumOfPeaks();
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_Sigmund_getMaxFrequency0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::Sigmund* sig = (pd::Sigmund*)ptr;
	return (jdouble)sig->getMaxFrequency();
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_Sigmund_getVibrato0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::Sigmund* sig = (pd::Sigmund*)ptr;
	return (jdouble)sig->getVibrato();

}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_Sigmund_getStableTime0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::Sigmund* sig = (pd::Sigmund*)ptr;
	return (jdouble)sig->getStableTime();
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_Sigmund_getMinPower0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::Sigmund* sig = (pd::Sigmund*)ptr;
	return (jdouble)sig->getMinPower();
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_Sigmund_getGrowth0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::Sigmund* sig = (pd::Sigmund*)ptr;
	return (jdouble)sig->getGrowth();
}
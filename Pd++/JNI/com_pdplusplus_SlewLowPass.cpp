#include "com_pdplusplus_SlewLowPass.h"
#include "SlewLowPass.h"

JNIEXPORT jlong JNICALL Java_com_pdplusplus_SlewLowPass_allocate0
(JNIEnv* env, jclass obj) {
	pd::SlewLowPass* slop = new pd::SlewLowPass();
	return (jlong)slop;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_SlewLowPass_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::SlewLowPass* slop = (pd::SlewLowPass*)ptr;
	delete slop;
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_SlewLowPass_perform0
(JNIEnv* env, jclass obj, jdouble input, jdouble freq, jdouble posLimitIn, jdouble posFreqIn, jdouble negLimitIn, jdouble negFreqIn, jlong ptr) {
	pd::SlewLowPass* slop = (pd::SlewLowPass*)ptr;
	return (jdouble)slop->perform(input, freq, posLimitIn, posFreqIn, negLimitIn, negFreqIn);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_SlewLowPass_set0
(JNIEnv* env, jclass obj, jdouble last, jlong ptr) {
	pd::SlewLowPass* slop = (pd::SlewLowPass*)ptr;
	slop->set(last);
}
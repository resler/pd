#include "com_pdplusplus_rFFT.h"
#include "rFFT.h"


JNIEXPORT jlong JNICALL Java_com_pdplusplus_rFFT_allocate0__
(JNIEnv* env, jclass obj) {
	pd::realFFT* rfft = new pd::realFFT();
	return (jlong)rfft;
}

JNIEXPORT jlong JNICALL Java_com_pdplusplus_rFFT_allocate0__I
(JNIEnv* env, jclass obj, jint ws) {
	pd::realFFT* rfft = new pd::realFFT(ws);
	return (jlong)rfft;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_rFFT_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::realFFT* rfft = (pd::realFFT*)ptr;
	delete rfft;
}

JNIEXPORT jdoubleArray JNICALL Java_com_pdplusplus_rFFT_perform0
(JNIEnv* env, jclass obj, jdouble input, jlong ptr) {
	pd::realFFT* rfft = (pd::realFFT*)ptr;

	double* out =rfft->perform((double)input);
	jdoubleArray output = env->NewDoubleArray((long)rfft->getFFTWindow());  // allocate
	if (NULL == output) return NULL;
	env->SetDoubleArrayRegion(output, 0, (long)rfft->getFFTWindow(), (const jdouble*)out);  // copy

	return output;
}
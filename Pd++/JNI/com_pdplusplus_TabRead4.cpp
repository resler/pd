#include "com_pdplusplus_TabRead4.h"
#include "TabRead4.h"

JNIEXPORT jlong JNICALL Java_com_pdplusplus_TabRead4_allocate0
(JNIEnv* env, jclass obj) {
	pd::TabRead4* tab = new pd::TabRead4();
	return (jlong)tab;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_TabRead4_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::TabRead4* tab = (pd::TabRead4*)ptr;
	delete tab;
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_TabRead4_perform0
(JNIEnv* env, jclass obj, jdouble index, jlong ptr) {
	pd::TabRead4* tab = (pd::TabRead4*)ptr;
	return (jdouble)tab->perform(index);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_TabRead4_setTable0
(JNIEnv* env, jclass obj, jdoubleArray table, jlong ptr) {
	pd::TabRead4* tab = (pd::TabRead4*)ptr;
	
	double* t = env->GetDoubleArrayElements(table, 0);
	std::vector<double> tabArray;
	//copy to vector
	for (int i = 0; i < env->GetArrayLength(table); i++)
	{
		tabArray.push_back(t[i]);
	}

	tab->setTable(tabArray);
	env->ReleaseDoubleArrayElements(table, t, 0);

}

JNIEXPORT jlong JNICALL Java_com_pdplusplus_TabRead4_getTableSize0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::TabRead4* tab = (pd::TabRead4*)ptr;
	return (jlong)tab->getTableSize();
}

JNIEXPORT void JNICALL Java_com_pdplusplus_TabRead4_setOnset0
(JNIEnv* env, jclass obj, jdouble onset, jlong ptr) {
	pd::TabRead4* tab = (pd::TabRead4*)ptr;
	tab->setOnset(onset);
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_TabRead4_getOnset0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::TabRead4* tab = (pd::TabRead4*)ptr;
	return tab->getOnset();
}
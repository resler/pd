#include "com_pdplusplus_Phasor.h"
#include "Phasor.h"

JNIEXPORT jlong JNICALL Java_com_pdplusplus_Phasor_allocate0
(JNIEnv* env, jclass obj) {
	pd::Phasor* phasor = new pd::Phasor();
	return (jlong)phasor;

}
 
JNIEXPORT void JNICALL Java_com_pdplusplus_Phasor_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::Phasor *phasor = (pd::Phasor*) ptr;
	delete phasor;
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_Phasor_perform0
(JNIEnv* env, jobject obj, jdouble freq, jlong ptr) {
	pd::Phasor* phasor = (pd::Phasor*) ptr;
	return (jdouble)phasor->perform((double)freq);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Phasor_setPhase0
(JNIEnv* env, jobject obj, jdouble phase, jlong ptr) {
	pd::Phasor* phasor = (pd::Phasor*) ptr;
	phasor->setPhase((double)phase);
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_Phasor_getPhase0
(JNIEnv* env, jobject obj, jlong ptr) {
	pd::Phasor* phasor = (pd::Phasor*) ptr;
	return (jdouble)phasor->getPhase();
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Phasor_setFrequency0
(JNIEnv* env, jobject obj, jdouble freq, jlong ptr) {
	//This does the same thing as perform(freq)
	pd::Phasor* phasor = (pd::Phasor*) ptr;
	phasor->setFrequency((double)freq);
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_Phasor_getFrequency0
(JNIEnv* env, jobject obj, jlong ptr) {
	pd::Phasor* phasor = (pd::Phasor*) ptr;
	return (jdouble)phasor->getFrequency();
}
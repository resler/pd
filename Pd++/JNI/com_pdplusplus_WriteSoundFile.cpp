#include "com_pdplusplus_WriteSoundFile.h"
#include "WriteSoundFile.h"

JNIEXPORT jlong JNICALL Java_com_pdplusplus_WriteSoundFile_allocate0
(JNIEnv* env, jclass obj) {
	pd::WriteSoundFile* wsf = new pd::WriteSoundFile();
	return (jlong)wsf;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_WriteSoundFile_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::WriteSoundFile* wsf = (pd::WriteSoundFile*)ptr;
	delete wsf;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_WriteSoundFile_open0__Ljava_lang_String_2IJ
(JNIEnv* env, jclass obj, jstring file, jint ch, jlong ptr) {
	pd::WriteSoundFile* wsf = (pd::WriteSoundFile*)ptr;
	const char* f = env->GetStringUTFChars(file, NULL);
	if (NULL == f) return;

	std::string soundFile = f;

	wsf->open(soundFile, ch);

	env->ReleaseStringUTFChars(file, f);
	
}

JNIEXPORT void JNICALL Java_com_pdplusplus_WriteSoundFile_open0__Ljava_lang_String_2IJJ
(JNIEnv* env, jclass obj, jstring file, jint ch, jlong type, jlong format, jlong ptr) {
	pd::WriteSoundFile* wsf = (pd::WriteSoundFile*)ptr;
	const char* f = env->GetStringUTFChars(file, NULL);
	if (NULL == f) return;

	std::string soundFile = f;

	wsf->open(soundFile, ch, type, format);

	env->ReleaseStringUTFChars(file, f);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_WriteSoundFile_start0
(JNIEnv* env, jclass obj, jdoubleArray input, jlong ptr) {
	pd::WriteSoundFile* wsf = (pd::WriteSoundFile*)ptr;
	double* in = env->GetDoubleArrayElements(input, 0);
	wsf->start(in);
	env->ReleaseDoubleArrayElements(input, in, 0);
	
}

JNIEXPORT void JNICALL Java_com_pdplusplus_WriteSoundFile_stop0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::WriteSoundFile* wsf = (pd::WriteSoundFile*)ptr;
	wsf->stop();
}

JNIEXPORT void JNICALL Java_com_pdplusplus_WriteSoundFile_print0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::WriteSoundFile* wsf = (pd::WriteSoundFile*)ptr;
	wsf->print();
}
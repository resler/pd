
#include "com_pdplusplus_cFFT.h"
#include "cFFT.h"

JNIEXPORT jlong JNICALL Java_com_pdplusplus_cFFT_allocate0
(JNIEnv* env, jclass obj) {
	pd::complexFFT* fft = new pd::complexFFT();
	return (jlong)fft;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_cFFT_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::complexFFT* fft = (pd::complexFFT*)ptr;
	delete fft;
}

JNIEXPORT jdoubleArray JNICALL Java_com_pdplusplus_cFFT_perform0
(JNIEnv* env, jobject obj, jdouble real, jdouble imag, jlong ptr) {

	pd::complexFFT* fft = (pd::complexFFT*)ptr;
	double* out = fft->perform((double)real, (double)imag);

	jdoubleArray output = env->NewDoubleArray((long)fft->getFFTWindow()*2);  // allocate
	if (NULL == output) return NULL;
	env->SetDoubleArrayRegion(output, 0, (long)fft->getFFTWindow()*2, (const jdouble *)out);  // copy
	
	return output;
}
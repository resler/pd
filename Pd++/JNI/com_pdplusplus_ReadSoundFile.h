/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class com_pdplusplus_ReadSoundFile */

#ifndef _Included_com_pdplusplus_ReadSoundFile
#define _Included_com_pdplusplus_ReadSoundFile
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     com_pdplusplus_ReadSoundFile
 * Method:    allocate0
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_com_pdplusplus_ReadSoundFile_allocate0
  (JNIEnv *, jclass);

/*
 * Class:     com_pdplusplus_ReadSoundFile
 * Method:    free0
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_com_pdplusplus_ReadSoundFile_free0
  (JNIEnv *, jclass, jlong);

/*
 * Class:     com_pdplusplus_ReadSoundFile
 * Method:    open0
 * Signature: (Ljava/lang/String;J)V
 */
JNIEXPORT void JNICALL Java_com_pdplusplus_ReadSoundFile_open0__Ljava_lang_String_2J
  (JNIEnv *, jclass, jstring, jlong);

/*
 * Class:     com_pdplusplus_ReadSoundFile
 * Method:    open0
 * Signature: (Ljava/lang/String;DJ)V
 */
JNIEXPORT void JNICALL Java_com_pdplusplus_ReadSoundFile_open0__Ljava_lang_String_2DJ
  (JNIEnv *, jclass, jstring, jdouble, jlong);

/*
 * Class:     com_pdplusplus_ReadSoundFile
 * Method:    start0
 * Signature: (J)[D
 */
JNIEXPORT jdoubleArray JNICALL Java_com_pdplusplus_ReadSoundFile_start0
  (JNIEnv *, jclass, jlong);

/*
 * Class:     com_pdplusplus_ReadSoundFile
 * Method:    stop0
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_com_pdplusplus_ReadSoundFile_stop0
  (JNIEnv *, jclass, jlong);

/*
 * Class:     com_pdplusplus_ReadSoundFile
 * Method:    print0
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_com_pdplusplus_ReadSoundFile_print0
  (JNIEnv *, jclass, jlong);

/*
 * Class:     com_pdplusplus_ReadSoundFile
 * Method:    setBufferSize0
 * Signature: (IJ)V
 */
JNIEXPORT void JNICALL Java_com_pdplusplus_ReadSoundFile_setBufferSize0
  (JNIEnv *, jclass, jint, jlong);

/*
 * Class:     com_pdplusplus_ReadSoundFile
 * Method:    getBufferSize0
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL Java_com_pdplusplus_ReadSoundFile_getBufferSize0
  (JNIEnv *, jclass, jlong);

/*
 * Class:     com_pdplusplus_ReadSoundFile
 * Method:    isComplete0
 * Signature: (J)Z
 */
JNIEXPORT jboolean JNICALL Java_com_pdplusplus_ReadSoundFile_isComplete0
  (JNIEnv *, jclass, jlong);

#ifdef __cplusplus
}
#endif
#endif

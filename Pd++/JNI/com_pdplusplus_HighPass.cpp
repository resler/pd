#include "com_pdplusplus_HighPass.h"
#include "HighPass.h"

JNIEXPORT jlong JNICALL Java_com_pdplusplus_HighPass_allocate0
(JNIEnv* env, jclass obj) {
	pd::HighPass *hip = new pd::HighPass();
	return (jlong)hip;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_HighPass_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::HighPass* hip = (pd::HighPass*)ptr;
	delete hip;
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_HighPass_perform0
(JNIEnv* env, jobject obj, jdouble input, jlong ptr) {
	pd::HighPass* hip = (pd::HighPass*)ptr;
	return (jdouble)hip->perform((double)input);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_HighPass_setCutoff0
(JNIEnv* env, jobject obj, jdouble freq, jlong ptr) {
	pd::HighPass* hip = (pd::HighPass*)ptr;
	hip->setCutOff((double)freq);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_HighPass_clear0
(JNIEnv* env, jobject obj, jlong ptr) {
	pd::HighPass* hip = (pd::HighPass*)ptr;
	hip->clear(0);
}
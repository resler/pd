/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class com_pdplusplus_PdMaster */

#ifndef _Included_com_pdplusplus_PdMaster
#define _Included_com_pdplusplus_PdMaster
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     com_pdplusplus_PdMaster
 * Method:    setSampleRate0
 * Signature: (JJ)V
 */
JNIEXPORT void JNICALL Java_com_pdplusplus_PdMaster_setSampleRate0
  (JNIEnv *, jobject, jlong, jlong);

/*
 * Class:     com_pdplusplus_PdMaster
 * Method:    getSampleRate0
 * Signature: (J)J
 */
JNIEXPORT jlong JNICALL Java_com_pdplusplus_PdMaster_getSampleRate0
  (JNIEnv *, jobject, jlong);

/*
 * Class:     com_pdplusplus_PdMaster
 * Method:    setBlockSize0
 * Signature: (IJ)V
 */
JNIEXPORT void JNICALL Java_com_pdplusplus_PdMaster_setBlockSize0
  (JNIEnv *, jobject, jint, jlong);

/*
 * Class:     com_pdplusplus_PdMaster
 * Method:    getBlockSize0
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL Java_com_pdplusplus_PdMaster_getBlockSize0
  (JNIEnv *, jobject, jlong);

/*
 * Class:     com_pdplusplus_PdMaster
 * Method:    getTimeInSampleTicks0
 * Signature: (J)D
 */
JNIEXPORT jdouble JNICALL Java_com_pdplusplus_PdMaster_getTimeInSampleTicks0
  (JNIEnv *, jobject, jlong);

/*
 * Class:     com_pdplusplus_PdMaster
 * Method:    getTimeInMilliSeconds0
 * Signature: (DJ)J
 */
JNIEXPORT jlong JNICALL Java_com_pdplusplus_PdMaster_getTimeInMilliSeconds0
  (JNIEnv *, jobject, jdouble, jlong);

/*
 * Class:     com_pdplusplus_PdMaster
 * Method:    setFFTWindow0
 * Signature: (IJ)V
 */
JNIEXPORT void JNICALL Java_com_pdplusplus_PdMaster_setFFTWindow0
  (JNIEnv *, jobject, jint, jlong);

/*
 * Class:     com_pdplusplus_PdMaster
 * Method:    getFFTWindow0
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL Java_com_pdplusplus_PdMaster_getFFTWindow0
  (JNIEnv *, jobject, jlong);

/*
 * Class:     com_pdplusplus_PdMaster
 * Method:    mtof0
 * Signature: (DJ)D
 */
JNIEXPORT jdouble JNICALL Java_com_pdplusplus_PdMaster_mtof0
  (JNIEnv *, jobject, jdouble, jlong);

/*
 * Class:     com_pdplusplus_PdMaster
 * Method:    ftom0
 * Signature: (DJ)D
 */
JNIEXPORT jdouble JNICALL Java_com_pdplusplus_PdMaster_ftom0
  (JNIEnv *, jobject, jdouble, jlong);

/*
 * Class:     com_pdplusplus_PdMaster
 * Method:    powtodb0
 * Signature: (DJ)D
 */
JNIEXPORT jdouble JNICALL Java_com_pdplusplus_PdMaster_powtodb0
  (JNIEnv *, jobject, jdouble, jlong);

/*
 * Class:     com_pdplusplus_PdMaster
 * Method:    dbtopow0
 * Signature: (DJ)D
 */
JNIEXPORT jdouble JNICALL Java_com_pdplusplus_PdMaster_dbtopow0
  (JNIEnv *, jobject, jdouble, jlong);

/*
 * Class:     com_pdplusplus_PdMaster
 * Method:    rmstodb0
 * Signature: (DJ)D
 */
JNIEXPORT jdouble JNICALL Java_com_pdplusplus_PdMaster_rmstodb0
  (JNIEnv *, jobject, jdouble, jlong);

/*
 * Class:     com_pdplusplus_PdMaster
 * Method:    dbtorms0
 * Signature: (DJ)D
 */
JNIEXPORT jdouble JNICALL Java_com_pdplusplus_PdMaster_dbtorms0
  (JNIEnv *, jobject, jdouble, jlong);

/*
 * Class:     com_pdplusplus_PdMaster
 * Method:    allocate0
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_com_pdplusplus_PdMaster_allocate0
  (JNIEnv *, jclass);

/*
 * Class:     com_pdplusplus_PdMaster
 * Method:    free0
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_com_pdplusplus_PdMaster_free0
  (JNIEnv *, jclass, jlong);

#ifdef __cplusplus
}
#endif
#endif

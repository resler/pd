#include "com_pdplusplus_VoltageControlFilter.h"
#include "VoltageControlFilter.h"

JNIEXPORT jlong JNICALL Java_com_pdplusplus_VoltageControlFilter_allocate0
(JNIEnv* env, jclass obj) {
	pd::VoltageControlFilter* vcf = new pd::VoltageControlFilter();
	return (jlong)vcf;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_VoltageControlFilter_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::VoltageControlFilter* vcf = (pd::VoltageControlFilter*)ptr;
	delete vcf;
}

JNIEXPORT jdoubleArray JNICALL Java_com_pdplusplus_VoltageControlFilter_perform0
(JNIEnv* env, jobject obj, jdouble input, jdouble cf, jlong ptr) {
	pd::VoltageControlFilter* vcf = (pd::VoltageControlFilter*)ptr;
	
	pd::vcfOutput out = vcf->perform((double)input, cf);

	jdouble pair[] = { out.real, out.imaginary };

	jdoubleArray output = env->NewDoubleArray(2);  // allocate
	if (NULL == output) return NULL;
	env->SetDoubleArrayRegion(output, 0, 2, pair);  // copy
	return output;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_VoltageControlFilter_setQ0
(JNIEnv* env, jobject obj, jdouble freq, jlong ptr) {
	pd::VoltageControlFilter* vcf = (pd::VoltageControlFilter*)ptr;
	vcf->setQ((double)freq);
}

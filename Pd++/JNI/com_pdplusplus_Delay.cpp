#include "com_pdplusplus_Delay.h"
#include "Delay.h"

JNIEXPORT jlong JNICALL Java_com_pdplusplus_Delay_allocate0
(JNIEnv* env, jclass obj) {
	pd::Delay* del = new pd::Delay();
	return (jlong)del;

}

JNIEXPORT void JNICALL Java_com_pdplusplus_Delay_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::Delay* del = (pd::Delay*)ptr;
	delete del;
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_Delay_perform0
(JNIEnv* env, jclass obj, jdouble input, jlong ptr) {
	pd::Delay* del = (pd::Delay*)ptr;
	return (jdouble)del->perform(input);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Delay_setDelayTime0
(JNIEnv* env, jclass obj, jdouble time, jlong ptr) {
	pd::Delay* del = (pd::Delay*)ptr;
	del->setDelayTime(time);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Delay_reset0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::Delay* del = (pd::Delay*)ptr;
	del->reset();
}
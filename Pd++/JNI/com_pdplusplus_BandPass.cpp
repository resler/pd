#include "com_pdplusplus_BandPass.h"
#include "BandPass.h"

JNIEXPORT jlong JNICALL Java_com_pdplusplus_BandPass_allocate0
(JNIEnv* env, jclass obj) {
	pd::BandPass *bp = new pd::BandPass();
	return (jlong)bp;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_BandPass_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::BandPass* bp = (pd::BandPass*) ptr;
	delete bp;
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_BandPass_perform0
(JNIEnv* env, jobject obj, jdouble input, jlong ptr) {
	pd::BandPass* bp = (pd::BandPass*) ptr;
	return (jdouble)bp->perform((double)input);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_BandPass_setCenterFrequency0
(JNIEnv* env, jobject obj, jdouble cf, jlong ptr) {
	pd::BandPass* bp = (pd::BandPass*) ptr;
	bp->setCenterFrequency((double)cf);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_BandPass_setQ0
(JNIEnv* env, jobject obj, jdouble q, jlong ptr) {
	pd::BandPass* bp = (pd::BandPass*) ptr;
	bp->setQ((double)q);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_BandPass_clear0
(JNIEnv* env, jobject obj, jlong ptr) {
	pd::BandPass* bp = (pd::BandPass*) ptr;
	bp->clear();
}
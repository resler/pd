#include "com_pdplusplus_ComplexZero.h"
#include "RawFilters.h"


JNIEXPORT jlong JNICALL Java_com_pdplusplus_ComplexZero_allocate0
(JNIEnv* env, jclass obj) {
	pd::ComplexZero* cz = new pd::ComplexZero();
	return (jlong)cz;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_ComplexZero_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::ComplexZero* cz = (pd::ComplexZero*)ptr;
	delete cz;
}

JNIEXPORT jdoubleArray JNICALL Java_com_pdplusplus_ComplexZero_perform0
(JNIEnv* env, jclass obj, jdouble real, jdouble imag, jdouble realCoef, jdouble imagCoef, jlong ptr) {
	pd::ComplexZero* cz = (pd::ComplexZero*)ptr;
	complexOutput out = cz->perform((double)real, (double)imag, (double)realCoef, (double)imagCoef);

	jdouble pair[] = { out.real, out.imaginary };

	jdoubleArray output = env->NewDoubleArray(2);  // allocate
	if (NULL == output) return NULL;
	env->SetDoubleArrayRegion(output, 0, 2, pair);  // copy
	return output;
}


JNIEXPORT void JNICALL Java_com_pdplusplus_ComplexZero_set0
(JNIEnv* env, jclass obj, jdouble real, jdouble imag, jlong ptr) {
	pd::ComplexZero* cz = (pd::ComplexZero*)ptr;
	cz->set((double)real, (double)imag);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_ComplexZero_clear0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::ComplexZero* cz = (pd::ComplexZero*)ptr;
	cz->clear();
}

JNIEXPORT void JNICALL Java_com_pdplusplus_ComplexZero_setLast0
(JNIEnv* env, jclass obj, jdouble last, jlong ptr) {
	pd::ComplexZero* cz = (pd::ComplexZero*)ptr;
	cz->setLast(last);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_ComplexZero_setLastReal0
(JNIEnv* env, jclass obj, jdouble lr, jlong ptr) {
	pd::ComplexZero* cz = (pd::ComplexZero*)ptr;
	cz->setLastReal(lr);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_ComplexZero_setLastImaginary0
(JNIEnv* env, jclass obj, jdouble li, jlong ptr) {
	pd::ComplexZero* cz = (pd::ComplexZero*)ptr;
	cz->setLastImaginary(li);
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_ComplexZero_getLast0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::ComplexZero* cz = (pd::ComplexZero*)ptr;
	return (jdouble)cz->getLast();
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_ComplexZero_getLastReal0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::ComplexZero* cz = (pd::ComplexZero*)ptr;
	return (jdouble)cz->getLastReal();
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_ComplexZero_getLastImaginary0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::ComplexZero* cz = (pd::ComplexZero*)ptr;
	return (jdouble)cz->getLastImaginary();
}

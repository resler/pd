#include "com_pdplusplus_Convolution.h"
#include "Convolution.h"

JNIEXPORT jlong JNICALL Java_com_pdplusplus_Convolution_allocate0__
(JNIEnv* env, jclass obj) {
	rwe::Convolution* conv = new rwe::Convolution();
	return (jlong)conv;
}

JNIEXPORT jlong JNICALL Java_com_pdplusplus_Convolution_allocate0__IIJ
(JNIEnv* env, jclass obj, jint ws, jint ov, jlong ptr) {
	rwe::Convolution* conv = new rwe::Convolution(ws, ov);
	return (jlong)conv;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Convolution_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	rwe::Convolution* conv = (rwe::Convolution*)ptr;
	delete conv;
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_Convolution_perform0
(JNIEnv* env, jclass obj, jdouble filter, jdouble control, jlong ptr) {
	rwe::Convolution* conv = (rwe::Convolution*)ptr;
	return (jdouble)conv->perform(filter, control);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Convolution_setSquelch0
(JNIEnv* env, jclass obj, jint sq, jlong ptr) {
	rwe::Convolution* conv = (rwe::Convolution*)ptr;
	conv->setSquelch((jint)sq);
}

JNIEXPORT jint JNICALL Java_com_pdplusplus_Convolution_getSquelch0
(JNIEnv* env, jclass obj, jlong ptr) {
	rwe::Convolution* conv = (rwe::Convolution*)ptr;
	return (jint)conv->getSquelch();
}
#include "com_pdplusplus_RealPole.h"
#include "RawFilters.h"


JNIEXPORT jlong JNICALL Java_com_pdplusplus_RealPole_allocate0
(JNIEnv* env, jclass obj) {
	pd::RealPole* rp = new pd::RealPole();
	return (jlong)rp;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_RealPole_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::RealPole* rp = (pd::RealPole*)ptr;
	delete rp;
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_RealPole_perform0
(JNIEnv* env, jclass obj, jdouble real, jdouble imag, jlong ptr) {
	pd::RealPole* rp = (pd::RealPole*)ptr;
	return (jdouble)rp->perform((double)real, (double)imag);

}


JNIEXPORT void JNICALL Java_com_pdplusplus_RealPole_set0
(JNIEnv* env, jclass obj, jdouble real, jdouble imag, jlong ptr) {
	pd::RealPole* rp = (pd::RealPole*)ptr;
	rp->set((double)real, (double)imag);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_RealPole_clear0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::RealPole* rp = (pd::RealPole*)ptr;
	rp->clear();
}

JNIEXPORT void JNICALL Java_com_pdplusplus_RealPole_setLast0
(JNIEnv* env, jclass obj, jdouble last, jlong ptr) {
	pd::RealPole* rp = (pd::RealPole*)ptr;
	rp->setLast(last);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_RealPole_setLastReal0
(JNIEnv* env, jclass obj, jdouble lr, jlong ptr) {
	pd::RealPole* rp = (pd::RealPole*)ptr;
	rp->setLastReal(lr);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_RealPole_setLastImaginary0
(JNIEnv* env, jclass obj, jdouble li, jlong ptr) {
	pd::RealPole* rp = (pd::RealPole*)ptr;
	rp->setLastImaginary(li);
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_RealPole_getLast0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::RealPole* rp = (pd::RealPole*)ptr;
	return (jdouble)rp->getLast();
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_RealPole_getLastReal0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::RealPole* rp = (pd::RealPole*)ptr;
	return (jdouble)rp->getLastReal();
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_RealPole_getLastImaginary0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::RealPole* rp = (pd::RealPole*)ptr;
	return (jdouble)rp->getLastImaginary();
}

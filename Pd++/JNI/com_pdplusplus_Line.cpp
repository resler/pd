#include "com_pdplusplus_Line.h"
#include "Line.h"

JNIEXPORT jlong JNICALL Java_com_pdplusplus_Line_allocate0
(JNIEnv* env, jclass obj) {
	pd::Line* line = new pd::Line();
	return (jlong)line;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Line_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::Line* line = (pd::Line*)ptr;
	delete line;
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_Line_perform0
(JNIEnv* env, jclass obj, jdouble target, jdouble time, jlong ptr) {
	pd::Line* line = (pd::Line*)ptr;
	return (jdouble)line->perform(target, time);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Line_set0
(JNIEnv* env, jclass obj, jdouble target, jdouble time, jlong ptr) {
	pd::Line* line = (pd::Line*)ptr;
	line->set(target, time);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Line_stop0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::Line* line = (pd::Line*)ptr;
	line->stop();
}
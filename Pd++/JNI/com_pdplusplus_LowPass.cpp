#include "com_pdplusplus_LowPass.h"
#include "LowPass.h"

JNIEXPORT jlong JNICALL Java_com_pdplusplus_LowPass_allocate0
(JNIEnv* env, jclass obj) {
	pd::LowPass *lop = new pd::LowPass();
	return (jlong)lop;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_LowPass_free0
(JNIEnv* env, jclass obl, jlong ptr) {
	pd::LowPass *lop = (pd::LowPass*) ptr;
	delete lop;
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_LowPass_perform0
(JNIEnv* env, jobject obj, jdouble input, jlong ptr) {
	pd::LowPass* lop = (pd::LowPass*) ptr;
	return (jdouble)lop->perform((double)input);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_LowPass_setCutoff0
(JNIEnv* env, jobject obj, jdouble freq, jlong ptr) {
	pd::LowPass* lop = (pd::LowPass*) ptr;
	lop->setCutoff((double)freq);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_LowPass_clear0
(JNIEnv* env, jobject obj, jlong ptr) {
	pd::LowPass* lop = (pd::LowPass*) ptr;
	lop->clear();

}
#include "com_pdplusplus_Threshold.h"
#include "Threshold.h"

JNIEXPORT jlong JNICALL Java_com_pdplusplus_Threshold_allocate0
(JNIEnv* env, jclass obj) {
	pd::Threshold* thresh = new pd::Threshold();
	return (jlong)thresh;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Threshold_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::Threshold* thresh = (pd::Threshold*)ptr;
	delete thresh;
}

JNIEXPORT jint JNICALL Java_com_pdplusplus_Threshold_perform0
(JNIEnv* env, jclass obj, jdouble input, jlong ptr) {
	pd::Threshold* thresh = (pd::Threshold*)ptr;
	return (jint)thresh->perform((double)input);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Threshold_setValues0
(JNIEnv* env, jclass obj, jdouble ht, jdouble hd, jdouble lt, jdouble ld, jlong ptr) {
	pd::Threshold* thresh = (pd::Threshold*)ptr;
	thresh->setValues((double)ht, (double)hd, (double)lt, (double)ld);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Threshold_setState0
(JNIEnv* env, jclass obj, jint s, jlong ptr) {
	pd::Threshold* thresh = (pd::Threshold*)ptr;
	thresh->setState((int)s);
}
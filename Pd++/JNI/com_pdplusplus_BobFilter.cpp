#include "com_pdplusplus_BobFilter.h"
#include "BobFilter.h"

JNIEXPORT jlong JNICALL Java_com_pdplusplus_BobFilter_allocate0
(JNIEnv* env, jclass obj) {
	pd::BobFilter* bob = new pd::BobFilter();
	return (jlong)bob;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_BobFilter_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::BobFilter* bob = (pd::BobFilter*)ptr;
	delete bob;
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_BobFilter_perform0
(JNIEnv* env, jobject obj, jdouble input, jlong ptr) {
	pd::BobFilter* bob = (pd::BobFilter*)ptr;
	return (jdouble)bob->perform((double)input);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_BobFilter_setCutoffFrequency0
(JNIEnv* env, jobject obj, jdouble cf, jlong ptr) {
	pd::BobFilter* bob = (pd::BobFilter*)ptr;
	bob->setCutoffFrequency((double)cf);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_BobFilter_setResonance0
(JNIEnv* env, jobject obj, jdouble r, jlong ptr) {
	pd::BobFilter* bob = (pd::BobFilter*)ptr;
	bob->setResonance((double)r);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_BobFilter_setSaturation0
(JNIEnv* env, jobject obj, jdouble s, jlong ptr) {
	pd::BobFilter* bob = (pd::BobFilter*)ptr;
	bob->setSaturation((double)s);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_BobFilter_setOversampling0
(JNIEnv* env, jobject obj, jdouble o, jlong ptr) {
	pd::BobFilter* bob = (pd::BobFilter*)ptr;
	bob->setOversampling((double)o);
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_BobFilter_getCutoffFrequency0
(JNIEnv* env, jobject obj, jlong ptr) {
	pd::BobFilter* bob = (pd::BobFilter*)ptr;
	return (jdouble)bob->getCutoffFrequency();
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_BobFilter_getResonance0
(JNIEnv* env, jobject obj, jlong ptr) {
	pd::BobFilter* bob = (pd::BobFilter*)ptr;
	return (jdouble)bob->getResonance();
}

JNIEXPORT void JNICALL Java_com_pdplusplus_BobFilter_error0
(JNIEnv* env, jobject obj, jlong ptr) {
	pd::BobFilter* bob = (pd::BobFilter*)ptr;
	bob->error();
}

JNIEXPORT void JNICALL Java_com_pdplusplus_BobFilter_clear0
(JNIEnv* env, jobject obj, jlong ptr) {
	pd::BobFilter* bob = (pd::BobFilter*)ptr;
	bob->clear();
}

JNIEXPORT void JNICALL Java_com_pdplusplus_BobFilter_print0
(JNIEnv* env, jobject obj, jlong ptr) {
	pd::BobFilter* bob = (pd::BobFilter*)ptr;
	bob->print();
}
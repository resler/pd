#include "com_pdplusplus_PhaseVocoder.h"
#include "PhaseVocoder.h"

JNIEXPORT jlong JNICALL Java_com_pdplusplus_PhaseVocoder_allocate0__
(JNIEnv* env, jclass obj) {
	rwe::PhaseVocoder* pvoc = new rwe::PhaseVocoder();
	return (jlong)pvoc;
}
 
JNIEXPORT jlong JNICALL Java_com_pdplusplus_PhaseVocoder_allocate0__II
(JNIEnv* env, jclass ob, jint ws, jint o) {
	rwe::PhaseVocoder* pvoc = new rwe::PhaseVocoder(ws, o);
	return (jlong)pvoc;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_PhaseVocoder_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	rwe::PhaseVocoder* pvoc = (rwe::PhaseVocoder*)ptr;
	delete pvoc;
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_PhaseVocoder_perform0
(JNIEnv* env, jobject obj, jlong ptr) {
	rwe::PhaseVocoder* pvoc = (rwe::PhaseVocoder*)ptr;
	return (jdouble)pvoc->perform();
}

JNIEXPORT void JNICALL Java_com_pdplusplus_PhaseVocoder_inSample0
(JNIEnv* env, jobject obj, jstring file, jlong ptr) {
	rwe::PhaseVocoder* pvoc = (rwe::PhaseVocoder*)ptr;
	const char* f = env->GetStringUTFChars(file, NULL);
	std::string soundFile = f;
	pvoc->inSample(soundFile);
	env->ReleaseStringUTFChars(file, f);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_PhaseVocoder_setSpeed0
(JNIEnv* env, jobject obj, jdouble s, jlong ptr) {
	rwe::PhaseVocoder* pvoc = (rwe::PhaseVocoder*)ptr;
	pvoc->setSpeed(s);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_PhaseVocoder_setTranspo0
(JNIEnv* env, jobject obj, jdouble t, jlong ptr) {
	rwe::PhaseVocoder* pvoc = (rwe::PhaseVocoder*)ptr;
	pvoc->setTranspo(t);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_PhaseVocoder_setLock0
(JNIEnv* env, jobject obj, jint l, jlong ptr) {
	rwe::PhaseVocoder* pvoc = (rwe::PhaseVocoder*)ptr;
	pvoc->setLock(l);
}

JNIEXPORT void JNICALL Java_com_pdplusplus_PhaseVocoder_setRewind0
(JNIEnv* env, jobject obj, jlong ptr) {
	rwe::PhaseVocoder* pvoc = (rwe::PhaseVocoder*)ptr;
	pvoc->setRewind();
}
#include "com_pdplusplus_Envelope.h"
#include "Envelope.h"

JNIEXPORT jlong JNICALL Java_com_pdplusplus_Envelope_allocate0
(JNIEnv* env, jclass obj) {
	pd::Envelope* en = new pd::Envelope();
	return (jlong)en;
}

//this is so we can overload our constructor
JNIEXPORT jlong JNICALL Java_com_pdplusplus_Envelope_allocate1
(JNIEnv * env, jclass obj, jint ws, jint p) {
    pd::Envelope* en = new pd::Envelope(ws, p);
    return (jlong)en;
}

JNIEXPORT void JNICALL Java_com_pdplusplus_Envelope_free0
(JNIEnv* env, jclass obj, jlong ptr) {
	pd::Envelope* en = (pd::Envelope*)ptr;
	delete en;
}

JNIEXPORT jdouble JNICALL Java_com_pdplusplus_Envelope_perform0
(JNIEnv* env, jclass obj, jdouble input, jlong ptr) {
	pd::Envelope* en = (pd::Envelope*)ptr;
	return (jdouble)en->perform((double)input);
}

TARGET_EXEC ?= libpd++.so

BUILD_DIR ?= ./build
SRC_DIRS ?= ./Pd++

SRCS := $(shell find $(SRC_DIRS) -name *.cpp | grep -v Source | grep -v JNI)

OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)
DEPS := $(OBJS:.o=.d)

INC_DIRS := $(shell find $(SRC_DIRS) -type d)
INC_FLAGS := $(addprefix -I,$(INC_DIRS))

CPPFLAGS ?= $(INC_FLAGS) -MMD -MP -std=c++11

$(BUILD_DIR)/$(TARGET_EXEC): $(OBJS)
	$(CXX) -shared -o $@ $(LDFLAGS) $(OBJS)

# c++ source
$(BUILD_DIR)/%.cpp.o: %.cpp
	$(MKDIR_P) $(dir $@)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c -fpic $< -o $@

.PHONY: clean, install

clean:
	$(RM) -r $(BUILD_DIR)

#make sure to use sudo and run ldconfig after
install:
	cp ./build/$(TARGET_EXEC) /usr/local/lib

-include $(DEPS)

MKDIR_P ?= mkdir -p


Pd++: a C++ implementation of Pure Data objects v.07 (tested, and in development)

# Welcome to Pd++
Currently this library is meant to emulate Pd objects in pure C++.  This is different from libpd (www.libpd.cc) and other iterations.  It is a different paradigm altogether, and only uses the commonality of the Pd DSP and signal flow, not the graphical convention or anything else similar to Pd.

Contributors:  Please see the comments in the various classes.  To write new C++ classes of Pd objects you will need the Pure Data source code which is provided with a version of Pd-vanilla or libpd.  Go to http://puredata.info to obtain these files or applications.

Once you have the Pd source you will need to dig around to find particular Pd "Vanilla" objects.  For example the unit generators are in a file called d_osc.c.  To translate the C code into our C++ code will take some knowledge of the Pd class system.  Most of Pd's code (e.g _setup(), _dsp() functions) may not be necessary.  Though it is important you generally understand what is happening when the Pd object is created and running.  If you want to know more about Pd objects and their class system go to: http://iem.at/pd/externals-HOWTO/HOWTO-externals-en.html

# Operating Systems
This code has been tested on Windows 10, Linux (Ubuntu), Raspian and MacOSX.
Linux users: this library has been tested on Ubuntu and Raspian, it seems to compile and work fine.  There is a Makefile in the root directory that builds the shared library and also one in the /Source file that builds the binary linked to libpd++.so and libportaudio.a.   

# Time in Pd++ 
Pd objects that use time in some way, eg. [metro], [timer], [line] or [line~] will not use Pd's t_clock. Instead time objects will have to be scheduled at increments no smaller than 1/SR since the audio I/O (port audio) only calls PdAlgorithm::runAlgorithm(double, double) once per callback. These classes may stray from Pd's code.
 
# FFT windowing:
There is a global fft window in PdMaster, use PdMaster and FFT as superclasses.  
 
# Sound Files:  
The Pd soundfiler code was too much trouble to extract so currently I'm using STK's file read and write classes.  To my ear they sound the same and have similar functionality.

# Audio API: 
This library has a wrapper class for portaudio called paRender++.  To use portaudio download the library from: http://www.portaudio.com, and compile it, then add the library to your project.  You can use any audio API and create your own wrapper class and schedule your buffer however you like.  Portaudio has worked well on OSX.

# Contribution 
Finally, please adopt the coding style of this project.  Classes are capitalized, functions and variable names use the stepCase format and are descriptive of what they do. Avoid underscores and dashes. If you create your own personal classes use a namespace such as your initials, see the classes in MyClasses.

Place any new C++ classes of Pd objects in the Pd++Classes folder.  

Developers: If you are using this library and not contributing you will mostly be interested in the PdAlgorithm class.  This is where all of your audio input and output data will be processed.  All Pd++ classes should generally behave the same as in Pd with arguments and dataflow of a Pd object being the same as Pd++ object.  Outputs will generally be double precision floats, though there may be some that return a structure or other data types.  Look at their implementation and header files for more information.

Some Pd objects will not exist and will instead use C++'s own definitions. 
This includes math functions like *, /, +, -, abs, etc. as well as relational and conditional logic expressions.  
Example, in Pd there is a [*~] and a [* ] object that accept signal data and control data respectively.  
In Pd++ this is not necessary as all data is scheduled per sample,
 so * in C++ is good for signal and control data.


# API DOCUMENTATION: 
There is a doxygen documentation of the project.  It is no longer being updated and will disappear at some point.  So read the code instead.  It's the same thing anyways.

Java Bindings: Pd++ can now be used with Java or Processing, for more information see https://github.com/robertesler/Pd4P3

Makefiles:  There are three make files.  The root makefile will build a .so without the JNI bindings.  The ./Pd++/Makefile will build the library with the JNI bindings.  The ./Pd++/Source/Makefile will build your PdAlgorithm.cpp, Main.cpp with the shared library.  This could be used for testing or whatever.  If you have questions contact the author: Robert Esler

# License
License and other information:
This library is released under the same license as Pure Data. See the license information below.

This software is copyrighted by Robert Esler, some parts by Miller Puckette and others. The following terms (the "Standard Improved BSD License") apply to all files associated with the software unless explicitly disclaimed in individual files:

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above
copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
The name of the author may not be used to endorse or promote products derived from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.